// AutoInstall.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "direct.h"
#include "Afxwin.h"
#include "shlwapi.h"
#include "atlbase.h"

#include "difxapi.h"
#include "devioctl.h"
#include "ntddscsi.h"
#include "spti.h"
#include "SetupAPI.h"
#include <devguid.h>

#define MAX_STRING (1024)
#define AMOI_MOBILE_MODEM	L"WP3.0 USB Modem"
#define AMOI_NET_SETTING            _T("Software\\AMOI WP3.0")

void CopyFolderAllFiles(CString   csSourceFolder,   CString   csNewFolder);
static BOOL ReadRegister(CString strKeyName, CString strValueName, CString &strValue);
static BOOL WriteRegister(CString strKeyName, CString strValueName, CString strValue);
void SendSCSICmd(BOOL bStart); //added by chenguiming for install usb drivers automaticlly
static BOOL IsSetup();

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
 	// TODO: Place code here.
	CString	filepath;
	CFileFind   finder; 
    int result = 0;
	size_t nRet = 0;
	CString   csNewFolder = L"C:\\IM2 TEMP";
	CString szTmp;

	//如果有其他com口存在 不自动运行
	{
		HDEVINFO		hDevInfo;
		SP_DEVINFO_DATA DeviceInfoData;  
		DWORD	i;
	
		//CString ComStr = L"USB\\Vid_1614&Pid_0407&Rev_0000&MI_02";  //8709
		CString ComStr = L"USB\\Vid_1614&Pid_0408&Rev_0000&MI_02";
		// Enumerate through all devices in Set.
		DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		hDevInfo = SetupDiGetClassDevs((LPGUID)&GUID_DEVCLASS_PORTS, 0, 0,DIGCF_PRESENT);
		if (hDevInfo == INVALID_HANDLE_VALUE)
			return 0;
		
		for (i = 0; SetupDiEnumDeviceInfo(hDevInfo, i,
				&DeviceInfoData); i++)
		{
			DWORD DataT;
			char  buffer[1024] = {0};
			DWORD buffersize = sizeof(buffer);
		
			if (!SetupDiGetDeviceRegistryProperty(
						hDevInfo,
						&DeviceInfoData,
						SPDRP_HARDWAREID,
						&DataT,
						(PBYTE)buffer,
						buffersize,
						&buffersize))
			{
				continue;
			}
			{
				CString strCom;
				
				strCom.Format(_T("%s"),buffer);
				strCom.MakeLower();
				ComStr.MakeLower();
				if (strCom != ComStr)
					continue;
			}

			nRet = 1;
		}
		
		// Cleanup	 
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}

	//只有在只有光盘存在的情况下，才会做清空前面的操作，已防止驱动安装没成功就被清除
	if (nRet == 0)
	{
		if (PathFileExists(csNewFolder))
		{
			  SHFILEOPSTRUCT   fileop;   
			    
			  fileop.hwnd           =   NULL;   
			  fileop.wFunc         =   FO_DELETE;   
			  fileop.pFrom         =   "c:\\IM2 TEMP\0";   //注意，后面需要有一个\0字符   
			  fileop.pTo             =   NULL;   
			  fileop.fFlags       =   FOF_SILENT   |   FOF_NOCONFIRMATION;   
			  SHFileOperation(&fileop);
		}	
	}
	//把驱动和软件统一起来
	if (IsSetup())
	{

		//修改处理，拷贝安装程序到电脑固定位置再进行安装
		CString	filepath;
		char   dataPath[MAX_PATH] = {0};   
		CString   csSourceFolder;

		//只有光盘存在的情况下才进行安装操作,如果有其他com口存在说明已经切换过不需要做处理
		//if (nRet != 0)
		//	return 0;

		//拷贝安装程序到本机
		GetCurrentDirectory(MAX_PATH,(LPTSTR)dataPath);
		csSourceFolder.Format(_T("%s"),dataPath);

		if(!PathFileExists(csNewFolder))   
		  mkdir(csNewFolder);   

		CopyFolderAllFiles(csSourceFolder,csNewFolder);

		//启动pc端的安装工具
		filepath.Format(_T("%s%s"),csNewFolder,_T("\\setup.exe"));

		ShellExecute(NULL,NULL,filepath,NULL,NULL,SW_SHOW); 
	}
	else
	{
		int temp = 0;

		//如果没有进行切换，只做切换操作
		if (nRet == 0)
		{
			SendSCSICmd(FALSE); //After installing driver, send scsi command to mobile
			return 0;
		}
					
		ReadRegister(AMOI_NET_SETTING,_T("FilePath"), filepath);

		//读注册表的信息，看看是否需要启动MM。这部分代码不需要修改
		TCHAR Return[255] = {0};
		CString szTmp;
		CString szFile;

		szFile.Format(_T("%s%s"),filepath,_T("\\ModemSetting.ini"));
		GetPrivateProfileString("Setting","Auto","9",Return,255,szFile);
		szTmp.Format(_T("%s"),Return);
		temp = _ttoi(szTmp);

		if (temp == 9)
			temp = 1;

		/*if (!ReadRegister(AMOI_NET_SETTING,_T("Auto"), szTmp))
		{
			CString SetConn;
			SetConn.Format(_T("%d"),1);
			WriteRegister(AMOI_NET_SETTING, _T("Auto"), SetConn);
			temp = 1;
		}
		else
		{
			temp = _ttoi(szTmp);
		}*/

		if (temp == 1)
		{
			 filepath.Format(_T("%s%s"),filepath,_T("\\modem.bat"));
			 ShellExecute(NULL,NULL,filepath,NULL,NULL,SW_HIDE);   
		}
		
	}

	return 0;
}

void  CopyFolderAllFiles(CString   csSourceFolder,   CString   csNewFolder)   
  {CFileFind   f;   
    BOOL   bFind=f.FindFile(csSourceFolder+"\\*.*");   
  while(bFind){   
        bFind   =   f.FindNextFile();   
          TRACE(_T("%s\r\n"),f.GetFileName());   
        if(f.IsDots())   continue;   
        if(f.IsDirectory()){   
        _mkdir(csNewFolder+"\\"+f.GetFileName());   
          CopyFolderAllFiles(csSourceFolder+"\\"+f.GetFileName(),csNewFolder+"\\"+f.GetFileName());   }   
      ::SetFileAttributes(csSourceFolder+"\\"+f.GetFileName(),FILE_ATTRIBUTE_NORMAL);   
          ::CopyFile(csSourceFolder+"\\"+f.GetFileName(),csNewFolder+"\\"+f.GetFileName(),FALSE);   
	}   
  }

  // 读注册表
static BOOL ReadRegister(CString strKeyName, CString strValueName, CString &strValue)
{
    CRegKey regSubKey;
    if (regSubKey.Open(HKEY_LOCAL_MACHINE, strKeyName) != ERROR_SUCCESS)
	{
		regSubKey.Close();
        return FALSE;
	}

    DWORD dwLen = 256L;
    if (regSubKey.QueryValue(/*(LPSTR)(LPCTSTR)*/strValue.GetBuffer(100),  // MODIFIED BY SHAOZW
        (LPCTSTR)strValueName, &dwLen) != ERROR_SUCCESS)
	{   
		strValue.ReleaseBuffer(); // ADD BY SHAOZW (really curious! :-O)
		regSubKey.Close();
        return FALSE;
	}
    
	strValue.ReleaseBuffer(); // ADD BY SHAOZW
	regSubKey.Close();
    return TRUE;
}

// 写注册表
static BOOL WriteRegister(CString strKeyName, CString strValueName, CString strValue)
{
    CRegKey regSubKey;
    if (regSubKey.Create(HKEY_LOCAL_MACHINE, strKeyName) != ERROR_SUCCESS)
        return FALSE;
    
    if (regSubKey.SetValue(strValue, strValueName) != ERROR_SUCCESS)
        return FALSE;
    
    if (regSubKey.Close() != ERROR_SUCCESS)
        return FALSE;
    
    return TRUE;
}

void SendSCSICmd(BOOL bStart)
{
    ULONG length = 0;
    ULONG errorCode = 0;
    ULONG returned = 0;
    ULONG sectorSize = 512;
    BOOL status = 0;
    HANDLE fileHandle = NULL;
    PUCHAR dataBuffer = NULL;
    SCSI_PASS_THROUGH_WITH_BUFFERS sptwb;
    SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER sptdwb;
    DWORD accessMode = 0, shareMode = 0;
	CString    szFileModuleName;
	char	*szFilePath;
	char	fileman[MAX_PATH + 1] = {0};
	int n = 0;

	//获取当前盘符，获得句柄
	{
		GetModuleFileName(NULL,szFileModuleName.GetBufferSetLength(MAX_PATH+1),MAX_PATH);
		szFileModuleName.ReleaseBuffer ();
		
		szFilePath = (char*)(LPCTSTR)szFileModuleName;
		
		for(n = 0; n < strlen(szFilePath); n++)
		{
			if(szFilePath[n] == '\\')
			{
				szFilePath[n] = 0x00;
				break;
			}
		}
	}
	
    strcpy(fileman,"\\\\.\\");
	strcat(fileman,szFilePath);

    shareMode = FILE_SHARE_READ | FILE_SHARE_WRITE;  // default
    accessMode = GENERIC_WRITE | GENERIC_READ;       // default
    
    fileHandle = CreateFile(fileman,
        accessMode,
        shareMode,
        NULL,
        OPEN_EXISTING,
        0,
        NULL);
    if (fileHandle == INVALID_HANDLE_VALUE) {
        return;
    }

    ZeroMemory(&sptwb,sizeof(SCSI_PASS_THROUGH_WITH_BUFFERS));
    sptwb.spt.Length = sizeof(SCSI_PASS_THROUGH);
    sptwb.spt.PathId = 0;
    sptwb.spt.TargetId = 1;
    sptwb.spt.Lun = 0;
    sptwb.spt.CdbLength = CDB6GENERIC_LENGTH;
    sptwb.spt.SenseInfoLength = 24;
    sptwb.spt.DataIn = SCSI_IOCTL_DATA_IN;
    sptwb.spt.DataTransferLength = 0;
    sptwb.spt.TimeOutValue = 2;
    sptwb.spt.DataBufferOffset =
        offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);
    sptwb.spt.SenseInfoOffset =
        offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucSenseBuf);
    sptwb.spt.Cdb[0] = bStart?SCSIOP_REWIND:SCSIOP_SEEK;
    length = offsetof(SCSI_PASS_THROUGH_WITH_BUFFERS,ucDataBuf);

    status = DeviceIoControl(fileHandle,
        IOCTL_SCSI_PASS_THROUGH,
        &sptwb,
        sizeof(SCSI_PASS_THROUGH),
        &sptwb,
        length,
        &returned,
        FALSE); 
    CloseHandle(fileHandle);
    return;
}

//这个函数用来判断是否需要重新安装modem
static BOOL IsSetup()
{
	CString	filepath;
	CFileFind   finder;
	CString szTmp;
	int i;

	//先判断注册表中的拨号软件地址信息是否存在，再判断该文件是否存在。
	if ((!ReadRegister(AMOI_NET_SETTING,_T("FilePath"), filepath)) ||  (!finder.FindFile(filepath)))
	{
		return TRUE;
	}	

	//获取版本信息，判断当前PC上是不是最新版本
	if (!ReadRegister(AMOI_NET_SETTING,_T("Version"), szTmp))
	{
		return TRUE;
	}
	else if(szTmp.Compare(_T("18")) == 0)
	{
		//这是特殊处理，因为上个版本不是写到注册表中不是很规范的版本号，而是直接写18
		return TRUE;
	}
	else
	{

		//获取ini文件路径
		CString csTempfolder;
		CString	filepath;
		
		GetModuleFileName(NULL,csTempfolder.GetBufferSetLength(MAX_PATH+1),MAX_PATH);
		csTempfolder.ReleaseBuffer ();
		int mPos;
		mPos=csTempfolder.ReverseFind ('\\');
		csTempfolder=csTempfolder.Left (mPos);
		filepath.Format(_T("%s%s"),csTempfolder,_T("\\INQ1_MobileModem\\MobileModem_setting.ini"));


		//获取版本信息
		TCHAR Return[255] = {0};
		CString	SzVersion;

		GetPrivateProfileString("Version","Version","1",Return,255,filepath);		
		SzVersion.Format(_T("%s"),Return);

		i = szTmp.Compare(SzVersion);
		 if (i < 0)
			return TRUE;
	}


	return FALSE;
}

/*BOOL   InitInstance()   
{   
      m_hMutex   =   ::CreateMutex(NULL,true,"test");   
      if(GetLastError()   ==   ERROR_ALREADY_EXISTS)   
		return   false;     //表明已经运行   
      ReleaseMutex(m_hMutex);   
}*/

