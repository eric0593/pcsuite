// setup.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "direct.h"
#include "Afxwin.h"
#include "shlwapi.h"
#include "atlbase.h"
#include "SetupAPI.h"
#include <devguid.h>

#define MAX_STRING (1024)
#define AMOI_NET_SETTING            _T("Software\\AMOI WP3.0")

BOOL   isWow64(void);
static BOOL ReadRegister(CString strKeyName, CString strValueName, CString &strValue);

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
	CString	filepath;
	CFileFind   finder; 
    int result = 0;
	size_t nRet = 0;
	CString   csNewFolder = L"C:\\IM2 TEMP";
	CString szTmp;

		//如果有其他com口存在 不自动运行
	{
		HDEVINFO		hDevInfo;
		SP_DEVINFO_DATA DeviceInfoData;  
		DWORD	i;

		// Enumerate through all devices in Set.
		DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		hDevInfo = SetupDiGetClassDevs((LPGUID)&GUID_DEVCLASS_MODEM, 0, 0,DIGCF_PRESENT);
		if (hDevInfo != INVALID_HANDLE_VALUE)
		{
			for (i = 0; SetupDiEnumDeviceInfo(hDevInfo, i,
					&DeviceInfoData); i++)
			{
				DWORD DataT;
				char  buffer[MAX_STRING] = {0};
				DWORD buffersize = sizeof(buffer);
				CString strCom;
					
				if (!SetupDiGetDeviceRegistryProperty(
							hDevInfo,
							&DeviceInfoData,
							SPDRP_FRIENDLYNAME,
							&DataT,
							(PBYTE)buffer,
							buffersize,
							&buffersize))
						continue;
				
				strCom.Format(_T("%s"),buffer);

				if (strCom.Find("INQ1") != -1)
				{
					nRet = 1;
					break;
				}
				
			}
			
			// Cleanup	 
			SetupDiDestroyDeviceInfoList(hDevInfo);		
		}
	}

	//接这个位置弄个在pc上安装驱动和modem的程序
	{
		CString csTempfolder;

		GetModuleFileName(NULL,csTempfolder.GetBufferSetLength(MAX_PATH+1),MAX_PATH);
		csTempfolder.ReleaseBuffer ();
		int mPos;
		mPos=csTempfolder.ReverseFind ('\\');
		csTempfolder=csTempfolder.Left (mPos);

		//驱动安装
		if (isWow64())
		{
			filepath.Format(_T("%s%s"),csTempfolder,_T("\\INQ1_USB_Driver\\WIN64\\DPInst.exe"));

			ShellExecute(NULL,NULL,filepath,"/LM /SW /SE",NULL,SW_SHOW); 		
		}
		else
		{
			filepath.Format(_T("%s%s"),csTempfolder,_T("\\INQ1_USB_Driver\\WIN32\\DPInst.exe"));

			ShellExecute(NULL,NULL,filepath,"/LM /SW",NULL,SW_SHOW); 		
		}

		//mobile modem安装
		{
			//获取ini中的安装文件名称
			CString	Inipath;
			
			Inipath.Format(_T("%s%s"),csTempfolder,_T("\\INQ1_MobileModem\\MobileModem_setting.ini"));


			//获取版本信息
			TCHAR Return[255] = {0};
			GetPrivateProfileString("Software","setup","1",Return,255,Inipath);		
			szTmp.Format(_T("%s"),Return);		
		}

		  STARTUPINFO   s;   
		  ZeroMemory(&s,   sizeof(s));   
		  s.cb   =   sizeof(s);   
		  PROCESS_INFORMATION   p;   

		  filepath.Format(_T("%s%s%s"),csTempfolder,_T("\\INQ1_MobileModem\\"),szTmp);
		  BOOL   b   =   CreateProcess((LPCTSTR)filepath,   NULL,   NULL,   NULL,     FALSE,   0,   NULL,   NULL,   &s,   &p);   
		  DWORD   e   =   0;   
		  if(b)   WaitForSingleObject((HANDLE)p.hProcess,   INFINITE);   
		  else   e   =   GetLastError();   		

		//After installing, send scsi command to mobile
		if (nRet ==1)
		{
			int temp = 0;
						
			ReadRegister(AMOI_NET_SETTING,_T("FilePath"), filepath);

			//读注册表的信息，看看是否需要启动MM。这部分代码不需要修改
			TCHAR Return[255] = {0};
			CString szTmp;
			CString szFile;

			szFile.Format(_T("%s%s"),filepath,_T("\\ModemSetting.ini"));
			GetPrivateProfileString("Setting","Auto","9",Return,255,szFile);
			szTmp.Format(_T("%s"),Return);
			temp = _ttoi(szTmp);

			if (temp == 9)
				temp = 1;

			if (temp == 1)
			{
				 filepath.Format(_T("%s%s"),filepath,_T("\\modem.bat"));
				 ShellExecute(NULL,NULL,filepath,NULL,NULL,SW_HIDE);   
			}

		}
		else
		{
			filepath.Format(_T("%s%s"),csTempfolder,_T("\\INQ1_MobileModem\\SendSCSI.exe"));
			ShellExecute(NULL,NULL,filepath,NULL,NULL,SW_SHOW); 
		}

		return 0;
	}

	return 0;
}

BOOL   isWow64(void)   
{   
	UINT   unResult   =   0;   
	int   nResult   =   0;   
	TCHAR   szWinSysDir[MAX_PATH+1]   =   "";   
	TCHAR szKernel32File[MAX_PATH+1+14]   =   "";   
	HINSTANCE   hLibKernel32   =   NULL;   
	BOOL   bIsWow64Process   =   FALSE;   

	BOOL   (WINAPI   *lpIsWow64Process)(HANDLE,PBOOL)   =   NULL;   

	unResult   =   GetSystemDirectory(szWinSysDir,sizeof(szWinSysDir)/sizeof(TCHAR));   
	if   (unResult   >   0)   {   
	nResult   =   _stprintf(szKernel32File,_T("%s\\kernel32.dll"),szWinSysDir);   
	if   (nResult   >   0)   {   
	hLibKernel32   =   LoadLibrary(szKernel32File);   
	}   
	}   

	if   (NULL   ==   hLibKernel32)   {   
	hLibKernel32   =   LoadLibrary(_T("kernel32.dll"));   
	}   

	//   Get   the   Address   of   Win32   API   --   IsWow64Process()   
	if   (   NULL   !=   hLibKernel32   )   {   
	lpIsWow64Process   =   (BOOL   (WINAPI   *)(HANDLE,PBOOL))   
	GetProcAddress(hLibKernel32,_T("IsWow64Process"));   
	}   

	if   (   NULL   !=   lpIsWow64Process   )   {   
	//   Check   whether   the   32-bit   program   is   running   under   WOW64   environment.   
	if   (!lpIsWow64Process(GetCurrentProcess(),&bIsWow64Process))   {   
	FreeLibrary(hLibKernel32);   
	return   FALSE;   
	}   
	}   
	if   (NULL   !=   hLibKernel32)   {   
	FreeLibrary(hLibKernel32);   
	}   

	return   (   bIsWow64Process   );   
}  

  // 读注册表
static BOOL ReadRegister(CString strKeyName, CString strValueName, CString &strValue)
{
    CRegKey regSubKey;
    if (regSubKey.Open(HKEY_LOCAL_MACHINE, strKeyName) != ERROR_SUCCESS)
	{
		regSubKey.Close();
        return FALSE;
	}

    DWORD dwLen = 256L;
    if (regSubKey.QueryValue(/*(LPSTR)(LPCTSTR)*/strValue.GetBuffer(100),  // MODIFIED BY SHAOZW
        (LPCTSTR)strValueName, &dwLen) != ERROR_SUCCESS)
	{   
		strValue.ReleaseBuffer(); // ADD BY SHAOZW (really curious! :-O)
		regSubKey.Close();
        return FALSE;
	}
    
	strValue.ReleaseBuffer(); // ADD BY SHAOZW
	regSubKey.Close();
    return TRUE;
}
