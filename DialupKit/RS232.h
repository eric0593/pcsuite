#ifndef _RS232_H_
#define _RS232_H_

typedef void (CALLBACK *PFNRS232CALLBACK)(void* pUserData, int iPort);

#define ERR_SUCCESS 0
#define ERR_FAILED  1
#define ERR_NORESOURCE 2
#define ERR_MEMORYOVERFLOW 3
#define ERR_PORTOPENFAILED 4
#define ERR_TIMEOUT 5

class CRS232
{														 
public:
	static DWORD WINAPI SerialThreadProc(LPVOID lpParam);
	DWORD ThreadProc();
	BOOL IsOpen();
	int RegisteCallBack(PFNRS232CALLBACK pfnCallBack, void* pUserData);
	CRS232(void);
	~CRS232(void);

	int Open(int iPort = 1, int nBaud = CBR_115200,
	        int nParity = NOPARITY, int nStopbits = ONESTOPBIT,
	        int nDatabits = 8);
	int Close(void);

	int Send(BYTE* pBuff, int iSize, int *pCount);
	int Receive(BYTE* pBuff, int iSize, int *pCount);
	int ClearBuffer(void);
	int SetTimeOut(int Millisecond = 1000);


private:
	int        m_nPort;
	HANDLE     m_hComm;
	HANDLE     m_hThread;
	int        m_TimeOut;
	int        m_WaitingTimer;
	BOOL       m_bExitThread;
	OVERLAPPED m_WaitOverLapped;
	OVERLAPPED m_ReadOverLapped;
	OVERLAPPED m_WriteOverLapped;
	
	HANDLE     m_hMutex;
	void              *m_pUserData;
	PFNRS232CALLBACK  m_pfnCallback;
};

#endif //_C_SERIAL_PROT_H_
