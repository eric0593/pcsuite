

#if !defined(AFX_MYDLG_H__4CF833FB_C7A9_4DE4_AF8A_D8D263E4D4C2__INCLUDED_)
#define AFX_MYDLG_H__4CF833FB_C7A9_4DE4_AF8A_D8D263E4D4C2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MyDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMyDlg dialog

class CMyDlg : public CDialog
{
// Construction
public:
	CMyDlg(UINT nIDTemplate, CWnd* pParent = NULL, BOOL bTranslateMsg = FALSE);   // standard constructor
    virtual void SetAppText(UINT uResourceID_AppName);          // 设置窗口标题
    virtual void SetCtrlWindowTitle() {};                       // 设置界面上静态控件的文本
    
    void SetWindowTitle(UINT uCtrlID, UINT uResourceID);        // 设置控件的文本
    char *GetMessageString(UINT uResource);                     // 从资源取出文本

    void InitListCtrl(CImageList *pImageList, CListCtrl *pListCtrl);

public:

// Dialog Data
	//{{AFX_DATA(CMyDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
    BOOL m_bTranslateMsg;

	// Generated message map functions
	//{{AFX_MSG(CMyDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYDLG_H__4CF833FB_C7A9_4DE4_AF8A_D8D263E4D4C2__INCLUDED_)
