// RasEntryAmoi.h: interface for the CRasEntryAmoi class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RASENTRYAMOI_H__0636D5B8_B5E8_4B3D_9E3E_BB81C9148784__INCLUDED_)
#define AFX_RASENTRYAMOI_H__0636D5B8_B5E8_4B3D_9E3E_BB81C9148784__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "RasEntry.h"

class CRasEntryAmoi : public CRasEntry  
{
public:
	CRasEntryAmoi();
	virtual ~CRasEntryAmoi();

    BOOL ExistModemDriver();                                    // 检测驱动是否安装
    BOOL EnumModem(TCHAR *szDeviceType, CString &strDevice);     // 枚举当前是否有夏新手机Modem连接
    void GetPbkDir(CString &str);                               // 取的Pbk文件目录
    BOOL WritePbkProfile                                        // 写参数到Pbk文件中
        (
        CString strSection, 
        CString strKeyName, 
        int nValue
        );
};

#endif // !defined(AFX_RASENTRYAMOI_H__0636D5B8_B5E8_4B3D_9E3E_BB81C9148784__INCLUDED_)
