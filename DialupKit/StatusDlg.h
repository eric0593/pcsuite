#if !defined(AFX_STATUSDLG_H__53028D1E_78E4_4E4E_807F_1B3A8A252B69__INCLUDED_)
#define AFX_STATUSDLG_H__53028D1E_78E4_4E4E_807F_1B3A8A252B69__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StatusDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStatusDlg dialog
#include "MyDlg.h"
class CStatusDlg : public CMyDlg
{
	// Construction
public:
	//CStatusDlg(CString strInfo, CWnd* pParent = NULL);   // standard constructor
	CStatusDlg(CWnd* pParent = NULL); 
	
	// Dialog Data
	//{{AFX_DATA(CStatusDlg)
	enum { IDD = IDD_DIALOG_LINKSTATUS };
	CString	m_strStatusText;
	//}}AFX_DATA
	
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStatusDlg)
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	
	// Generated message map functions
	//{{AFX_MSG(CStatusDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnCancel();
	afx_msg void OnDisconnect();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STATUSDLG_H__53028D1E_78E4_4E4E_807F_1B3A8A252B69__INCLUDED_)
