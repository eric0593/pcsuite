// DialupKitDlg.h : header file
//

#if !defined(AFX_DIALUPKITDLG_H__7048A3B2_9D9A_4BBC_992C_1F6F26FA8A10__INCLUDED_)
#define AFX_DIALUPKITDLG_H__7048A3B2_9D9A_4BBC_992C_1F6F26FA8A10__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "MyDlg.h"
#include "olorMenu.h"

//define to load the dll
//#define DLGetValidComPortName  "GetValidComPort"
//typedef int TComId;  // com port id
//typedef int (TDLGetValidComPortProc)(TComId *pComIdBuf, size_t ComIdBufSize);


/////////////////////////////////////////////////////////////////////////////
// CDialupKitDlg dialog

class CDialupKitDlg : public CMyDlg
{
// Construction
public:
	BOOL m_bShutDown;
    CString	m_HandsetStatus;
	void CreateMenu();
	CDialupKitDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDialupKitDlg)
	enum { IDD = IDD_DIALUPKIT_DIALOG };
	CListCtrl	m_lstNetLink;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialupKitDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

	void TransDlg();
	CTime m_tConnectionBegin;
    //lishch added
   // HINSTANCE m_hLibraryListComPort;
    //TDLGetValidComPortProc* m_GetValidComPortProc;
    //UINT m_DetectComTimer;
// Implementation
protected:
	HICON m_hIcon;
	CImageList m_ImageList;
	ColorMenu m_hMenu;

    int   m_nTx;
    int   m_nRx;
    CTimeSpan m_tsDuration;
	// Generated message map functions
	//{{AFX_MSG(CDialupKitDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnMenuSetup();
	afx_msg void OnUpdateMenuSetup(CCmdUI* pCmdUI);
	afx_msg void OnMenuConn();
	afx_msg void OnUpdateMenuConn(CCmdUI* pCmdUI);
	afx_msg void OnMenuDiscon();
	afx_msg void OnUpdateMenuDiscon(CCmdUI* pCmdUI);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnAppExit();
	afx_msg void OnBtnConnect();
	afx_msg void OnBtnExit();
	afx_msg void OnBtnMinimize();
	afx_msg void OnDblclkListGprs(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMenuAbout();
	afx_msg void OnUpdateMenuAbout(CCmdUI* pCmdUI);
	afx_msg void OnAppAbout();
	afx_msg void OnUpdateAppAbout(CCmdUI* pCmdUI);
	afx_msg void OnMenuHistory();
	afx_msg void OnUpdateMenuHistory(CCmdUI* pCmdUI);
	afx_msg void OnClose();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	//}}AFX_MSG
  afx_msg void OnGetDialStatus(WPARAM wParam, LPARAM lParam);     // 拨号后的状态，这个是消息处理函数，对应系统消息:WM_RAS_GETTINGDIALSTATUS
	LRESULT OnTrayNotify(WPARAM wParam,LPARAM lParam);//wParam接收的是图标的ID，而lParam接收的是鼠标的行为
	void SetTrayIcon(BOOL bAdd);
	DECLARE_MESSAGE_MAP()void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu); 
private:
	bool m_bLanChange;
	//CString m_szUSBDisconnect;
	//CString m_szUSBConnect;
	bool m_bPreConnectState;
    bool  m_bFirstTime;
	static BOOL CALLBACK EnumChildProc(
		HWND hwnd,      // handle to child window
		LPARAM lParam   // application-defined value
		);
    void UpdateConnectionStatus();              // update connection all connection status
    void UpdateDeviceConnectStatus();           //detect if there is any amoi device connect
    void UpdateConnectionIcon(int nTx, int nRx); // update connection status icon
    CString FormatSpeed(int nBps);  // format the connection speed (Kbps) 
    CString FormatNumberPointedOff(int n); // SEPARATE THE NUMBER WITH ',' PROPERLY
	void Disconnect();
    //void ReleaseData();
    void OnLangStart(UINT nID);
    void OnUpdateLangStart(CCmdUI* pCmdUI);

    //member
    CBitmap m_BitConnect;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALUPKITDLG_H__7048A3B2_9D9A_4BBC_992C_1F6F26FA8A10__INCLUDED_)
