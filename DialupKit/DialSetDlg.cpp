// DialSetDlg.cpp : implementation file
//

#include "stdafx.h"

#include "DialSetDlg.h"
#include "ControlData.h"
#include "Common.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CFont Newfont;
extern BOOL m_IsAddFone;//判断是否要添加字体

CMapStringToOb* m_pAPMap = NULL;
static char* DBHeader[] = { "Country", "ConnectName","Provider Name" ,"APNName","UserName","Password","PhoneNumber","HomePage"};
/////////////////////////////////////////////////////////////////////////////
// CDialSetDlg dialog
CDialSetDlg::CDialSetDlg(CWnd* pParent /*=NULL*/)
	: CRgnDialog(CDialSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialSetDlg)
	m_szAPN = _T("");
	m_szIP = _T("");
	m_szPass = _T("");
	m_szUser = _T("");
	m_szConnectName = _T("");
	m_szNO = _T("");
	//}}AFX_DATA_INIT
    m_pAPNDB = NULL;
}

void CDialSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CRgnDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialSetDlg)
	DDX_Control(pDX, IDC_DIALNO, m_hDialNo);
	DDX_Control(pDX, IDC_COUNTRYCOMBO, m_hCountryCombo);
	DDX_Text(pDX, IDC_EDIT_APN, m_szAPN);
	DDX_Text(pDX, IDC_EDIT_PASS, m_szPass);
	DDX_Text(pDX, IDC_EDIT_USER, m_szUser);
	DDX_CBString(pDX, IDC_DIALNO, m_szNO);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_COUNTRY, m_Country);
	DDX_Control(pDX, IDC_APNNAME, m_APNName);
	DDX_Control(pDX, IDC_PBNUMBER, m_ConName);
	DDX_Control(pDX, IDC_USERNAME, m_UserName);
	DDX_Control(pDX, IDC_PASSWORD, m_pas);
	DDX_Control(pDX, IDC_SETTITLE, m_Title);
	DDX_Control(pDX, IDC_EDIT_APN, m_APNEDIT);
	DDX_Control(pDX, IDC_EDIT_USER, m_UerNameEdit);
	DDX_Control(pDX, IDOK, m_ok);
	DDX_Control(pDX, IDCANCEL, m_bcancel);
}


BEGIN_MESSAGE_MAP(CDialSetDlg, CRgnDialog)
	//{{AFX_MSG_MAP(CDialSetDlg)
	ON_WM_ERASEBKGND()
	ON_CBN_SELCHANGE(IDC_COUNTRYCOMBO, OnSelchangeCountrycombo)
	//ON_CBN_SELCHANGE(IDC_NETWORKCOMBO, OnSelchangeNetworkcombo)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CDialSetDlg::OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////


///

void CDialSetDlg::OnOK() 
{
	// TODO: Add extra validation here
    CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	//CRegKey regKey;
	CString strIPV4= "AT+CGDCONT=1,\"IP\",\"";
	CString szFile,szPath;
	
	UpdateData(TRUE);
	
	//写ini
	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\APNINFO.ini");
	
    pApp->m_pDialPram->szAPN = _tcsdup(this->m_szAPN);
    TRACE(pApp->m_pDialPram->szAPN);
    TRACE(L"\n");
    
    strIPV4 += pApp->m_pDialPram->szAPN;
    strIPV4 += "\"";
	TRACE(strIPV4);
    TRACE(L"\n");

    m_hCountryCombo.GetLBText(m_hCountryCombo.GetCurSel(), m_szCountry);
    pApp->m_pDialPram->szCountry =  _tcsdup(this->m_szCountry);
    pApp->m_pDialPram->szIPAddress =  _tcsdup(this->m_szIP);
    pApp->m_pDialPram->szPassword =  _tcsdup(this->m_szPass);
    pApp->m_pDialPram->szPhoneNumber =  _tcsdup(this->m_szNO);
    pApp->m_pDialPram->szUserName =  _tcsdup(this->m_szUser);

	TCHAR Return[255] = {0};
	GetPrivateProfileString(m_szCountry,L"HomePage",L"1",Return,255,szFile);
	m_szHomepage = _wcsdup(Return);
	pApp->m_pDialPram->szHomepage =  _tcsdup(this->m_szHomepage);

	
	WritePrivateProfileString(m_szCountry,L"APNName",m_szAPN,szFile);
	WritePrivateProfileString(m_szCountry,L"PhoneNumber",m_szNO,szFile);
	WritePrivateProfileString(m_szCountry,L"UserName",m_szUser,szFile);
	WritePrivateProfileString(m_szCountry,L"Password",m_szPass,szFile);

	CControlData::Instance()->SetAPN8709(strIPV4);
	CControlData::Instance()->WriteNetSettings();

	CRgnDialog::OnOK();
}


BOOL CDialSetDlg::OnInitDialog() 
{
    int i = 0, n = 0;
    int nCount = 0;
    CString  szCountry;
    BOOL     bSet = FALSE;
    CString  CurCountry;
	CString szFile,szPath;
	CString szReturn;
	TCHAR* pReturn = szReturn.GetBuffer(255);
	
	//m_Bitmap.LoadBitmap(IDB_DIALSETBK);
	//SetBackgroundBitmap(&m_Bitmap,RGB(255,0,255));
	CRgnDialog::OnInitDialog();

	if (m_IsAddFone == FALSE)
	{
		m_Country.SetFont(&Newfont);
		m_hCountryCombo.SetFont(&Newfont);
		m_APNName.SetFont(&Newfont);
		m_APNEDIT.SetFont(&Newfont);
		m_ConName.SetFont(&Newfont);
		m_hDialNo.SetFont(&Newfont);
		m_UserName.SetFont(&Newfont);
		m_pas.SetFont(&Newfont);
		m_UerNameEdit.SetFont(&Newfont);
		m_ok.SetFont(&Newfont);
		m_bcancel.SetFont(&Newfont);
	}

	//获取ini文件的绝对路径
	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\APNINFO.ini");

	//获取注册表中设置的国家信息，如果信息有问题就默认使用italy
	CString szFile1,szPath1;
	TCHAR Return[255] = {0};
	GetModuleFileName(NULL,szPath1.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath1.ReleaseBuffer ();
	int mPos1;
	mPos1=szPath1.ReverseFind ('\\');
	szPath1=szPath1.Left (mPos1);
	szFile1 = szPath1 + _T("\\ModemSetting.ini");
	GetPrivateProfileString(L"DialupSet",L"Country",L"1",Return,255,szFile1);
	CurCountry.Format(L"%s",Return);

	//获取国家信息放到下拉框中
	GetPrivateProfileString(NULL,NULL,NULL,pReturn,255,szFile); 	
	szReturn.ReleaseBuffer();
	
	{
		int i,pos = 0;
		CString Temp;
		
		for (i=0;i<255;i++)
		{
			if (pReturn[i] == NULL)
			{
				Temp = (TCHAR*)(pReturn + pos);
				m_hCountryCombo.AddString(Temp);

				if (!Temp.Compare(CurCountry))
				{
					bSet = TRUE;
				}
				
				pos = i + 1;
				if (pReturn[pos] == NULL)
					break;
			}
				
		}
	}

	//确定combo显示的值
	if (bSet == FALSE)
	{
		CurCountry.Format(_T("%s"),L"Italy");
	}
	m_szCountry.Format(_T("%s"),CurCountry);
	
	nCount = m_hCountryCombo.GetCount();
	for(i = 0; i < nCount; i++)
	{
		n = m_hCountryCombo.GetLBTextLen( i );
		m_hCountryCombo.GetLBText( i, szCountry.GetBuffer(n) );
		if(!szCountry.Compare(CurCountry))
		{
			szCountry.ReleaseBuffer();
			m_hCountryCombo.SetCurSel(i);
		}
	}

	//设置其他需要显示的
	{
		TCHAR Return[255] = {0};
		GetPrivateProfileString(CurCountry,L"APNName",L"1",Return,255,szFile);

		m_szAPN.Format(L"%s",Return);
		
		GetPrivateProfileString(CurCountry,L"PhoneNumber",L"1",Return,255,szFile);
		m_szNO.Format(L"%s",Return);
		
		GetPrivateProfileString(CurCountry,L"UserName",L"1",Return,255,szFile);
		m_szUser.Format(L"%s",Return);
		
		GetPrivateProfileString(CurCountry,L"Password",L"1",Return,255,szFile);
		m_szPass.Format(L"%s",Return);		
	}
	
    m_hDialNo.AddString(_T("*99#"));
    m_hDialNo.AddString(_T("*98*1#"));
    if(!m_szNO.Compare(_T("*99#")))
    {
        m_hDialNo.SetCurSel(0); 
    }
    else
    {
        m_hDialNo.SetCurSel(1);
    }

    TransDlg();

	//设透明背景
	m_Title.SetBackColor(TRANS_BACK);
	m_Title.SetTextColor(RGB(255,255,255));

	m_Country.SetBackColor(TRANS_BACK);
	m_APNName.SetBackColor(TRANS_BACK);
	m_ConName.SetBackColor(TRANS_BACK);
	m_UserName.SetBackColor(TRANS_BACK);
	m_pas.SetBackColor(TRANS_BACK);
	
    UpdateData(FALSE);
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//
void CDialSetDlg::TransDlg()
{
	CString strvalue;
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	int ID_Array[]={/*IDC_SETTITLE,*/IDC_COUNTRY,IDC_NETWORK,IDC_APNNAME,IDC_CONNECTNAME,IDC_PBNUMBER,
		IDC_USERNAME,IDC_PASSWORD,IDOK,IDCANCEL,IDC_ACCESSNAME};
	int T_Array[]={/*T_PROFILE,*/T_COUNTRY,IDC_NETWORK,T_APNNAME,T_CONNECTNAME,T_PBNUMBER,
		T_USERNAME,T_PASSWORD,T_OK,T_CANCEL,IDC_ACCESSNAME};
	for(UINT j=0;j<_countof(ID_Array);j++)
	{
		
		strvalue=pApp->g_GetText(T_Array[j],pApp->m_nLandId);
			//	GetDlgItem(ID_Array[j])->SetWindowText(strvalue);
		SetDlgItemText(ID_Array[j],strvalue);
		strvalue.Empty();
	}
	strvalue=pApp->g_GetText(T_PROFILE,pApp->m_nLandId);
    SetWindowText(strvalue);
	
}

void CDialSetDlg::OnSelchangeCountrycombo() 
{
	// TODO: Add your control notification handler code here
	CString szKey;
	CString szFile,szPath;

	UpdateData(TRUE);	//加这句，使那些全局获得最新的数值(m_szAPN,m_szNO...)

	//获取ini文件的绝对路径
	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\APNINFO.ini");
	WritePrivateProfileString(m_szCountry,L"APNName",m_szAPN,szFile);
	WritePrivateProfileString(m_szCountry,L"PhoneNumber",m_szNO,szFile);
	WritePrivateProfileString(m_szCountry,L"UserName",m_szUser,szFile);
	WritePrivateProfileString(m_szCountry,L"Password",m_szPass,szFile);
	
	m_hCountryCombo.GetLBText(m_hCountryCombo.GetCurSel(),szKey);
	m_szCountry.Format(_T("%s"),szKey);

	//设置其他需要显示的
	{
		TCHAR Return[255] = {0};
		GetPrivateProfileString(szKey,L"APNName",L"1",Return,255,szFile);
		m_szAPN = _wcsdup(Return);
		
		GetPrivateProfileString(szKey,L"PhoneNumber",L"1",Return,255,szFile);
		m_szNO = _wcsdup(Return);
		
		GetPrivateProfileString(szKey,L"UserName",L"1",Return,255,szFile);
		m_szUser = _wcsdup(Return);
		
		GetPrivateProfileString(szKey,L"Password",L"1",Return,255,szFile);
		m_szPass = _wcsdup(Return); 	
	}

	if(!m_szNO.Compare(_T("*99#")))
	{
		m_hDialNo.SetCurSel(0); 
	}
	else
	{
		m_hDialNo.SetCurSel(1);
	}
	UpdateData(FALSE);
}

void CDialSetDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
    CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	//CRegKey regKey;
	CString strIPV4= "AT+CGDCONT=1,\"IP\",\"";
	CString szFile,szPath;
	
	UpdateData(TRUE);
	
	//写ini
	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\APNINFO.ini");
	
    pApp->m_pDialPram->szAPN = _tcsdup(this->m_szAPN);
    TRACE(pApp->m_pDialPram->szAPN);
    TRACE(L"\n");
    
    strIPV4 += pApp->m_pDialPram->szAPN;
    strIPV4 += "\"";
	TRACE(strIPV4);
    TRACE(L"\n");

    m_hCountryCombo.GetLBText(m_hCountryCombo.GetCurSel(), m_szCountry);
    pApp->m_pDialPram->szCountry =  _tcsdup(this->m_szCountry);
    pApp->m_pDialPram->szIPAddress =  _tcsdup(this->m_szIP);
    pApp->m_pDialPram->szPassword =  _tcsdup(this->m_szPass);
    pApp->m_pDialPram->szPhoneNumber =  _tcsdup(this->m_szNO);
    pApp->m_pDialPram->szUserName =  _tcsdup(this->m_szUser);

	TCHAR Return[255] = {0};
	GetPrivateProfileString(m_szCountry,L"HomePage",L"1",Return,255,szFile);
	m_szHomepage = _wcsdup(Return);
	pApp->m_pDialPram->szHomepage =  _tcsdup(this->m_szHomepage);

	
	WritePrivateProfileString(m_szCountry,L"APNName",m_szAPN,szFile);
	WritePrivateProfileString(m_szCountry,L"PhoneNumber",m_szNO,szFile);
	WritePrivateProfileString(m_szCountry,L"UserName",m_szUser,szFile);
	WritePrivateProfileString(m_szCountry,L"Password",m_szPass,szFile);

	CControlData::Instance()->SetAPN8709(strIPV4);
	CControlData::Instance()->WriteNetSettings();

	CRgnDialog::OnOK();
}

BOOL CDialSetDlg::OnEraseBkgnd(CDC* pDC )
{
	CBitmap    bBitmap;
	CDC 	   tempDC;	 
	CRect	   rect;   
	BITMAP		Bmp;

	bBitmap.LoadBitmap(IDB_DIALSETBK);

	tempDC.CreateCompatibleDC(pDC);

	tempDC.SelectObject(&bBitmap);

	GetClientRect(&rect);
	
	bBitmap.GetBitmap(&Bmp);

	pDC->StretchBlt(0, 0, rect.Width(), rect.Height(), &tempDC, 0, 0, Bmp.bmWidth, Bmp.bmHeight, SRCCOPY);

	bBitmap.DeleteObject();

	return 0;
}
