#ifndef _STRING_RESOURCE_H__
#define _STRING_RESOURCE_H__

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the STRINGRESOURCE_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// STRINGRESOURCE_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef STRINGRESOURCE_EXPORTS
#define STRINGRESOURCE_API __declspec(dllexport)
#else
#define STRINGRESOURCE_API __declspec(dllimport)
#endif

#define FREEIF(Point) if (Point)\
                        {\
                            free(Point);\
							Point=NULL;\
                        }

typedef enum
{
	LANG_ENG = 0,  //English
	LANG_ITA,      //Italy
	LANG_FRE,      //French
	LANG_DEU,      //Deutschland  /  Germany
	LANG_DAN,      //Danish
	LANG_SWE,      //Swedish
	LANG_CHN,      //Simple Chinese
	LANG_CHT,      //Traditional  Chinese			 

	LANG_MAX       //Max language supported
} TLanguage;


STRINGRESOURCE_API int GetStringResource(unsigned int nResID,WCHAR** ppszText);
STRINGRESOURCE_API int SetResourceLanguage(TLanguage Language);
STRINGRESOURCE_API void FreeStringResource(void* pMemory);


#endif //_STRING_RESOURCE_H__