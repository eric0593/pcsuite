#ifndef _CONNECTION_HISTORY_H
#define _CONNECTION_HISTORY_H

#define ONE_KBYTE    1000
#define ONE_KBYTE_F  1000.0
#define ONE_MBYTE    1000000
#define ONE_MBYTE_F  1000000.0

CString FormatBytesOutput(int nBytes);
BOOL    GetOneHistoryRecord(int nNo, CString& strDate, CString& strTimeSpan, int& nTx, int& nRx);
CString FormatHistoryRecordString(CTimeSpan timeSpan, int nTx, int nRx);
BOOL    GetHistoryTotalStatistic(CTimeSpan& timeSpan, int& nTx, int& nRx);
BOOL    GetHistoryRecordCnt(int& nCnt);
BOOL    WriteOneHistoryRecord(CTimeSpan timeSpan, int nTx, int nRx);
BOOL    ClearAllHistoryRecords();
#endif