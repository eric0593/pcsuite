// DialupToolDlg.h : header file
//

#if !defined(AFX_DIALUPTOOLDLG_H__EE8B85B6_2979_47C1_941C_EB97FBFC72B3__INCLUDED_)
#define AFX_DIALUPTOOLDLG_H__EE8B85B6_2979_47C1_941C_EB97FBFC72B3__INCLUDED_

#include "RgnDialog.h"
#include "ExtendMenu.h"
#include "ExtendButton.h"
#include "afxwin.h"
#include "StaticTrans.h"
#include "RS232.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CDialupKitDlg dialog

class CDialupKitDlg : public CRgnDialog
{
// Construction
public:
	CDialupKitDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDialupKitDlg)
	enum { IDD = IDD_DIALOG_DIALUPTOOL};
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialupKitDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

private:
	CBitmap  m_Bitmap;
	CBitmap  m_Bitmap1;
	UINT	 TrayIcon;
	CRS232	 *m_pRS232;
	//TDialStatus	m_Dialstatus;

// Implementation
protected:
	HICON m_hIcon;
	CExtendMenu m_hMenu;

	// Generated message map functions
	//{{AFX_MSG(CDialupKitDlg)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC );
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnDeviceChange(UINT nEventType, DWORD dwData);
	afx_msg void OnDestroy();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT nIDEvent); 
	afx_msg void OnClose();
	afx_msg void OnFontChange( );
	afx_msg LRESULT OnGetDialStatus(WPARAM wParam, LPARAM lParam); 	// 拨号后的状态，这个是消息处理函数，对应系统消息:WM_RAS_GETTINGDIALSTATUS
	afx_msg void OnMenuConnectionSetting();
	afx_msg void OnMenuProfile();
	afx_msg void OnMenuHelp();
	afx_msg void OnMenuAbout();
	afx_msg void OnMenulanguage(UINT nID);
	afx_msg void OnMenuExit();
	afx_msg void OnMenuOpen();
	void SetTrayIcon(int dwMessage, UINT uID);
	BOOL Insertmenu(CMenu* pMenu, UINT nIDNewItem, UINT nStrId);
	CString GetText(UINT nStrId);
	int OpenCom();
	int CloseCom();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnBnClickedButtonControl();
	CExtendButton m_bControl;
	CStaticTrans m_TState;
	CExtendButton m_bSetting;
	afx_msg void OnBnClickedSetting();
	CExtendButton m_bHelp;
	afx_msg void OnBnClickedHelp();
	afx_msg void OnBnClickedExit();
	afx_msg void OnBnClickedMin();
	afx_msg void OnBnClickedExit1();
	LRESULT OnTrayNotify(WPARAM wParam,LPARAM lParam);//wParam接收的是图标的ID，而lParam接收的是鼠标的行为
	LRESULT HandleCommMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnBnClickedButton1();
	void CStaticSetText(UINT NID, CString Str);
	void CreateNewFone();
	void GetSpeed();
	CString FormatNumberPointedOff(int n);
	void Disconnect();
	void ReleaseTemp();	
	void SetLangueAndApn(CString StrCountry);	
	void GetInformation(char* pBuff);
	CStaticTrans m_HSTATUS1;
	CStaticTrans m_HSTATUS2;
	CStaticTrans m_HSTATUS3;
	CStaticTrans m_Signal1;
	CStaticTrans m_Signal2;
	CStatic m_bSingnalbitmap;

	CStaticTrans m_Speed;
	CTimeSpan m_tsDuration;
	CExtendButton m_bUsage;
	CExtendButton m_bInq;

	int 	m_Show;	//是否可以显示主窗体
	int		m_Check;//用来判断当前是否在查询状态，充电开机时使用
	BOOL	Isbigtypeface;//判断是否是大字体
public:
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);
	
public:
	afx_msg void OnBnClickedButtonInq();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALUPTOOLDLG_H__EE8B85B6_2979_47C1_941C_EB97FBFC72B3__INCLUDED_)
