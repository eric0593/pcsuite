// CUsageDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "DialupKit.h"
#include "CUsageDlg.h"
#include ".\cusagedlg.h"
#include "DialupKitDlg.h"
#include "ConnectionHistory.h"
#include "common.h"

// CCUsageDlg 对话框
extern CFont Bigfont;
extern int   Receive;
extern int   Transmit;
extern CFont Newfont;
extern BOOL m_IsAddFone;//判断是否要添加字体
extern RECT	DlgOnScr;
extern BOOL	IsResizeUsage; //用来判断主窗口回复大小时是否要同时回复usage
extern CString	MSISDN;

IMPLEMENT_DYNAMIC(CCUsageDlg, CDialog)
CCUsageDlg::CCUsageDlg(CWnd* pParent /*=NULL*/)
	: CRgnDialog(CCUsageDlg::IDD, pParent)
{
}

CCUsageDlg::~CCUsageDlg()
{
}

void CCUsageDlg::DoDataExchange(CDataExchange* pDX)
{
	CRgnDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_UPLOAD1, m_SUpload1);
	DDX_Control(pDX, IDC_UPLOAD2, m_SUpload2);
	DDX_Control(pDX, IDC_DOWNLOAD1, m_SDownload1);
	DDX_Control(pDX, IDC_DOWNLOAD2, m_SDownload2);
	DDX_Control(pDX, IDC_TITLE_USAGE, m_sUsage);
	DDX_Control(pDX, IDC_TOTAL2, m_sTatol2);
	DDX_Control(pDX, IDC_CLEAR, m_bClear);
	DDX_Control(pDX, IDC_CONN, m_BConn);
	DDX_Control(pDX, IDC_Histroy, m_bHistory);
	DDX_Control(pDX, IDC_UP6, m_SUp6);
	DDX_Control(pDX, IDC_UP5, m_SUp5);
	DDX_Control(pDX, IDC_UP4, m_SUp4);
	DDX_Control(pDX, IDC_UP3, m_SUp3);
	DDX_Control(pDX, IDC_UP2, m_SUp2);
	DDX_Control(pDX, IDC_UP1, m_SUp1);
	DDX_Control(pDX, IDC_USAGE_TITLE, m_STitle);
	DDX_Control(pDX, IDC_UP0, m_SUp0);
	DDX_Control(pDX, IDC_DOWN0, m_SDown0);
	DDX_Control(pDX, IDC_DOWN1, m_SDown1);
	DDX_Control(pDX, IDC_DOWN2, m_SDown2);
	DDX_Control(pDX, IDC_DOWN3, m_SDown3);
	DDX_Control(pDX, IDC_DOWN4, m_SDown4);
	DDX_Control(pDX, IDC_DOWN5, m_SDown5);
	DDX_Control(pDX, IDC_DOWN6, m_SDown6);
	DDX_Control(pDX, IDC_MSISDN, m_MSISDN);
}


BEGIN_MESSAGE_MAP(CCUsageDlg, CRgnDialog)
	ON_WM_ERASEBKGND()	
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_CONN, OnBnClickedConn)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_Histroy, OnBnClickedHistroy)
	ON_BN_CLICKED(IDC_CLEAR, OnBnClickedClear)
END_MESSAGE_MAP()


// CCUsageDlg 消息处理程序
BOOL CCUsageDlg::OnInitDialog()
{
	CRect  ClientRect;
	CString Totalstr;

	//m_Bitmap.LoadBitmap(IDB_USAGEBK);
	//SetBackgroundBitmap(&m_Bitmap,RGB(255,0,255));

	CRgnDialog::OnInitDialog();

	GetClientRect(&ClientRect);
	TRACE("CUsageDlg::ClientRect(x = %d,y = %d,dx = %d,dy = %d)\n",ClientRect.left,ClientRect.top,ClientRect.Width(),ClientRect.Height());

	if (m_IsAddFone == FALSE)
	{
		m_SUp0.SetFont(&Newfont);
		m_SUp1.SetFont(&Newfont);
		m_SUp2.SetFont(&Newfont);
		m_SUp3.SetFont(&Newfont);
		m_SUp4.SetFont(&Newfont);
		m_SUp5.SetFont(&Newfont);
		m_SUp6.SetFont(&Newfont);
		m_SDown0.SetFont(&Newfont);
		m_SDown1.SetFont(&Newfont);
		m_SDown2.SetFont(&Newfont);
		m_SDown3.SetFont(&Newfont);
		m_SDown4.SetFont(&Newfont);
		m_SDown5.SetFont(&Newfont);
		m_SDown6.SetFont(&Newfont);

		m_bHistory.SetFont(&Newfont);
		m_bClear.SetFont(&Newfont);
		m_BConn.SetFont(&Newfont);

		m_SUpload1.SetFont(&Newfont);
		m_SDownload1.SetFont(&Newfont);
		m_SUpload2.SetFont(&Newfont);
		m_SDownload2.SetFont(&Newfont);
		m_sTatol2.SetFont(&Newfont);		
	}

	
	SetWindowText(GetText(T_TITLE_USAGE));
	/*m_STitle.SetWindowText(GetText(T_TITLE_USAGE));
	m_STitle.SetTextColor(RGB(0,0,0));
	m_STitle.SetFont(&Bigfont);*/
		
	m_bHistory.SetBitmaps(IDB_BUTTONBK,RGB(255,0,255),0,0);
	m_bHistory.SetWindowText(GetText(T_CONNECTION));
	m_bHistory.SetAlign(CExtendButton::ST_ALIGN_OVERLAP);
	m_bHistory.DrawBorder(FALSE,FALSE);
	m_bHistory.DrawTransparent(TRUE);

	m_bClear.SetBitmaps(IDB_BUTTONBK,RGB(255,0,255),0,0);
	m_bClear.SetWindowText(GetText(T_CLEAR));
	m_bClear.SetAlign(CExtendButton::ST_ALIGN_OVERLAP);
	m_bClear.DrawBorder(FALSE,FALSE);
	m_bClear.DrawTransparent(TRUE);
	
	m_BConn.SetBitmaps(IDB_BUTTONBK,RGB(255,0,255),0,0);
	m_BConn.SetWindowText(GetText(T_MY3));
	m_BConn.SetAlign(CExtendButton::ST_ALIGN_OVERLAP);
	m_BConn.DrawBorder(FALSE,FALSE);
	m_BConn.DrawTransparent(TRUE);
	
	m_sUsage.SetWindowText(GetText(T_TITLE_USAGE));
	m_sUsage.SetTextColor(RGB(255,255,255));
	m_sUsage.SetFont(&Bigfont);
	m_SUpload1.SetWindowText(GetText(T_UPLOADED));
	m_SDownload1.SetWindowText(GetText(T_DOWNLOADED));

	m_SUp0.SetBackColor(TRANS_BACK);
	m_SUp1.SetBackColor(TRANS_BACK);
	m_SUp2.SetBackColor(TRANS_BACK);
	m_SUp3.SetBackColor(TRANS_BACK);
	m_SUp4.SetBackColor(TRANS_BACK);
	m_SUp5.SetBackColor(TRANS_BACK);
	m_SUp6.SetBackColor(TRANS_BACK);
	m_SDown0.SetBackColor(TRANS_BACK);
	m_SDown1.SetBackColor(TRANS_BACK);
	m_SDown2.SetBackColor(TRANS_BACK);
	m_SDown3.SetBackColor(TRANS_BACK);
	m_SDown4.SetBackColor(TRANS_BACK);
	m_SDown5.SetBackColor(TRANS_BACK);
	m_SDown6.SetBackColor(TRANS_BACK);
	m_MSISDN.SetBackColor(TRANS_BACK);

	m_MSISDN.SetFont(&Newfont);
	m_MSISDN.SetWindowText(L"MSISDN:\r " + MSISDN);
	
	m_nRx = Receive;
	m_nTx = Transmit;
	NumberType = 1;
	SetTimer(TIMER_SPEED_CHANGE, DETECT_CONNECTION_ELAPSE, NULL);
	m_SUpload2.SetWindowText(FormatNumber(m_nTx));
	m_SDownload2.SetWindowText(FormatNumber(m_nRx));
	Totalstr.Format(_T("%s%s"), GetText(T_USAGETOTAL),FormatNumber(m_nRx));
	m_sTatol2.SetWindowText(Totalstr);//(FormatNumber(m_nTx+m_nRx));
	m_sTatol2.SetTextColor(RGB(255,255,255));
	m_sTatol2.SetFont(&Bigfont);
	
	SetUpandDown(m_nTx, m_nRx);

	//获取信息当前使用的是大字体还是小字体
	CWnd *pWnd = AfxGetMainWnd();			 // Get main window
	CDC *pDC = pWnd->GetDC();			   // Get device context
	int   nLogPixelsx = pDC->GetDeviceCaps(LOGPIXELSX);   
	Isbigtypeface = FALSE;
	if (nLogPixelsx >= 120)
		Isbigtypeface = TRUE;
	
	pWnd->ReleaseDC(pDC);

	//控制usga窗口显示的位置
	RECT	rDlgOnScr;
	int   y;   
	int   cy   =   GetSystemMetrics(   SM_CYSCREEN	 );
	GetWindowRect(&rDlgOnScr);

	if (Isbigtypeface)
	{
		y = DlgOnScr.top - (rDlgOnScr.bottom - rDlgOnScr.top);
		if (y <= 0)
			y = DlgOnScr.top + (rDlgOnScr.bottom - rDlgOnScr.top);
	}
	else
	{
		if ((rDlgOnScr.bottom + 200) > cy)
			y = DlgOnScr.top - (rDlgOnScr.bottom - rDlgOnScr.top);
		else
			y = DlgOnScr.top + (rDlgOnScr.bottom - rDlgOnScr.top);
	}
	
	SetWindowPos(NULL, rDlgOnScr.left,y,0,0,SWP_SHOWWINDOW|SWP_NOSIZE);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCUsageDlg::OnBnClickedConn()
{
	// TODO: 在此添加控件通知处理程序代码
	CString page = ((CDialupKitApp*)AfxGetApp())->m_pDialPram->szHomepage;

	ShellExecute(NULL,   _T("open"),   _T("iexplore"),   page,   NULL,   SW_SHOWNORMAL); 
}

CString CCUsageDlg::GetText(UINT nStrId)
{
	CString str;
	
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	str=pApp->g_GetText(nStrId,pApp->m_nLandId);

	return str;
}

CString CCUsageDlg::FormatNumber(int n)
{
	CString str;

	if (n > (1024*1024*1024)) //G
	{
		str.Format(_T("%d%s"), n/(1024*1024),_T("GB"));
	}
	else if (n > (1024*1024)) //M
	{
		str.Format(_T("%d%s"), n/1024,_T("MB"));
	}
	else if (n > 1024) //K
	{
		str.Format(_T("%d%s"), n,_T("KB"));
	}
	else
	{
		if (n <= 0)
			n = 0;
		
		str.Format(_T("%d%s"), n,_T("B"));
		return str;
	}
	
	
	int len = str.GetLength();
	if (len > 5) 
		str.Insert(len - 5, _T('.')); 

	TRACE(_T("FormatNumber(%d): %s\n"), n, str);
	return str;
}

void CCUsageDlg::OnPaint()
{

	CRgnDialog::OnPaint();

	DrawRect();
	
}

void CCUsageDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(TIMER_SPEED_CHANGE == nIDEvent)
	{
		CRect	 Rect;
		CString Totalstr;
		
		if (m_nRx == Receive && m_nTx == Transmit)
			return;
		
		m_nRx = Receive;
		m_nTx = Transmit;		

		CStaticSetText(IDC_UPLOAD2, FormatNumber(m_nTx));
		CStaticSetText(IDC_DOWNLOAD2, FormatNumber(m_nRx));
		Totalstr.Format(_T("%s%s"), GetText(T_USAGETOTAL),FormatNumber(m_nRx));
		CStaticSetText(IDC_TOTAL2, Totalstr);
		SetUpandDown(m_nTx, m_nRx);
		
		GetClientRect(Rect);
		if (Isbigtypeface)
			Rect.left = Rect.left + 420;
		else
			Rect.left = Rect.left + 350;
		Invalidate();
		ValidateRect(&Rect);
		UpdateWindow();
	}
}

void CCUsageDlg::DrawRect()
{
	
	CBrush   BrushBlue(RGB(0, 216, 0));
	CPen     PenBlack;
	CRect    Rect,Rect1;
	CDC      *pDC = GetDC();

	CBrush* pOldBrush = pDC->SelectObject(&BrushBlue);
	
	PenBlack.CreatePen(PS_NULL, 0, RGB(255, 255, 255));
	CPen* pOldPen = pDC->SelectObject(&PenBlack);

	GetClientRect(Rect);

#if 0	
	//左图 上行流量 箭头绘制
	if (m_nTx > 0)
	{
		CPoint	m_pt[3];  
		CRgn rgn;
		int  Upload;

		if (m_nTx <= (10 * 1024 * 1024))
		{	//只有箭头，固定大小
			m_pt[0].x = Rect.left + 120;
			m_pt[0].y = Rect.top + 261;
			m_pt[1].x = Rect.left + 65;
			m_pt[1].y = Rect.top + 260;
			m_pt[2].x =Rect.left + 175;
			m_pt[2].y = Rect.top + 260;
			rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
			pDC->PaintRgn( &rgn );
		}
		else
		{
			Upload = m_nTx/(1024*1024);
	
			if (Upload <= 200)

			{
				//只有箭头，大小变化
				m_pt[0].x = Rect.left + 120;
				m_pt[0].y = Rect.top + 260 - Upload/10;
				m_pt[1].x = Rect.left + 65;
				m_pt[1].y = Rect.top + 260;
				m_pt[2].x =Rect.left + 175;
				m_pt[2].y = Rect.top + 260;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else
			{
				Rect1.left = Rect.left + 90;
				Rect1.bottom = Rect.top + 260;
				Rect1.top  = Rect1.bottom - Upload/34;
				Rect1.right = Rect1.left + 60;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 30;
				m_pt[0].y = Rect1.top - 20;
				m_pt[1].x = Rect1.right + 25;
				m_pt[1].y = Rect1.top + 5;
				m_pt[2].x = Rect1.left - 25;
				m_pt[2].y = Rect1.top + 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
		}	
	}/*upload箭头处理*/

	//右图 下载流量 箭头绘制
	if (m_nRx > 0)
	{
		CPoint	m_pt[3];  
		CRgn rgn;
		int  Download;
		
		if (m_nRx <= (10 * 1024 * 1024))
		{	//只有箭头，固定大小
			m_pt[0].x = Rect.left + 300;
			m_pt[0].y = Rect.top + 101;
			m_pt[1].x = Rect.left + 255;
			m_pt[1].y = Rect.top + 100;
			m_pt[2].x =Rect.left + 345;
			m_pt[2].y = Rect.top + 100;
			rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
			pDC->PaintRgn( &rgn );
		}
		else
		{
			Download = m_nRx/(1024*1024);

			if (Download <= 200)

			{
				//只有箭头，大小变化
				m_pt[0].x = Rect.left + 300;
				m_pt[0].y = Rect.top + 100 + Download/10;
				m_pt[1].x = Rect.left + 255;
				m_pt[1].y = Rect.top + 100;
				m_pt[2].x =Rect.left + 345;
				m_pt[2].y = Rect.top + 100;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else
			{
				Rect1.left = Rect.left + 270;
				Rect1.top  = Rect.top + 100;
				Rect1.right = Rect1.left + 60;
				Rect1.bottom = Rect1.top + Download/34;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 30;
				m_pt[0].y = Rect1.bottom + 20;
				m_pt[1].x = Rect1.right + 25;
				m_pt[1].y = Rect1.bottom - 5;
				m_pt[2].x = Rect1.left - 25;
				m_pt[2].y = Rect1.bottom - 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
		}	
	}/*download箭头处理*/
#else
if (Isbigtypeface)
{
	//左图 上行流量 箭头绘制

	if (m_nTx > 0)
	{
		CPoint	m_pt[3];  
		CRgn rgn;
		int  Upload;

		if (m_nTx <= 2*(1024 * 1024))
		{	//只有箭头，固定大小
			m_pt[0].x = Rect.left + 137;
			m_pt[0].y = Rect.top + 302;
			m_pt[1].x = Rect.left + 72;
			m_pt[1].y = Rect.top + 306;
			m_pt[2].x =Rect.left + 202;
			m_pt[2].y = Rect.top + 306;
			rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
			pDC->PaintRgn( &rgn );
		}
		else
		{
			Upload = m_nTx/(1024*1024);

			if (Upload <= 20)

			{
				//只有箭头，大小变化
				m_pt[0].x = Rect.left + 137;
				m_pt[0].y = Rect.top + 306 - Upload;
				m_pt[1].x = Rect.left + 72;
				m_pt[1].y = Rect.top + 306;
				m_pt[2].x =Rect.left + 202;
				m_pt[2].y = Rect.top + 306;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else if(Upload <= 500) 
			{
				Rect1.left = Rect.left + 104;
				Rect1.bottom = Rect.top + 306;
				Rect1.top  = Rect1.bottom - Upload/3;
				Rect1.right = Rect1.left + 70;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 35;
				m_pt[0].y = Rect1.top - 24;
				m_pt[1].x = Rect1.right + 30;
				m_pt[1].y = Rect1.top + 5;
				m_pt[2].x = Rect1.left - 30;
				m_pt[2].y = Rect1.top + 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else if(Upload <=1024)
			{
				Rect1.left = Rect.left + 104;
				Rect1.bottom = Rect.top + 306;
				Rect1.top  = Rect1.bottom - Upload/6.2;
				Rect1.right = Rect1.left + 70;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 35;
				m_pt[0].y = Rect1.top - 24;
				m_pt[1].x = Rect1.right + 30;
				m_pt[1].y = Rect1.top + 5;
				m_pt[2].x = Rect1.left - 30;
				m_pt[2].y = Rect1.top + 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else
			{
				Rect1.left = Rect.left + 104;
				Rect1.bottom = Rect.top + 306;
				Rect1.top  = Rect1.bottom - Upload/32;
				Rect1.right = Rect1.left + 70;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 35;
				m_pt[0].y = Rect1.top - 24;
				m_pt[1].x = Rect1.right + 30;
				m_pt[1].y = Rect1.top + 5;
				m_pt[2].x = Rect1.left - 30;
				m_pt[2].y = Rect1.top + 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
		}	
	}/*upload箭头处理*/

	//右图 下载流量 箭头绘制
	if (m_nRx > 0)
	{
		CPoint	m_pt[3];  
		CRgn rgn;
		int  Download;
		
		if (m_nRx <= (1024 * 1024))
		{	//只有箭头，固定大小
			m_pt[0].x = Rect.left + 320;
			m_pt[0].y = Rect.top + 125;
			m_pt[1].x = Rect.left + 260;
			m_pt[1].y = Rect.top + 123;
			m_pt[2].x =Rect.left + 380;
			m_pt[2].y = Rect.top + 123;
			rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
			pDC->PaintRgn( &rgn );
		}
		else
		{
			Download = m_nRx/(1024*1024);

			if (Download <= 20)
			{
				//只有箭头，大小变化
				m_pt[0].x = Rect.left + 320;
				m_pt[0].y = Rect.top + 123 + 24;
				m_pt[1].x = Rect.left + 260;
				m_pt[1].y = Rect.top + 123;
				m_pt[2].x =Rect.left + 380;
				m_pt[2].y = Rect.top + 123;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else if(Download <= 500)
			{
				Rect1.left = Rect.left + 285;
				Rect1.top  = Rect.top + 123;
				Rect1.right = Rect1.left + 70;
				Rect1.bottom = Rect1.top + Download/3;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 35;
				m_pt[0].y = Rect1.bottom + 24;
				m_pt[1].x = Rect1.right + 30;
				m_pt[1].y = Rect1.bottom - 5;
				m_pt[2].x = Rect1.left - 26;
				m_pt[2].y = Rect1.bottom - 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else if(Download <= 1024)
			{
				Rect1.left = Rect.left + 285;
				Rect1.top  = Rect.top + 123;
				Rect1.right = Rect1.left + 70;
				Rect1.bottom = Rect1.top + Download/6.2;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 35;
				m_pt[0].y = Rect1.bottom + 24;
				m_pt[1].x = Rect1.right + 30;
				m_pt[1].y = Rect1.bottom - 5;
				m_pt[2].x = Rect1.left - 26;
				m_pt[2].y = Rect1.bottom - 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else
			{
				Rect1.left = Rect.left + 285;
				Rect1.top  = Rect.top + 123;
				Rect1.right = Rect1.left + 70;
				Rect1.bottom = Rect1.top + Download/32;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 35;
				m_pt[0].y = Rect1.bottom + 24;
				m_pt[1].x = Rect1.right + 30;
				m_pt[1].y = Rect1.bottom - 5;
				m_pt[2].x = Rect1.left - 26;
				m_pt[2].y = Rect1.bottom - 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
		}	
	}/*download箭头处理*/
}
else
{
	//左图 上行流量 箭头绘制
	if (m_nTx > 0)
	{
		CPoint	m_pt[3];  
		CRgn rgn;
		int  Upload;
	
		if (m_nTx <= 2*(1024 * 1024))
		{	//只有箭头，固定大小
			m_pt[0].x = Rect.left + 117;
			m_pt[0].y = Rect.top + 249;
			m_pt[1].x = Rect.left + 62;
			m_pt[1].y = Rect.top + 248;
			m_pt[2].x =Rect.left + 172;
			m_pt[2].y = Rect.top + 248;
			rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
			pDC->PaintRgn( &rgn );
		}
		else
		{
			Upload = m_nTx/(1024*1024);
	
			if (Upload <= 20)
	
			{
				//只有箭头，大小变化
				m_pt[0].x = Rect.left + 117;
				m_pt[0].y = Rect.top + 248 - Upload;
				m_pt[1].x = Rect.left + 62;
				m_pt[1].y = Rect.top + 248;
				m_pt[2].x =Rect.left + 172;
				m_pt[2].y = Rect.top + 248;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else if(Upload <= 500) 
			{
				Rect1.left = Rect.left + 87;
				Rect1.bottom = Rect.top + 248;
				Rect1.top  = Rect1.bottom - Upload/3.8;
				Rect1.right = Rect1.left + 60;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 30;
				m_pt[0].y = Rect1.top - 20;
				m_pt[1].x = Rect1.right + 25;
				m_pt[1].y = Rect1.top + 5;
				m_pt[2].x = Rect1.left - 25;
				m_pt[2].y = Rect1.top + 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else if(Upload <=1024)
			{
				Rect1.left = Rect.left + 87;
				Rect1.bottom = Rect.top + 248;
				Rect1.top  = Rect1.bottom - Upload/7.6;
				Rect1.right = Rect1.left + 60;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 30;
				m_pt[0].y = Rect1.top - 20;
				m_pt[1].x = Rect1.right + 25;
				m_pt[1].y = Rect1.top + 5;
				m_pt[2].x = Rect1.left - 25;
				m_pt[2].y = Rect1.top + 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else
			{
				Rect1.left = Rect.left + 87;
				Rect1.bottom = Rect.top + 248;
				Rect1.top  = Rect1.bottom - Upload/38;
				Rect1.right = Rect1.left + 60;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 30;
				m_pt[0].y = Rect1.top - 20;
				m_pt[1].x = Rect1.right + 25;
				m_pt[1].y = Rect1.top + 5;
				m_pt[2].x = Rect1.left - 25;
				m_pt[2].y = Rect1.top + 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
		}	
	}/*upload箭头处理*/
	
	//右图 下载流量 箭头绘制
	if (m_nRx > 0)
	{
		CPoint	m_pt[3];  
		CRgn rgn;
		int  Download;
		
		if (m_nRx <= (1024 * 1024))
		{	//只有箭头，固定大小
			m_pt[0].x = Rect.left + 275;
			m_pt[0].y = Rect.top + 99;
			m_pt[1].x = Rect.left + 230;
			m_pt[1].y = Rect.top + 98;
			m_pt[2].x =Rect.left + 320;
			m_pt[2].y = Rect.top + 98;
			rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
			pDC->PaintRgn( &rgn );
		}
		else
		{
			Download = m_nRx/(1024*1024);
	
			if (Download <= 20)
			{
				//只有箭头，大小变化
				m_pt[0].x = Rect.left + 275;
				m_pt[0].y = Rect.top + 98 + Download;
				m_pt[1].x = Rect.left + 230;
				m_pt[1].y = Rect.top + 98;
				m_pt[2].x =Rect.left + 320;
				m_pt[2].y = Rect.top + 98;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else if(Download <= 500)
			{
				Rect1.left = Rect.left + 245;
				Rect1.top  = Rect.top + 98;
				Rect1.right = Rect1.left + 60;
				Rect1.bottom = Rect1.top + Download/3.8;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 30;
				m_pt[0].y = Rect1.bottom + 20;
				m_pt[1].x = Rect1.right + 25;
				m_pt[1].y = Rect1.bottom - 5;
				m_pt[2].x = Rect1.left - 25;
				m_pt[2].y = Rect1.bottom - 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else if(Download <= 1024)
			{
				Rect1.left = Rect.left + 245;
				Rect1.top  = Rect.top + 98;
				Rect1.right = Rect1.left + 60;
				Rect1.bottom = Rect1.top + Download/7.6;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 30;
				m_pt[0].y = Rect1.bottom + 20;
				m_pt[1].x = Rect1.right + 25;
				m_pt[1].y = Rect1.bottom - 5;
				m_pt[2].x = Rect1.left - 25;
				m_pt[2].y = Rect1.bottom - 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
			else
			{
				Rect1.left = Rect.left + 245;
				Rect1.top  = Rect.top + 98;
				Rect1.right = Rect1.left + 60;
				Rect1.bottom = Rect1.top + Download/38;
				pDC->Rectangle(&Rect1);
				
				m_pt[0].x = Rect1.left + 30;
				m_pt[0].y = Rect1.bottom + 20;
				m_pt[1].x = Rect1.right + 25;
				m_pt[1].y = Rect1.bottom - 5;
				m_pt[2].x = Rect1.left - 25;
				m_pt[2].y = Rect1.bottom - 5;
				rgn.CreatePolygonRgn( m_pt, 3, ALTERNATE );
				pDC->PaintRgn( &rgn );
			}
		}	
	}/*download箭头处理*/
}

#endif
		
	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);

	BrushBlue.DeleteObject();
	PenBlack.DeleteObject();

	ReleaseDC(pDC);
}

void CCUsageDlg::CStaticSetText(UINT NID, CString Str)
{
	int 	nTitleHeight = 0, nFrameWidth = 0;
	RECT	rDlgOnScr, rDlgClient, rCtrlOnScr, rCtrlClient;
	CWnd	*pWnd		 = GetDlgItem(NID);

	if (pWnd != NULL)
	{
		pWnd->SetWindowText(Str);
		
		pWnd->GetWindowRect(&rCtrlOnScr);
		
		GetWindowRect(&rDlgOnScr);
		
		(pWnd->GetParent())->GetClientRect(&rDlgClient);
		
		nTitleHeight = rDlgOnScr.bottom - rDlgOnScr.top  - rDlgClient.bottom;
		nFrameWidth  = (rDlgOnScr.right - rDlgOnScr.left - rDlgClient.right) / 2;
		
		rCtrlOnScr.right  = rCtrlOnScr.right - rDlgOnScr.left;
		rCtrlOnScr.bottom = rCtrlOnScr.bottom - rDlgOnScr.top;
		
		pWnd->GetClientRect(&rCtrlClient);
		rCtrlOnScr.left   = rCtrlOnScr.right - rCtrlClient.right - nFrameWidth;
		rCtrlOnScr.top	  = rCtrlOnScr.bottom - rCtrlClient.bottom - nTitleHeight;
		
		RedrawWindow(&rCtrlOnScr);
	}
	
}

void CCUsageDlg::OnBnClickedHistroy()
{
	// TODO: 在此添加控件通知处理程序代码
	
	/*CTimeSpan timeSpan;

	KillTimer(TIMER_SPEED_CHANGE);
	GetHistoryTotalStatistic(timeSpan, m_nTx, m_nRx);
	NumberType = 0;
	CStaticSetText(IDC_UPLOAD2, FormatNumber(m_nTx));
	CStaticSetText(IDC_DOWNLOAD2, FormatNumber(m_nRx));
	CStaticSetText(IDC_TOTAL2, FormatNumber(m_nTx+m_nRx));
	
	Invalidate();
	UpdateWindow();*/
	CRgnDialog::OnOK();
}

void CCUsageDlg::OnBnClickedClear()
{
	// TODO: 在此添加控件通知处理程序代码
	CString Totalstr;
	
	if (Receive ==0 && Transmit == 0)
		return;
	
	if (NumberType == 0)
	{
		ClearAllHistoryRecords();
	}
	else
	{
		Receive = 0;
		Transmit = 0;
	}
	m_nRx = 0;
	m_nTx = 0;
	CStaticSetText(IDC_UPLOAD2, FormatNumber(m_nTx));
	CStaticSetText(IDC_DOWNLOAD2, FormatNumber(m_nRx));
	Totalstr.Format(_T("%s%s"), GetText(T_USAGETOTAL),FormatNumber(m_nRx));
	CStaticSetText(IDC_TOTAL2, Totalstr);
	
	Invalidate();
	UpdateWindow();
}

void CCUsageDlg::SetUpandDown(int tx,int Rx)
{

	int  Upload,DownLoad;
	//upload
	Upload = tx/(1024*1024);
	if (Upload < 500)
	{
		CStaticSetText(IDC_UP1, L"20MB");
		CStaticSetText(IDC_UP2, L"100MB");
		CStaticSetText(IDC_UP3, L"200MB");
		CStaticSetText(IDC_UP4, L"300MB");
		CStaticSetText(IDC_UP5, L"400MB");
		CStaticSetText(IDC_UP6, L"500MB");
	}
	else if (Upload < 1024)
	{
		CStaticSetText(IDC_UP1, L"40MB");
		CStaticSetText(IDC_UP2, L"200MB");
		CStaticSetText(IDC_UP3, L"400MB");
		CStaticSetText(IDC_UP4, L"600MB");
		CStaticSetText(IDC_UP5, L"800MB");
		CStaticSetText(IDC_UP6, L"1GB");
	}
	else
	{
		CStaticSetText(IDC_UP1, L"200MB");
		CStaticSetText(IDC_UP2, L"1GB");
		CStaticSetText(IDC_UP3, L"2GB");
		CStaticSetText(IDC_UP4, L"3GB");
		CStaticSetText(IDC_UP5, L"4GB");
		CStaticSetText(IDC_UP6, L"5GB");
	}

	//Download
	DownLoad = Rx/(1024*1024);
	if (DownLoad < 500)
	{
		CStaticSetText(IDC_DOWN1, L"20MB");
		CStaticSetText(IDC_DOWN2, L"100MB");
		CStaticSetText(IDC_DOWN3, L"200MB");
		CStaticSetText(IDC_DOWN4, L"300MB");
		CStaticSetText(IDC_DOWN5, L"400MB");
		CStaticSetText(IDC_DOWN6, L"500MB");
	}
	else if (DownLoad < 1024)
	{
		CStaticSetText(IDC_DOWN1, L"40MB");
		CStaticSetText(IDC_DOWN2, L"200MB");
		CStaticSetText(IDC_DOWN3, L"400MB");
		CStaticSetText(IDC_DOWN4, L"600MB");
		CStaticSetText(IDC_DOWN5, L"800MB");
		CStaticSetText(IDC_DOWN6, L"1GB");
	}
	else
	{
		CStaticSetText(IDC_DOWN1, L"200MB");
		CStaticSetText(IDC_DOWN2, L"1GB");
		CStaticSetText(IDC_DOWN3, L"2GB");
		CStaticSetText(IDC_DOWN4, L"3GB");
		CStaticSetText(IDC_DOWN5, L"4GB");
		CStaticSetText(IDC_DOWN6, L"5GB");
	}
}

BOOL CCUsageDlg::OnEraseBkgnd(CDC* pDC )
{
	CBitmap    bBitmap;
	CDC 	   tempDC;	 
	CRect	   rect;   
	BITMAP		Bmp;

	bBitmap.LoadBitmap(IDB_USAGEBK);

	tempDC.CreateCompatibleDC(pDC);

	tempDC.SelectObject(&bBitmap);

	GetClientRect(&rect);
	
	bBitmap.GetBitmap(&Bmp);

	pDC->StretchBlt(0, 0, rect.Width(), rect.Height(), &tempDC, 0, 0, Bmp.bmWidth, Bmp.bmHeight, SRCCOPY);

	bBitmap.DeleteObject();

	return 0;
}

void CCUsageDlg::OnSize(   UINT nType,int cx,int cy)
{
	if (nType == SIZE_MINIMIZED)	
	{
		CWnd *pWnd = AfxGetMainWnd();			 // Get main window
		pWnd->ShowWindow(SW_MINIMIZE);

		IsResizeUsage = TRUE;
		CDialog::OnCancel();
	}
}

