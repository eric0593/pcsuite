#include "RgnDialog.h"
#include "ExtendMenu.h"
#include "ExtendButton.h"
#include "afxwin.h"
#include "StaticTrans.h"

#pragma once


// CCUsageDlg 对话框

class CCUsageDlg : public CRgnDialog
{
	DECLARE_DYNAMIC(CCUsageDlg)

public:
	CCUsageDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCUsageDlg();

// 对话框数据
	enum { IDD = IDD_USAGE };

private:
	CBitmap  m_Bitmap;

protected:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC );
	afx_msg void OnSize(   UINT nType,int cx,int cy);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedExitUsage();
	afx_msg void OnBnClickedMinUsage();
	afx_msg void OnBnClickedConn();
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent); 
	CString GetText(UINT nStrId);
	CString FormatNumber(int n);
	void DrawRect();
	void CStaticSetText(UINT NID, CString Str);
	void SetUpandDown(int tx,int Rx);
	
	int m_nTx;
	int m_nRx;
	int NumberType;					//用来表示当前显示的流量类型，0历史信息，1当前信息
	BOOL	Isbigtypeface;//判断是否是大字体
	CStaticTrans m_SUpload1;
	CStaticTrans m_SUpload2;
	CStaticTrans m_SDownload1;
	CStaticTrans m_SDownload2;
	CStaticTrans m_sUsage;
	CStaticTrans m_sTatol2;
	CExtendButton m_BConn;
	CExtendButton m_bClear;
	CExtendButton m_bHistory;
	afx_msg void OnBnClickedHistroy();
	afx_msg void OnBnClickedClear();
	CStaticTrans m_SUp6;
	CStaticTrans m_SUp5;
	CStaticTrans m_SUp4;
	CStaticTrans m_SUp3;
	CStaticTrans m_SUp2;
	CStaticTrans m_SUp1;
	CStaticTrans m_STitle;
	CStaticTrans m_SUp0;
	CStaticTrans m_SDown0;
	CStaticTrans m_SDown1;
	CStaticTrans m_SDown2;
	CStaticTrans m_SDown3;
	CStaticTrans m_SDown4;
	CStaticTrans m_SDown5;
	CStaticTrans m_SDown6;
	CStaticTrans m_MSISDN;
};
