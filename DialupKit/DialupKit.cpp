// DialupKit.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "DialupKit.h"
#include "DialupKitDlg.h"
#include "Common.h"
#include <afxdb.h>
#include <odbcinst.h>
#include "atlbase.h"
#include "SelCountry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifndef FREEIF
#define FREEIF(Point) if (Point)\
                        {\
                            free(Point);\
							Point=NULL;\
                        }
#endif

/////////////////////////////////////////////////////////////////////////////
// CDialupKitApp
#define MAX_VALUE_LEN 128
#define CLASS_AMOI_DIALINGKIT_CLASS_NAME  (_T("INQ1 Mobile Modem."))  
BEGIN_MESSAGE_MAP(CDialupKitApp, CWinApp)
	//{{AFX_MSG_MAP(CDialupKitApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialupKitApp construction
int MainDlgStatue = 0; //0,不存在;,1，机会状态;2,holde状态

CDialupKitApp::CDialupKitApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
		m_pIdMapStr = new CMapWordToOb;
		m_pResConverion = NULL;
		m_pResConverion = new CResConverion();
}

//de construct function
CDialupKitApp:: ~CDialupKitApp()
{
    WORD key;
    CStringArray *pstraValue; 
    POSITION pos = m_pIdMapStr->GetStartPosition();

	if(m_pDialPram != NULL)
	{
		
		FREEIF(m_pDialPram->szAPN);
		FREEIF(m_pDialPram->szCountry);
		FREEIF(m_pDialPram->szHomepage);
		FREEIF(m_pDialPram->szIPAddress);
		FREEIF(m_pDialPram->szPassword);
		FREEIF(m_pDialPram->szPhoneNumber);
		FREEIF(m_pDialPram->szUserName);

		delete m_pDialPram;
	}
    while( pos != NULL )
    {
        int i = 0;
        m_pIdMapStr->GetNextAssoc(pos, key, (CObject*&)pstraValue);
        /*while(i < pstraValue->GetSize())
        {
            delete (pstraValue->GetAt(i++));
        }*/
        //pstraValue->RemoveAll();
        delete pstraValue;
    }
    m_pIdMapStr->RemoveAll();
    delete m_pIdMapStr;

	if (m_pResConverion != NULL)
	{
		delete m_pResConverion;
		m_pResConverion = NULL;
	}


}

//AfxWinMain()

/////////////////////////////////////////////////////////////////////////////
// The one and only CDialupKitApp object

CDialupKitApp theApp;


/////////////////////////////////////////////////////////////////////////////
// CDialupKitApp initialization

BOOL CDialupKitApp::InitInstance()
{
	AfxEnableControlContainer();
    SetDialogBkColor(RGB(0Xff,0Xff, 0Xff), RGB(0x66,0x99,0xcc));

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.
	CWnd *pWndPrev, *pWndChild;  
	// Determine if another window with our class name exists...  
    if(pWndPrev = CWnd::FindWindow(NULL, L"INQ1 Mobile Modem."))  
    {  
		// If so, does it have any popups?  
        pWndChild = pWndPrev->GetLastActivePopup(); 
		
        // If iconic, restore the main window  
        //if (pWndPrev->IsIconic())   
        //    pWndPrev->ShowWindow(SW_RESTORE);
        //else
        pWndPrev->ShowWindow(SW_SHOWNORMAL);
		// Bring the main window or its popup to  the foreground  
        pWndChild->SetForegroundWindow(); // and we are done activating the previous one.  
        return FALSE;  
	}      
	
	///	m_bClassRegistered = TRUE;
	WNDCLASS wndcls;
    memset(&wndcls, 0, sizeof(WNDCLASS));
	::GetClassInfo(AfxGetInstanceHandle(),L"#32770",&wndcls);
    wndcls.lpszClassName = CLASS_AMOI_DIALINGKIT_CLASS_NAME;
    if(!AfxRegisterClass(&wndcls)) 
    { 
        TRACE(L"Class Registration Failed\n"); 
        return FALSE; 
    }
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CoInitialize(NULL);

	
	m_ImageList.Create (IDB_NETLINK, 20, 1, ILC_COLOR8);
 	GetAppRootDir(m_strRootDir);
	
	//读取设置ini
	CString szFile,szPath;

	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\ModemSetting.ini");

	//检测connection的三项设置
	ReadConnectionSet();
	TCHAR Return[255] = {0};
	CString szTemp;
	int temp = 0;
	
	if (GetPrivateProfileString(L"Setting",L"Auto",L"1",Return,255,szFile) == 0)
	{
		CString SetConn;
		SetConn.Format(_T("%d"),1);
		WritePrivateProfileString(L"Setting",L"Auto",SetConn,szFile);
	}
	else
	{
		szTemp.Format(L"%s",Return);
		temp = _ttoi(szTemp);
		if (temp == 1)
			n_Check1Value = TRUE;
		else
			n_Check1Value = FALSE;
	}
	
	if (GetPrivateProfileString(L"Setting",L"ConnSet",L"1",Return,255,szFile) == 0)
	{
		CString SetConn;
		SetConn.Format(_T("%d"),0);
		WritePrivateProfileString(L"Setting",L"ConnSet",SetConn,szFile);
	}
	else
	{
		szTemp.Format(L"%s",Return);
		temp = _ttoi(szTemp);
		ConnectionSet = temp;
	}
	
	//ReadExcel();
	//国家选择界面移到此处
	CString SzCountry;
	m_bGetlangandApn = FALSE;
	GetPrivateProfileString(L"DialupSet",L"Country",L"default",Return,255,szFile) ;
	SzCountry.Format(L"%s",Return);
	if(SzCountry.Find(L"default") != -1)
	{
		SelCountry dllg;

		m_nLandId = LANG_ENG;
		m_pResConverion->SetLanguage((TLanguage)m_nLandId);
		//dllg.DoModal(); 
		m_bGetlangandApn = TRUE;
	}
	else
	{
		int language;
		CString szTmp;

		GetPrivateProfileString(L"Language",L"Language",L"unknown",Return,255,szFile) ;		
		szTmp.Format(L"%s",Return);
		if (szTmp.Find(L"unknown") != -1)
		{
			m_nLandId = LANG_ENG;
		}
		else
		{
			language = _ttoi(szTmp);
			m_nLandId = (TLanguage)language;
		
			if (m_nLandId > LANG_MAX)
			{
				m_nLandId = LANG_ENG;
			}
		}
	}
		
	CDialupKitDlg dlg;
	m_pMainWnd = &dlg;

	int nResponse = dlg.DoModal();

	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

CString CDialupKitApp::g_GetText(UINT uID , int nLangID)
{
	BOOL bFound =FALSE;
	CString strvalue = _T("");
	CStringArray *pStrArr=NULL;
	WCHAR	*pText = NULL;
	if(nLangID<0)
		nLangID = 0;
	/*if(m_pIdMapStr)
	{
		
		bFound = m_pIdMapStr->Lookup(uID,(CObject*&)pStrArr);
		if(bFound&&pStrArr)
		{
			if(nLangID>=pStrArr->GetSize())
				nLangID = 0;
			strvalue = pStrArr->GetAt(nLangID);
			return strvalue;
		}
	} 
	strvalue.LoadString(uID);*/

	m_pResConverion->GetString(uID, &pText);
	strvalue.Format(L"%s",pText);
	free(pText);
	return strvalue;
}

CString GetExcelDriver()
{
	TCHAR szBuf[2001];
    WORD cbBufMax = 2000;
    WORD cbBufOut;
    TCHAR *pszBuf = szBuf;
    CString sDriver;
	
    // 获取已安装驱动的名称(涵数在odbcinst.h里)
    if (!SQLGetInstalledDriversW(szBuf, cbBufMax, &cbBufOut))
        return "";
    
    // 检索已安装的驱动是否有Excel...
    do
    {
        if (_tcsstr(pszBuf, _T("Excel")) != 0)
        {
            //发现 !
            sDriver = CString(pszBuf);
            break;
        }
        pszBuf = _tcschr(pszBuf, L'\0') + 1;
    }
    while (pszBuf[1] != '\0');
	
    return sDriver;
}
void CDialupKitApp::ReadExcel()
{
	if(m_pIdMapStr->GetCount())
		return;
	CDatabase database;
    CString szSql;
	CStringArray ItemStringArr;
    CString szDriver;
    CString szDsn;
	CString szItem;
    CString szFile,szPath;
    CStringArray* pValueArr;
	int nLandNum = 0;
	
	
	
	
	
	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
    int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\lang.ini");
	ReadINI(szFile);
	szFile = szPath + _T("\\Demo.xls"); 			// 将被读取的Excel文件名

	
    // 检索是否安装有Excel驱动 "Microsoft Excel Driver (*.xls)" 
    szDriver = GetExcelDriver();
    if (szDriver.IsEmpty())
    {
        // 没有发现Excel驱动
        AfxMessageBox(_T("没有安装Excel驱动!"));
		
    }
    
    // 创建进行存取的字符串
    szDsn.Format(_T("ODBC;DRIVER={%s};DSN='';DBQ=%s"), szDriver, szFile);
	
    TRY
    {
        // 打开数据库(既Excel文件)
        database.Open(NULL, false, false, szDsn);
        
        CRecordset recset(&database);//通过该类对纪录集进行操作
		szSql=_T("SELECT ");
		for(int n =0;n<m_LangArr.GetSize();n++)
		{
			szSql+=m_LangArr.GetAt(n);
			if((m_LangArr.GetSize()-1)!=n)
				szSql+=_T(", ");
			
		}
		nLandNum = m_LangArr.GetSize()-1;
        szSql+=_T(" FROM Exceldemo ");
        // 设置读取的查询语句.
		
		// AfxMessageBox(szSql);
        // 执行查询语句
        recset.Open(CRecordset::forwardOnly, szSql, CRecordset::readOnly);
		
		int i=0;
		int n=0;
		
        // 获取查询结果
        while (!recset.IsEOF())//当有纪录或者不是最后一条纪录时
			//读取Excel内部数值  
		{
			
			
            recset.GetFieldValue(m_LangArr.GetAt(0), szItem);
			pValueArr = new CStringArray();
			for(n =0;n<nLandNum;n++)
			{
				CString szTmp;
				recset.GetFieldValue(m_LangArr.GetAt(n+1), szTmp);
				
				pValueArr->Add(szTmp);

                TRACE(szTmp);
                TRACE(L"\n");
			}
			
			
			int nKey = _ttoi(szItem.GetBuffer(0));
			m_pIdMapStr->SetAt(nKey,pValueArr);
			i++;
			recset.MoveNext();
        }
		
        // 关闭数据库
        database.Close();
		
    }
    CATCH(CDBException ,e)
    {
        // 数据库操作产生异常时...
        AfxMessageBox(_T("数据库错误: ") + e->m_strError);
    }
    END_CATCH;
	
	
}

//if want to modify the lang set
//change code here
void CDialupKitApp::ReadINI(CString szPath)
{
	CString szKey;
	int n =0;
	//	CString szPath = _T("lang.ini");
	DWORD dwRet = 1;
	int nPos = 0;
	CString szTmp;
	int nLand = 0;
	BOOL bNoSet = FALSE;
	if (!ReadRegister(AMOI_NET_SETTING,_T("LangSet"), szTmp))
	{
		nLand =GetOSLang();
		bNoSet = TRUE;
	}
	else
	{
		nLand = _ttoi(szTmp);
		m_nLandId = nLand;
	}
	while(dwRet)
	{
		CString szReturn;
		TCHAR* pReturn = szReturn.GetBuffer(MAX_VALUE_LEN);
		szKey.Format(_T("Lang%d"),n);
		dwRet= ::GetPrivateProfileString(_T("Language"),szKey,_T(""),pReturn,MAX_VALUE_LEN,szPath);
		szReturn.ReleaseBuffer();
		
		if(szReturn.GetLength())
		{
			CString szSetLan;
			nPos = szReturn.Find(_T(','));
			CString szKey = szReturn.Left(nPos);
			CString szLang = szReturn.Right(szReturn.GetLength()-nPos-1);
			m_LangArr.Add(szLang);
			//if(bNoSet&&nLand==_ttoi(szKey))
			//m_nLandId = n-1;

			//szSetLan.Format(_T("%d"),m_nLandId);
			//WriteRegister(AMOI_NET_SETTING, _T("LangSet"), szSetLan);
            //lishch modify set the defaut language "English"
            if(bNoSet)
            {
                szSetLan.Format(_T("%d"),0);
			    WriteRegister(AMOI_NET_SETTING, _T("LangSet"), szSetLan);
            }
			
		}
		n++;
	}
}

void CDialupKitApp::ReadConnectionSet()
{
	CString szTmp;
	int temp = 0;
	
	if (!ReadRegister(AMOI_NET_SETTING,_T("Auto"), szTmp))
	{
		CString SetConn;
		SetConn.Format(_T("%d"),1);
		WriteRegister(AMOI_NET_SETTING, _T("Auto"), SetConn);
	}
	else
	{
		temp = _ttoi(szTmp);
		if (temp == 1)
			n_Check1Value = TRUE;
		else
			n_Check1Value = FALSE;
	}

	if (!ReadRegister(AMOI_NET_SETTING,_T("ConnSet"), szTmp))
	{
		CString SetConn;
		SetConn.Format(_T("%d"),0);
		WriteRegister(AMOI_NET_SETTING, _T("ConnSet"), SetConn);
	}
	else
	{
		temp = _ttoi(szTmp);
		ConnectionSet = temp;
	}
}

void CDialupKitApp::TransMenu(CMenu* menu)
{
	
    int iNum = menu->GetMenuItemCount();
	CStringArray* myStr;
	int IdList[] ={118,121,126};
    CString ItemValue;
	for ( INT i = 0; i < iNum; i++)
	{   
		
		CString strvalue;
		int  nItem = menu->GetMenuItemID(i);
		if(nItem==-1)
		{
			strvalue = g_GetText(IdList[i],m_nLandId);
			menu->ModifyMenu(i,MF_BYPOSITION,i,strvalue);
			TransMenu(menu->GetSubMenu(i));
		}
		BOOL bFound =m_pIdMapStr->Lookup(nItem,(CObject*&)myStr);
		if(bFound)
		{ 
			strvalue=myStr->GetAt(m_nLandId);
			menu->ModifyMenu(nItem,MF_BYCOMMAND,nItem,strvalue);
			strvalue.Empty();
		}
	
		
	}
	
}
int CDialupKitApp::GetOSLang()
{
	int nLang;
	int nLanguage = (INT)PRIMARYLANGID(::GetSystemDefaultLangID());
	int	nSubLanguage = (INT)SUBLANGID(::GetSystemDefaultLangID());
	
	nLang = nLanguage;//nLanguage<<8+nSubLanguage;
	return nLang;
}


