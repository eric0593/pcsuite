// ConnectionDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "DialupKit.h"
#include "Common.h"
#include "ConnectionDlg.h"
#include ".\connectiondlg.h"

bool n_Check1Value = TRUE;
int ConnectionSet = 0;
extern CFont Newfont;
extern BOOL	m_IsAddFone;//判断是否要添加字体

// CConnectionDlg 对话框

//IMPLEMENT_DYNAMIC(CConnectionDlg, CRgnDialog)
CConnectionDlg::CConnectionDlg(CWnd* pParent /*=NULL*/)
	: CRgnDialog(CConnectionDlg::IDD, pParent)
{
}

CConnectionDlg::~CConnectionDlg()
{
}

void CConnectionDlg::DoDataExchange(CDataExchange* pDX)
{
	CRgnDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK1, m_check1);
	DDX_Control(pDX, IDC_CHECK2, m_Check2);
	DDX_Control(pDX, IDC_CHECK3, m_Check3);
	DDX_Control(pDX, IDC_CONTITLE, m_Title);
	DDX_Control(pDX, IDC_CHECKTEXT1, m_checktext1);
	DDX_Control(pDX, IDC_CHECKTEXT2, m_checktext2);
	DDX_Control(pDX, IDC_CHECKTEXT3, m_checktext3);
	DDX_Control(pDX, IDC_BUTTON1, m_bOK);
	DDX_Control(pDX, IDC_BUTTON2, m_BCancel);
}


BEGIN_MESSAGE_MAP(CConnectionDlg, CRgnDialog)
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_CHECK1, OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_CHECK2, OnBnClickedCheck2)
	ON_BN_CLICKED(IDC_CHECK3, OnBnClickedCheck3)
	ON_BN_CLICKED(IDC_BUTTON1, &CConnectionDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CConnectionDlg::OnBnClickedButton2)
END_MESSAGE_MAP()

BOOL CConnectionDlg::OnInitDialog() 
{
	//m_Bitmap.LoadBitmap(IDB_CONNECTIONBK);
	//SetBackgroundBitmap(&m_Bitmap,RGB(255,0,255));
	CRgnDialog::OnInitDialog();

	if (m_IsAddFone == FALSE)
	{
		m_Title.SetFont(&Newfont);
		m_checktext1.SetFont(&Newfont);
		m_checktext2.SetFont(&Newfont);
		m_checktext3.SetFont(&Newfont);
		m_bOK.SetFont(&Newfont);
		m_BCancel.SetFont(&Newfont);
	}

	SetWindowText(GetText(T_CONNECTION));
	
	//m_Title.SetWindowText(GetText(T_CONNECTION));
	m_Title.SetBackColor(TRANS_BACK);
	m_Title.SetTextColor(RGB(255,255,255));

	if (n_Check1Value == TRUE)
		m_check1.SetCheck(BST_CHECKED);
	else
		m_check1.SetCheck(BST_UNCHECKED);

	switch(ConnectionSet)
	{
		case 1:
			m_Check2.SetCheck(BST_CHECKED);
			m_Check3.SetCheck(BST_UNCHECKED);
			n_Check2Value = TRUE;
			n_Check3Value = FALSE;
			break;

		case 2:
			m_Check2.SetCheck(BST_UNCHECKED);
			m_Check3.SetCheck(BST_CHECKED);
			n_Check2Value = FALSE;
			n_Check3Value = TRUE;
			break;

		case 3:
			m_Check2.SetCheck(BST_CHECKED);
			m_Check3.SetCheck(BST_CHECKED);
			n_Check2Value = TRUE;
			n_Check3Value = TRUE;
			break;

		default:
			m_Check2.SetCheck(BST_UNCHECKED);
			m_Check3.SetCheck(BST_UNCHECKED);
			n_Check2Value = FALSE;
			n_Check3Value = FALSE;
			break;
	}

	//AfxGetMainWnd()->SetWindowText(GetText(IDC_GRPBOX_CONNECTION));
	m_checktext1.SetWindowText(GetText(T_CONNECTSET1));
	m_checktext2.SetWindowText(GetText(T_CONNECTSET2));
	m_checktext3.SetWindowText(GetText(T_CONNECTSET3));
	m_bOK.SetWindowText(GetText(T_OK));

	m_BCancel.SetWindowText(GetText(T_CANCEL));

	return TRUE;
}

CString CConnectionDlg::GetText(UINT nStrId)
{
	CString str;
	
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	str=pApp->g_GetText(nStrId,pApp->m_nLandId);

	return str;
}
// CConnectionDlg 消息处理程序

void CConnectionDlg::OnBnClickedCheck1()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_check1.GetCheck() == BST_UNCHECKED)
	{
		n_Check1Value = FALSE;
	}
	else if (m_check1.GetCheck() == BST_CHECKED)
	{
		n_Check1Value = TRUE;
	}
}

void CConnectionDlg::OnBnClickedCheck2()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_Check2.GetCheck() == BST_UNCHECKED)
	{
		n_Check2Value = FALSE;
		if (n_Check3Value == TRUE)
			ConnectionSet = 2;
		else
			ConnectionSet = 0;
	}
	else if (m_Check2.GetCheck() == BST_CHECKED)
	{
		n_Check2Value = TRUE;
		if (n_Check3Value == TRUE)
			ConnectionSet = 3;
		else
			ConnectionSet = 1;
	}
}

void CConnectionDlg::OnBnClickedCheck3()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_Check3.GetCheck() == BST_UNCHECKED)
	{
		n_Check3Value = FALSE;
		if (n_Check2Value == TRUE)
			ConnectionSet = 1;
		else
			ConnectionSet = 0;
	}
	else if (m_Check3.GetCheck() == BST_CHECKED)
	{
		n_Check3Value = TRUE;
		if (n_Check2Value == TRUE)
			ConnectionSet = 3;
		else
			ConnectionSet = 2;
	}
}

void CConnectionDlg::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	CString szFile,szPath;
	
	//写ini
	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\ModemSetting.ini");
	
	//关闭时把设置变化写入注册表
	{
		CString SetConn;
		int temp;
	
		if (n_Check1Value == TRUE)
			temp = 1;
		else
			temp = 0;
		
		SetConn.Format(_T("%d"),temp);
		WritePrivateProfileString(L"Setting",L"Auto",SetConn,szFile);
	}
	{
		CString SetConn;
		SetConn.Format(_T("%d"),ConnectionSet);
		WritePrivateProfileString(L"Setting",L"ConnSet",SetConn,szFile);
	}
	CRgnDialog::OnOK();
}

void CConnectionDlg::OnBnClickedButton2()
{
	// TODO: Add your control notification handler code here
	CString szTmp;
	int temp = 0;
	CString szFile,szPath;
	TCHAR Return[255] = {0};

	//写ini
	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	//AfxMessageBox(szPath);
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\ModemSetting.ini");
	
	//恢复初始，设置无效
	if (GetPrivateProfileString(L"Setting",L"Auto",L"1",Return,255,szFile) == 0)
	{
		CString SetConn;
		SetConn.Format(_T("%d"),1);
		WritePrivateProfileString(L"Setting",L"Auto",SetConn,szFile);
	}
	else
	{
		szTmp.Format(L"%s",Return);
		temp = _ttoi(szTmp);
		if (temp == 1)
			n_Check1Value = TRUE;
		else
			n_Check1Value = FALSE;
	}

	
	if (GetPrivateProfileString(L"Setting",L"ConnSet",L"1",Return,255,szFile) == 0)
	{
		CString SetConn;
		SetConn.Format(_T("%d"),0);
		WritePrivateProfileString(L"Setting",L"ConnSet",SetConn,szFile);
	}
	else
	{
		szTmp.Format(L"%s",Return);
		temp = _ttoi(szTmp);
		ConnectionSet = temp;
	}

	CRgnDialog::OnOK();
}

BOOL CConnectionDlg::OnEraseBkgnd(CDC* pDC )
{
	CBitmap    bBitmap;
	CDC 	   tempDC;	 
	CRect	   rect;   
	BITMAP		Bmp;

	bBitmap.LoadBitmap(IDB_CONNECTIONBK);

	tempDC.CreateCompatibleDC(pDC);

	tempDC.SelectObject(&bBitmap);

	GetClientRect(&rect);
	
	bBitmap.GetBitmap(&Bmp);

	pDC->StretchBlt(0, 0, rect.Width(), rect.Height(), &tempDC, 0, 0, Bmp.bmWidth, Bmp.bmHeight, SRCCOPY);

	bBitmap.DeleteObject();

	return 0;
}

