// SelCountry.cpp : implementation file
//
#include "stdafx.h"
#include "dialupkit.h"
#include "SelCountry.h"
#include "common.h"
#include "Controldata.h"
#include ".\selcountry.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_PATH		  260

static DIAL_PRAM DefaulParam = 
{
     _T("Italy"),
     _T("tre.it"),
     _T(""),  
     _T(""),
     _T("*99#"),
     _T(""),
     _T("")
};



/////////////////////////////////////////////////////////////////////////////
// SelCountry dialog


SelCountry::SelCountry(CWnd* pParent /*=NULL*/)
	: CDialog(SelCountry::IDD, pParent)
{
	//{{AFX_DATA_INIT(SelCountry)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void SelCountry::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SelCountry)
	DDX_Control(pDX, IDC_COUN_SET, m_hCountry);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SelCountry, CDialog)
	//{{AFX_MSG_MAP(SelCountry)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SelCountry message handlers

//save the select country to the regedit
void SelCountry::OnOK() 
{
	// TODO: Add extra validation here
	InitDialApn(APN_SPECAIL);
	CDialog::OnOK();
}

//initialize the dialog
//the first step  get the system language(default for English),set the language
//second translate the dialog string for the special country
//third add the country string
BOOL SelCountry::OnInitDialog() 
{
	CDialog::OnInitDialog();
    
    TransDlg();
    InitDialApn(APN_DEFAULT);
    if(AddCountry() == FALSE)
    {
       AfxMessageBox(_T("Open database failed!"));
       //CDialog::OnOK();
       EndDialog(5);
    }
	
	// TODO: Add extra initialization here
    UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//Translate the dialog string to the special country
void SelCountry::TransDlg()
{
    CString strvalue;
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	int ID_Array[]={IDOK, IDC_COUN_TITLE};
	int T_Array[]={T_OK, T_COUN_TITLE};
	SetWindowText(pApp->g_GetText(T_COUN_TITLE,pApp->m_nLandId));
	
	for(UINT j=0;j<_countof(ID_Array);j++)
	{
		
		strvalue=pApp->g_GetText(T_Array[j],pApp->m_nLandId);
		//GetDlgItem(ID_Array[j])->SetWindowText(strvalue);
        SetDlgItemText(ID_Array[j],strvalue);
        TRACE(strvalue);
        TRACE(_T("\n"));
		strvalue.Empty();
	}

    return ;
}

//Set the default country apn to the regedit
//and write the userinit value
void SelCountry::InitDialApn(TypeAPN myApn)
{
    CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();

    if(myApn == APN_SPECAIL)
    {
       // According to pApp->m_nLandId(read from regedit) 
        //read value from db
        CString SzTemp; 
	   CString szFile,szPath;
	   TCHAR pReturn[255] = {0};

        m_hCountry.GetCurSel();
        m_hCountry.GetLBText(m_hCountry.GetCurSel(), SzTemp);

		if (wcscmp(SzTemp,L"Italy") == 0)
		{
			pApp->m_nLandId = 1;
		}
		else if (wcscmp(SzTemp,L"French") == 0)
		{
			pApp->m_nLandId = 2;
		}
		else if ((wcscmp(SzTemp,L"Germany") == 0) || (wcscmp(SzTemp,L"Austria") == 0))
		{
			pApp->m_nLandId = 3;
		}
		else if (wcscmp(SzTemp,L"Denmark") == 0)
		{
			pApp->m_nLandId = 4;
		}
		else if (wcscmp(SzTemp,L"Sweden") == 0)
		{
			pApp->m_nLandId = 5;
		}
		else if (wcscmp(SzTemp,L"China") == 0)
		{
			pApp->m_nLandId = 6;
		}
		else if (wcscmp(SzTemp,L"HongKong") == 0)
		{
			pApp->m_nLandId = 7;
		}
		else
		{
			pApp->m_nLandId = 0;
		}
		pApp->m_pResConverion->SetLanguage((TLanguage)pApp->m_nLandId);
		
	        DefaulParam.szCountry = _tcsdup(SzTemp);

		GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
		szPath.ReleaseBuffer ();
		int mPos;
		//AfxMessageBox(szPath);
		mPos=szPath.ReverseFind ('\\');
		szPath=szPath.Left (mPos);
		szFile = szPath + _T("\\APNINFO.ini");

         
        CString szKey;
		_variant_t Value;

		GetPrivateProfileString(SzTemp,L"APNName",L"1",pReturn,255,szFile);
		DefaulParam.szAPN = _wcsdup(pReturn);

		GetPrivateProfileString(SzTemp,L"PhoneNumber",L"1",pReturn,255,szFile);
		DefaulParam.szPhoneNumber = _wcsdup(pReturn);

		GetPrivateProfileString(SzTemp,L"UserName",L"1",pReturn,255,szFile);
		DefaulParam.szUserName = _wcsdup(pReturn);

		GetPrivateProfileString(SzTemp,L"Password",L"1",pReturn,255,szFile);
		DefaulParam.szPassword = _wcsdup(pReturn);

		GetPrivateProfileString(SzTemp,L"HomePage",L"1",pReturn,255,szFile);
		DefaulParam.szHomepage = _wcsdup(pReturn);
       
    }

    pApp->m_pDialPram = &DefaulParam;
    
    CString strIPV4 = _T("AT+CGDCONT=1,\"IP\",\"" );//"AT+CGDCONT=1,\"IP\",\"cmwap\"";
    strIPV4  += pApp->m_pDialPram->szAPN;
    strIPV4  += _T("\"");
    TRACE(strIPV4);
    TRACE(_T("\n"));
    
	CControlData::Instance()->SetAPN8709(strIPV4);	
    CControlData::Instance()->WriteNetSettings();
}

//add country to the combo box
BOOL SelCountry::AddCountry()
{
	CString szFile,szPath;
	CString szReturn;
	TCHAR* pReturn = szReturn.GetBuffer(255);
	
	//获取当前程序地址，读取ini信息
	{
		GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
		szPath.ReleaseBuffer ();
		int mPos;
		//AfxMessageBox(szPath);
		mPos=szPath.ReverseFind ('\\');
		szPath=szPath.Left (mPos);
		szFile = szPath + _T("\\APNINFO.ini");

		GetPrivateProfileString(NULL,NULL,NULL,pReturn,255,szFile);		
		szReturn.ReleaseBuffer();
	}

	{
		int i,pos = 0;
		CString Temp;
		
		for (i=0;i<255;i++)
		{
			if (pReturn[i] == NULL)
			{
				Temp = (TCHAR*)(pReturn + pos);
				m_hCountry.AddString(Temp);
				pos = i + 1;
				if (pReturn[pos] == NULL)
					break;
			}
				
		}
	}

	m_hCountry.SetCurSel(7);
	
    return TRUE;
}

void SelCountry::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnClose();
}

void SelCountry::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	OnOK();
}
