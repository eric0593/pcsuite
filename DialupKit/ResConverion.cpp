// ResConverion.cpp: implementation of the CResConverion class.
//
//////////////////////////////////////////////////////////////////////
#define _CRTDBG_MAP_ALLOC

#include "stdafx.h"
#include "StringResource.h"
#include "ResConverion.h"
#include "Error.h"
#include "Common.h"

#include <stdlib.h>
#include <crtdbg.h>
#include "stdio.h"
#include "conio.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
static TPCSuiteResFile g_PCSuiteResFile[LANG_MAX];

static TResFileName    g_ResFileName[LANG_MAX] = 
{
	{LANG_CHN, "Chinese.dat"},         //Simple Chinese
	{LANG_CHT, "Traditional.dat"},     //Traditional  Chinese			 
	{LANG_ENG, "English.dat"},         //English
	{LANG_ITA, "Italy.dat"},           //Italy
	{LANG_FRE, "French.dat"},          //French
	{LANG_DEU, "German.dat"},          //Deutschland  /  Germany
	{LANG_DAN, "Danish.dat"},          //Danish
	{LANG_SWE, "Swedish.dat"},         //Swedish
};

CResConverion::CResConverion()
{
	CString    szFileModuleName;
	char    szFilePath[MAX_PATH + 1] = {0};
	char    szFileName[MAX_PATH + 1] = {0};
	int     n,nPos = 0;

	TPCSuiteResFile    ResFile;
	CString szTmp;
	int language;
	
	//读取设置ini
	CString szFile,szPath;

	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\ModemSetting.ini");	

	TCHAR Return[255] = {0};
	GetPrivateProfileString(L"Language",L"Language",L"unknown",Return,255,szFile) ;	
	szTmp.Format(L"%s",Return);
	if (szTmp.Find(L"unknown") != -1)
	{
		m_Language = LANG_ENG;
	}
	else
	{
		language = _ttoi(szTmp);
		m_Language = (TLanguage)language;

		if (m_Language > LANG_MAX)
		{
			m_Language = LANG_ENG;
		}
	}
	
	//获取当前程序地址，读取资源文件
	{
		GetModuleFileName(NULL,szFileModuleName.GetBufferSetLength(MAX_PATH+1),MAX_PATH);
		szFileModuleName.ReleaseBuffer ();
		
		sprintf(szFilePath,"%S",szFileModuleName);
		
		for(n = strlen(szFilePath); n >= 0; n--)
		{
			if(szFilePath[n] == '\\')
			{
				szFilePath[n] = 0x00;
				break;
			}
		}
	}

	nPos = 0;
	for(n = 0; n < LANG_MAX; n++)
	{
		sprintf(szFileName,"%s\\Resource\\%s",szFilePath,g_ResFileName[n].szResFile);
		ResFile.Language = g_ResFileName[n].Language;
		if(ReadResourceFile(szFileName,&ResFile) == ERR_SUCCESS)
		{
			g_PCSuiteResFile[nPos++] = ResFile;	
		}
	}
}

CResConverion::~CResConverion()
{
	ReleaseResFile();
}

int CResConverion::SetLanguage(TLanguage Language)
{
	CString szSetLan;
	CString szFile,szPath;

	GetModuleFileName(NULL,szPath.GetBufferSetLength (MAX_PATH+1),MAX_PATH);
	szPath.ReleaseBuffer ();
	int mPos;
	mPos=szPath.ReverseFind ('\\');
	szPath=szPath.Left (mPos);
	szFile = szPath + _T("\\ModemSetting.ini");	
	
	m_Language = Language;
	szSetLan.Format(_T("%d"),(int)m_Language);
	WritePrivateProfileString(L"Language",L"Language",szSetLan,szFile);
	
	return ERR_SUCCESS;
}

int CResConverion::GetString(unsigned int nResID, WCHAR** ppszText)
{
	TPCSuiteRes  *pPCSuiteRes = NULL;
	int          nResCount    = 0;
	int          n = 0;
	int          nLow = 0,nMid = 0,nHigh = 0;

	for(n = 0; n < LANG_MAX; n++)
	{
		if(g_PCSuiteResFile[n].Language == m_Language)
		{
			pPCSuiteRes = g_PCSuiteResFile[n].pRes;
			nResCount   = g_PCSuiteResFile[n].nResCount;
			break;
		}
	}

	if(pPCSuiteRes == NULL)
	{
		return ERR_UNSUPPORTED;
	}

	nLow  = 0;
	nHigh = nResCount - 1; 
	
	while(nLow <= nHigh)
	{
		nMid = (nLow + nHigh) / 2;
		if(pPCSuiteRes[nMid].nResID < nResID)
		{
			nLow = nMid + 1; 
		}
		else if(pPCSuiteRes[nMid].nResID > nResID)
		{
			nHigh = nMid - 1;
		}
		else
		{
			//find the resource
			*ppszText = (WCHAR*)malloc((wcslen((wchar_t *)(pPCSuiteRes[nMid].pResData)) + 1) * sizeof(WORD));
			if(*ppszText != NULL)
			{
				memset(*ppszText,0,(wcslen((wchar_t *)(pPCSuiteRes[nMid].pResData)) + 1) * sizeof(WORD));
				memcpy(*ppszText,pPCSuiteRes[nMid].pResData,wcslen((wchar_t *)(pPCSuiteRes[nMid].pResData)) * sizeof(WORD));
			}
			break;
		}
	}

	return ERR_SUCCESS;
}

int CResConverion::ReadResourceFile(char* pszFileName, TPCSuiteResFile *pResFile)
{
	FILE     *pFile = NULL;
	int       iReadSize = 0;
	char      szReadData[MAX_RES_LENGTH];
	int       nResID,nResLength,nCount;

	TPCSuiteRes   *pRes   = NULL;
	char          *pszResData = NULL;

	TResDataType      ResDataType = RDT_RESID;
	TPCSuiteResList   *pResListHeader = NULL,*pResList = NULL;
	WORD              *pResData = NULL;
	int               nPos;

	if(pszFileName == NULL || pResFile == NULL)
	{
		return ERR_BADPARAM;
	}

	if(strlen(pszFileName) == 0)
	{
		return ERR_BADPARAM;
	}

	pFile = fopen(pszFileName, "rb");
	if(pFile == NULL)
	{
		return ERR_FILENOTEXISTED;
	}

	nCount = 0;
	iReadSize = sizeof(int);
	while(fread(szReadData,iReadSize,1,pFile))
	{
		switch(ResDataType)
		{
		case RDT_RESID:
			ResDataType = RDT_RESLENGTH;
			iReadSize = sizeof(nResLength);

			memcpy(&nResID,szReadData,iReadSize);
			continue;
		case RDT_RESLENGTH:
			ResDataType = RDT_RESDATA;
			memcpy(&nResLength,szReadData,iReadSize);

			iReadSize = nResLength;
			continue;
		case RDT_RESDATA:
			ResDataType = RDT_RESID;
			iReadSize = sizeof(nResID);

			nCount++;
			pResList = (TPCSuiteResList*)malloc(sizeof(TPCSuiteResList));
			pResData = (WORD*)malloc(nResLength + 2);
			if(pResList != NULL && pResData != NULL)
			{
				memset(pResData,0,nResLength + 2);
				memcpy(pResData,szReadData,nResLength);
				pResList->nResID   = nResID;
				pResList->pNext    = NULL;
				pResList->pResData = pResData;

				if(pResListHeader == NULL)
				{
					pResListHeader = pResList;
				}	
				else
				{
					TPCSuiteResList* pTemp = pResListHeader;
					while(pTemp->pNext != NULL)
					{
						pTemp = pTemp->pNext;
					}

					pTemp->pNext = pResList;
				}
			}
			
			continue;
		}
	}

	pResFile->nResCount = nCount;
	pRes = (TPCSuiteRes*)malloc(sizeof(TPCSuiteRes) * nCount);
	
	nPos = 0;
	TPCSuiteResList* pTemp   = pResListHeader;
	TPCSuiteResList* pDelete = NULL;

	while(pTemp != NULL)
	{
		pDelete = pTemp;
		pTemp = pTemp->pNext;

		pRes[nPos].nResID   = pDelete->nResID;
		pRes[nPos].pResData = pDelete->pResData;

		nPos++;
		FREEIF(pDelete);
	}
	
	pResFile->pRes  = pRes;
			
	fclose(pFile);
	return ERR_SUCCESS;
}

void CResConverion::ReleaseResFile()
{
	TPCSuiteResFile  ResFile;
	int              i = 0, n = 0;

	for(i = 0; i < LANG_MAX; i++)
	{
		ResFile = g_PCSuiteResFile[i];

		for(n = 0; n < ResFile.nResCount; n++)
		{
			FREEIF(ResFile.pRes[n].pResData);
		}

		FREEIF(ResFile.pRes);
	}
}
