// ResConverion.h: interface for the CResConverion class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RESCONVERION_H__B7686061_E0DA_4909_A5F2_1956E987881D__INCLUDED_)
#define AFX_RESCONVERION_H__B7686061_E0DA_4909_A5F2_1956E987881D__INCLUDED_

#include "StringResource.h"	// Added by ClassView
#include "PCSuiteRes.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef struct
{
	TLanguage   Language;
	int         nResCount;
	TPCSuiteRes *pRes;
}TPCSuiteResFile;

#define MAX_NAME_LENGTH   (256)
typedef struct
{
	TLanguage   Language;
	char        szResFile[MAX_NAME_LENGTH];
}TResFileName;

typedef enum
{
	RDT_RESID,       //resource id
	RDT_RESLENGTH,   //resource length
	RDT_RESDATA,     //resource data
}TResDataType;

typedef struct _tagPCSuiteResList TPCSuiteResList;

struct _tagPCSuiteResList
{
	unsigned int    nResID;
	unsigned short  *pResData;
	TPCSuiteResList *pNext;
};

class CResConverion  
{
public:

	int GetString(unsigned int nResID,WCHAR** ppszText);
	int SetLanguage(TLanguage Language);
	CResConverion();
	virtual ~CResConverion();

private:
	void ReleaseResFile();
	int  ReadResourceFile(char *pszFileName,TPCSuiteResFile *pResFile);

	TLanguage         m_Language;
};

#endif // !defined(AFX_RESCONVERION_H__B7686061_E0DA_4909_A5F2_1956E987881D__INCLUDED_)
