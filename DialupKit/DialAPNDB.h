// DialAPNDB.h: interface for the DialAPNDB class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIALAPNDB_H__C07FE222_C4A7_4AA1_B7F2_B845436202C8__INCLUDED_)
#define AFX_DIALAPNDB_H__C07FE222_C4A7_4AA1_B7F2_B845436202C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afxtempl.h"
#include <afxdb.h>
#include <odbcinst.h>
#include "atlbase.h"

#import"c:\Program Files\Common Files\System\ado\msado15.dll" no_namespace rename("EOF", "EndOfFile") rename("BOF", "BOfFile")

class DialAPNDB  
{
public:
	DialAPNDB();
    DialAPNDB(CString szDBName, CString szSql);
	virtual ~DialAPNDB();

    _ConnectionPtr ADO_Connect(CString szDBName);
    BOOL ADO_OpenDB(CString szSql);
    BOOL ADO_IsEof();
    _variant_t ADO_GetValue(char* pItem);
    void ADO_Execute(CString szSql,CString szErr);
    void ADO_MoveNext( );
    void ADO_Close();

    _RecordsetPtr  m_pRecordSet;
    BOOL           m_bOpenState;

protected:
private:
   _ConnectionPtr m_pConnection;
   CString        m_szDBName;
};

#endif // !defined(AFX_DIALAPNDB_H__C07FE222_C4A7_4AA1_B7F2_B845436202C8__INCLUDED_)
