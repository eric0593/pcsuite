// DialupKit.h : main header file for the DIALUPKIT application
//

#if !defined(AFX_DIALUPKIT_H__2E496C63_03E2_4C7D_9536_F7035183605C__INCLUDED_)
#define AFX_DIALUPKIT_H__2E496C63_03E2_4C7D_9536_F7035183605C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "ResConverion.h"
#include "RS232.h"

/////////////////////////////////////////////////////////////////////////////
// CDialupKitApp:
// See DialupKit.cpp for the implementation of this class
//
typedef struct
{
	TCHAR* szCountry;
	TCHAR* szAPN;
	TCHAR* szUserName;
	TCHAR* szPassword;	
	TCHAR* szPhoneNumber;
	TCHAR* szIPAddress;
	TCHAR* szHomepage;
}DIAL_PRAM;
class CDialupKitApp : public CWinApp
{
public:
	CDialupKitApp();
    virtual ~CDialupKitApp();
    enum	{	STATE_USB_DISCONNECT	= 0,			// USB and BT no connect
				STATE_USB_CONNECT,					// USB and BT connect 
				STATE_NETWORK_CONNECT,			// connected network
			};

public:
	void ReadINI(CString szPath);
	void ReadExcel();
	void ReadConnectionSet();
	CString g_GetText(UINT uID , int nLangID);
	CString m_strRootDir;                       // 保存该exe文件的所在的目录
    CImageList m_ImageList;
	CMapWordToOb* m_pIdMapStr;
	CStringArray m_LangArr;
	int m_nLandId;
	DIAL_PRAM *m_pDialPram;
	BOOL m_bNetType;
	void TransMenu(CMenu* menu);
	int GetOSLang();
	byte modemstate;						//保存与网络usb等的连接状态
	CResConverion  *m_pResConverion;
	BOOL m_bGetlangandApn;				//判断是否要获取语言和apn
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialupKitApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDialupKitApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALUPKIT_H__2E496C63_03E2_4C7D_9536_F7035183605C__INCLUDED_)
