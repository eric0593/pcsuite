// HelpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DialupKit.h"
#include "HelpDlg.h"


// HelpDlg dialog

IMPLEMENT_DYNAMIC(HelpDlg, CDialog)

HelpDlg::HelpDlg(CWnd* pParent /*=NULL*/)
	: CDialog(HelpDlg::IDD, pParent)
{

}

HelpDlg::~HelpDlg()
{
}

void HelpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(HelpDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON2, &HelpDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// HelpDlg message handlers
BOOL HelpDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	
	SetWindowText(pApp->g_GetText(T_HELP,pApp->m_nLandId));	 
	SetDlgItemText(IDC_STATIC_Helptext, pApp->g_GetText(T_HELPINFORMATION,pApp->m_nLandId));
	SetDlgItemText(IDC_BUTTON2, pApp->g_GetText(T_OK,pApp->m_nLandId));
	
	return TRUE;  // return TRUE unless you set the focus to a control
}


void HelpDlg::OnBnClickedButton2()
{
	// TODO: Add your control notification handler code here
	CDialog::OnCancel();
}
