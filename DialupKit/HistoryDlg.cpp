// HistoryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DialupKit.h"
#include "HistoryDlg.h"
#include "ConnectionHistory.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHistoryDlg dialog


CHistoryDlg::CHistoryDlg(CWnd* pParent /*=NULL*/)
	: CMyDlg(CHistoryDlg::IDD, pParent, TRUE)
{
	//{{AFX_DATA_INIT(CHistoryDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CHistoryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHistoryDlg)
	DDX_Control(pDX, IDC_LIST_HISTORY, m_listHistory);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHistoryDlg, CDialog)
	//{{AFX_MSG_MAP(CHistoryDlg)
	ON_BN_CLICKED(IDC_BTN_RESETALL, OnBtnResetall)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHistoryDlg message handlers

BOOL CHistoryDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	TransDlg();
	InitHistoryList();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CHistoryDlg::InitHistoryList()
{
    int     nTx = 0;
    int     nRx = 0;
    CRect   rect;
    CString strDate;
    CString strTimeSpan;
    CString strTx;
    CString strRx;
    CTimeSpan timeSpan;
    
    m_listHistory.GetWindowRect(rect);
    LVCOLUMN lvcolumn;
    lvcolumn.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH;
    lvcolumn.fmt = LVCFMT_LEFT;
    lvcolumn.cx = rect.Width()/4;

	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	CString szTmp =pApp->g_GetText(IDS_DATE,pApp->m_nLandId);
	
    lvcolumn.pszText =szTmp.GetBuffer(0);
    lvcolumn.iSubItem = 0;
    //m_listHistory.Create( LVS_REPORT | LVS_EDITLABELS | WS_VISIBLE | WS_BORDER | WS_CHILD  ,/* CRect(10,10,410,260)*/rect , this, IDC_LISTCTRL);
    
    m_listHistory.InsertColumn(0, &lvcolumn);
    szTmp.Empty();
	szTmp =pApp->g_GetText(IDS_TIME,pApp->m_nLandId);
    lvcolumn.pszText = szTmp.GetBuffer(0);
    lvcolumn.iSubItem = 1;
    m_listHistory.InsertColumn(1, &lvcolumn);
	szTmp.Empty();
	szTmp =pApp->g_GetText(IDS_TX,pApp->m_nLandId);
    lvcolumn.pszText = szTmp.GetBuffer(0);
    lvcolumn.iSubItem = 2;
    m_listHistory.InsertColumn(2, &lvcolumn);
	szTmp.Empty();
	szTmp =pApp->g_GetText(IDS_RX,pApp->m_nLandId);
    lvcolumn.pszText = szTmp.GetBuffer(0);
    lvcolumn.iSubItem = 3;
    m_listHistory.InsertColumn(3, &lvcolumn);
    
    // add extend style to the list view contrl
    DWORD dwStyle = m_listHistory.SendMessage(LVM_GETEXTENDEDLISTVIEWSTYLE,0,0);
    dwStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
    m_listHistory.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, dwStyle); 


    // Get History records from Registry
    int nRecordCnt = 0;
    GetHistoryRecordCnt(nRecordCnt);

    for(int i = 0; i < nRecordCnt; i++)
    {
        GetOneHistoryRecord(i, strDate, strTimeSpan, nTx, nRx);
        strTx = FormatBytesOutput(nTx);
        strRx = FormatBytesOutput(nRx);

        // INSERT ONE RECORD ITEM TO THE HISTORY LIST CONTROL
        m_listHistory.InsertItem(i, strDate);            // DATE
        m_listHistory.SetItemText(i, 1, strTimeSpan);    // TIME
        m_listHistory.SetItemText(i, 2, strTx);          // TX
        m_listHistory.SetItemText(i, 3, strRx);          // RX
    }

    if(0 == nRecordCnt)
    {
        return;
    }
    // INSERT THE TOTAL STATISTIC TO THE BOTTOM OF THE LIST CONTROL
    GetHistoryTotalStatistic(timeSpan, nTx, nRx);
    
    // FORMAT TIME SPAN STRING 
    if(timeSpan.GetDays() > 0)
    {
        strTimeSpan.Format(L"%d:%.2d:%.2d", 
                   timeSpan.GetDays()*24 + timeSpan.GetHours(), 
                   timeSpan.GetMinutes(), 
                   timeSpan.GetSeconds());
    }
    else
    {
        strTimeSpan = timeSpan.Format(IDS_TIMESPAN_FORMAT);
    }

    strTx = FormatBytesOutput(nTx);
    strRx = FormatBytesOutput(nRx);
	szTmp.Empty();
	szTmp =pApp->g_GetText(IDS_TOTAL,pApp->m_nLandId);
    m_listHistory.InsertItem(i, szTmp.GetBuffer(0));
    m_listHistory.SetItemText(i, 1, strTimeSpan);
    m_listHistory.SetItemText(i, 2, strTx);
    m_listHistory.SetItemText(i, 3, strRx);

}

void CHistoryDlg::OnBtnResetall() 
{
	// TODO: Add your control notification handler code here
	m_listHistory.DeleteAllItems();
    ClearAllHistoryRecords();
}
void CHistoryDlg::TransDlg()
{
	CString strvalue;
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	int ID_Array[]={IDC_BTN_RESETALL,IDOK};
	for(UINT j=0;j<_countof(ID_Array);j++)
	{
		
		strvalue=pApp->g_GetText(ID_Array[j],pApp->m_nLandId);
		//	GetDlgItem(ID_Array[j])->SetWindowText(strvalue);
		SetDlgItemText(ID_Array[j],strvalue);
		strvalue.Empty();
	}
	strvalue=pApp->g_GetText(ID_MENU_HISTORY,pApp->m_nLandId);
	strvalue =strvalue.Left(strvalue.Find(_T("(")));
    SetWindowText(strvalue);
}