// StatusDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DialupKit.h"
#include "StatusDlg.h"
#include "Controller.h"
#include "common.h"
#include ".\statusdlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStatusDlg dialog
extern int 	PhoneStatus;
extern int ConnectionSet;
extern TDialStatus	m_Dialstatus;

#define TIMER_CLOSE_STATUS_DLG        1
#define CLOSE_STATUS_DLG_ELAPSE       1000
/////////////////////////////////////////////////////////////////////////////
// CStatusDlg dialog


/*CStatusDlg::CStatusDlg(CString strInfo, CWnd* pParent =NULL)
	: CMyDlg(CStatusDlg::IDD, pParent, TRUE) // ADD THE 3rd PARAMETER TRUE TO VALIDATE ENTER AND ESC KEY EVENT 2005-11-24
{
	//{{AFX_DATA_INIT(CStatusDlg)
	m_strStatusText = _T("");
	//}}AFX_DATA_INIT

    m_strStatusText = strInfo;
}*/

CStatusDlg::CStatusDlg(CWnd* pParent /*=NULL*/)
	: CMyDlg(CStatusDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CStatusDlg)
	m_strStatusText = _T("");
	//}}AFX_DATA_INIT
}

void CStatusDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStatusDlg)
	DDX_Text(pDX, IDC_STATIC_STATUSTEXT, m_strStatusText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CStatusDlg, CDialog)
	//{{AFX_MSG_MAP(CStatusDlg)
	ON_WM_TIMER()
	ON_BN_CLICKED(ID_DISCONNECT, OnDisconnect)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStatusDlg message handlers

BOOL CStatusDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

    CString strCancel;
	// hide the OK button
    CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
    strCancel = pApp->g_GetText(2,pApp->m_nLandId);
	if (PhoneStatus == 1)
	{
		SetWindowText(pApp->g_GetText(T_OOC,pApp->m_nLandId));   
		GetDlgItem(IDOK)->ShowWindow(FALSE);
		SetDlgItemText(IDC_STATIC_STATUSTEXT, pApp->g_GetText(T_OOCINFO,pApp->m_nLandId));
		SetDlgItemText(ID_DISCONNECT, pApp->g_GetText(T_OK,pApp->m_nLandId));
	}
	else
	{
		SetWindowText(pApp->g_GetText(T_STATIC_ERRORS,pApp->m_nLandId));	 
		SetDlgItemText(IDC_STATIC_STATUSTEXT, pApp->g_GetText(T_TRYAGAIN,pApp->m_nLandId));
		if ((ConnectionSet == 3 || ConnectionSet == 1) && (PhoneStatus == 4))
		{
			SetTimer(TIMER_CLOSE_STATUS_DLG, CLOSE_STATUS_DLG_ELAPSE, NULL);
		}
		SetDlgItemText(IDOK, pApp->g_GetText(T_YES,pApp->m_nLandId));
		SetDlgItemText(ID_DISCONNECT, pApp->g_GetText(T_NO,pApp->m_nLandId));
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CStatusDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default

	switch(nIDEvent) 
    {
		case TIMER_CLOSE_STATUS_DLG:  // ADD BY SHAOZW
			KillTimer(TIMER_CLOSE_STATUS_DLG);

            // MODIFIED BY SHAOZW 2005-11-24
			// PostMessage(WM_CLOSE, 0, 0);  // CLOSE THE WINDOW
            PostMessage(WM_COMMAND, IDOK, 0); // CHANGE CLOSE PATTERN (SEND IDOK COMMAND TO PREVENT THE FRAMEWORK CALLING OnClose());
			
            break;
	    default:
            break;
	}

	CDialog::OnTimer(nIDEvent);
}

void CStatusDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
    CController::Instance()->StopConnect();
	CDialog::OnCancel();
}

BOOL CStatusDlg::PreTranslateMessage(MSG* pMsg) 
{
    if (!m_bTranslateMsg) {
	    if (pMsg->message == WM_KEYDOWN) {
		    char cKey = pMsg->wParam;
		    if (cKey == VK_RETURN) {
			    return TRUE;
		    } else if (cKey == VK_ESCAPE)
			    return TRUE;
	    }
    }

	return CDialog::PreTranslateMessage(pMsg);
}

void CStatusDlg::OnDisconnect() 
{
	// TODO: Add your control notification handler code here
	// hang up
//	CController::Instance()->StopConnect();
	KillTimer(TIMER_CLOSE_STATUS_DLG);
	OnCancel();	
}

void CStatusDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	KillTimer(TIMER_CLOSE_STATUS_DLG);
	m_Dialstatus = STATUS_BEGINCONNECT;
	CController::Instance()->BeginConnect();
	CDialog::OnCancel();
}
