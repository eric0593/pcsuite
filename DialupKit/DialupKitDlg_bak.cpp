// DialupKitDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DialupKit.h"
#include "DialupKitDlg.h"
#include "common.h"
#include "Controller.h"
#include "HistoryDlg.h"
#include "ConnectionHistory.h"
#include "SelCountry.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#define NUMBER_MENU             5
#define ONE_KBYTE    1000
#define ONE_KBYTE_F  1000.0
#define ONE_MBYTE    1000000
#define ONE_MBYTE_F  1000000.0
#define MAX_PATH		  260
#define STRUSBCONNECT      1004
#define STRUSBDISCONNECT   1005

//HINSTANCE ghLibraryDllParam = NULL;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
/*typedef struct _RAS_STATS 
{  
    DWORD dwSize;  
    DWORD dwBytesXmited;  
    DWORD dwBytesRcved;  
    DWORD dwFramesXmited;  
    DWORD dwFramesRcved;  
    DWORD dwCrcErr;  
    DWORD dwTimeoutErr;  
    DWORD dwAlignmentErr;
    DWORD dwHardwareOverrunErr;
    DWORD dwFramingErr; 
    DWORD dwBufferOverrunErr; 
    DWORD dwCompressionRatioIn; 
    DWORD dwCompressionRatioOut;  
    DWORD dwBps; 
    DWORD dwConnectDuration;
} RAS_STATS, *PRAS_STATS;
*/
extern "C" DWORD APIENTRY RasGetConnectionStatistics(
													 HRASCONN hRasConn,
													 RAS_STATS* lpStatistics
													 );

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialupKitDlg dialog

CDialupKitDlg::CDialupKitDlg(CWnd* pParent /*=NULL*/)
	: CMyDlg(CDialupKitDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialupKitDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    m_bFirstTime = TRUE;
    m_bPreConnectState = FALSE;
    //m_szUSBConnect = "WP-S1 Modem Connect";
    //m_szUSBDisconnect = "No device";
    m_bLanChange =  FALSE;
}

void CDialupKitDlg::DoDataExchange(CDataExchange* pDX)
{
	CMyDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialupKitDlg)
	DDX_Control(pDX, IDC_LIST_GPRS, m_lstNetLink);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDialupKitDlg, CMyDlg)
	//{{AFX_MSG_MAP(CDialupKitDlg)
	ON_WM_DESTROY()
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND(ID_MENU_SETUP, OnMenuSetup)
	ON_UPDATE_COMMAND_UI(ID_MENU_SETUP, OnUpdateMenuSetup)
	ON_COMMAND(ID_MENU_CONN, OnMenuConn)
	ON_UPDATE_COMMAND_UI(ID_MENU_CONN, OnUpdateMenuConn)
	ON_COMMAND(ID_MENU_DISCON, OnMenuDiscon)
	ON_UPDATE_COMMAND_UI(ID_MENU_DISCON, OnUpdateMenuDiscon)
	ON_WM_TIMER()
	ON_COMMAND(ID_MENU_EXIT, OnAppExit)
	ON_BN_CLICKED(IDC_BTN_CONNECT, OnBtnConnect)
	ON_BN_CLICKED(IDC_BTN_EXIT, OnBtnExit)
	ON_BN_CLICKED(IDC_BTN_MINIMIZE, OnBtnMinimize)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_GPRS, OnDblclkListGprs)
	ON_COMMAND(ID_MENU_ABOUT, OnMenuAbout)
	ON_UPDATE_COMMAND_UI(ID_MENU_ABOUT, OnUpdateMenuAbout)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_UPDATE_COMMAND_UI(ID_APP_ABOUT, OnUpdateAppAbout)
	ON_COMMAND(ID_MENU_HISTORY, OnMenuHistory)
	ON_UPDATE_COMMAND_UI(ID_MENU_HISTORY, OnUpdateMenuHistory)
	ON_WM_CLOSE()
    //ON_MESSAGE(WM_RAS_GETTINGDIALSTATUS, OnGetDialStatus)
	ON_MESSAGE(WM_TRAY_NOTIFY, OnTrayNotify)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	//}}AFX_MSG_MAP
	ON_COMMAND_RANGE(ID_LANG_START, ID_LANG_START+20, OnLangStart)
	ON_UPDATE_COMMAND_UI_RANGE(ID_LANG_START, ID_LANG_START+20,OnUpdateLangStart)
	ON_WM_INITMENUPOPUP()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialupKitDlg message handlers
//on timer message handle funtion
#if 0
void CNVExportImportDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent == m_DetectComTimer)
	{
		int ValidComPortAmount = 0;
		int ComPortID[100] = {0};
		
		if(m_GetValidComPortProc != NULL)
			ValidComPortAmount = m_GetValidComPortProc(ComPortID,100);

		if(ValidComPortAmount == 1)
		{
			//gComPortInfo.ComID = ComPortID[0];
			//gComPortInfo.bIsConnect = TRUE;
			//gComPortInfo.bIsOnlyOneCom = TRUE;

			m_HandsetStatus.Format("Amoi phone connected !");

			//m_Export.EnableWindow(TRUE);
			//m_Import.EnableWindow(TRUE);
			//m_NVBackup.EnableWindow(TRUE);
			//m_NVRestore.EnableWindow(TRUE);


			UpdateData(FALSE);
		}
		else
		{
			//m_Export.EnableWindow(FALSE);
			//m_Import.EnableWindow(FALSE);
			//m_NVBackup.EnableWindow(FALSE);
			//m_NVRestore.EnableWindow(FALSE);

			if(ValidComPortAmount <= 0)
			{
				//gComPortInfo.bIsConnect = FALSE;
				//gComPortInfo.bIsOnlyOneCom = TRUE;
				m_HandsetStatus.Format("No phone connected !");
			}
			else
			{
				//gComPortInfo.bIsConnect = TRUE;
				//gComPortInfo.bIsOnlyOneCom = FALSE;
				m_HandsetStatus.Format("Two or more phones connected !");
			}
			
			UpdateData(FALSE);
		}
	}
	
	CDialog::OnTimer(nIDEvent);
}
#endif

BOOL CDialupKitDlg::OnInitDialog()
{
	CMyDlg::OnInitDialog();


	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	m_bShutDown = FALSE;
	CreateMenu();
    // 取得G网, C网图标，显示在ListCtrl中
    InitListCtrl(&m_ImageList, &m_lstNetLink);

    // 取得主对话框指针并初始化RAS信息
    CController::Instance()->Initialize(this);

	// ADD BY SHAOZW
	// SET ICON TO SYSTEM TRAY
	SetTrayIcon(TRUE);
    
    //init the OLe
    if(AfxOleInit() == 0)
        AfxMessageBox(_T("Init failed!"));
    //here to show the the dlg for init
    //first get the country of the regedit

    if(_wcsicmp(((CDialupKitApp*)AfxGetApp())->m_pDialPram->szCountry,_T("china")) == 0)
    {
        SelCountry dlg;
        int nResponse = dlg.DoModal(); 

        //if(nResponse != IDOK)
        //{
            //return FALSE;
        //}
    }

	// set connect or disconnect button and connection status
	CString str;
    //should add a function to confirm the usb is connected or not
    #if 0
    char FullName[MAX_PATH+1] = {0};

	//Load Library
	m_hLibraryListComPort = LoadLibrary("ListComPort.dll");
	if(m_hLibraryListComPort==NULL)
		MessageBox("Load ListComPort.dll failed!",NULL,MB_ICONWARNING);

	m_GetValidComPortProc = (TDLGetValidComPortProc*)GetProcAddress(m_hLibraryListComPort,DLGetValidComPortName);
	if(m_GetValidComPortProc == NULL)
		MessageBox("Load Process \"GetValidComPort\" failed!",NULL,MB_ICONWARNING);
	#if 0
	//hLibraryDllParam = LoadLibrary("../../APST/DllParam/Debug/DllParam.dll");
	hLibraryDllParam = LoadLibrary("DllParam.dll");
	if(hLibraryDllParam==NULL)
		MessageBox("Load DllParam.dll failed!",NULL,MB_ICONWARNING);
	#endif

	m_DetectComTimer = SetTimer(1010, 200, NULL);
	#endif
	
	SetTimer(TIMER_MODEM_DEVICE_CONNECTED, DETECT_CONNECTION_ELAPSE, NULL);
	
	if(CController::Instance()->IsConnectionActive())
	{
		str.LoadString(IDS_BTN_DISCONNECT);
        GetDlgItem(IDC_BTN_CONNECT)->SetWindowText(str);

		str.LoadString(IDS_STATUS_CONNECTED);
		GetDlgItem(IDC_CONNECTION_STATUS)->SetWindowText(str);

		// et connection begin time and set timer to detect connection status
		m_tConnectionBegin = CTime::GetCurrentTime();
        SetTimer(TIMER_DETECT_CONNECTION_STATUS, DETECT_CONNECTION_ELAPSE, NULL);
	}
	else
	{
		str.LoadString(IDS_BTN_CONNECT);
        GetDlgItem(IDC_BTN_CONNECT)->SetWindowText(str);

		str.LoadString(IDS_STATUS_DISCONNECTED);
		GetDlgItem(IDC_CONNECTION_STATUS)->SetWindowText(str);
	}

	//lishch modify
    HBITMAP hBitmap;
    CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
    CStatic *pStatic = (CStatic *)GetDlgItem(IDC_BITSTATUS);
	str = pApp->g_GetText(IDC_DISCONNECT,pApp->m_nLandId);
	GetDlgItem(IDC_DEVICE_CONNECT)->SetWindowText(str);

    hBitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), 
                                    MAKEINTRESOURCE(IDB_BITMAP_DISCONNECT), 
                                    IMAGE_BITMAP, 
                                    0, 
                                    0, 
                                    LR_LOADMAP3DCOLORS);
	pStatic->ModifyStyle(0xF, SS_BITMAP);
    pStatic->SetBitmap(hBitmap);
	
	pApp->TransMenu(&m_hMenu);
	SetMenu(&m_hMenu);
	TransDlg();	
	UpdateData();
//	// set timer to dectect connection status
//	SetTimer(TIMER_DETECT_CONNECTION_STATUS, DETECT_CONNECTION_ELAPSE, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}
void CDialupKitDlg::OnDblclkListGprs(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	TRACE(_T("OnDblclkListGprs\n"));
	int nIndex = CController::Instance()->GetSelIndex();
	
    if(nIndex == 0) // the user double click the GPRS dialup icon
	{   // begin cennect
		CController::Instance()->BeginConnect();
	}
	*pResult = 0;
}
void CDialupKitDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CMyDlg::OnSysCommand(nID, lParam);
	}

	if(SC_MINIMIZE == nID) // hide the window when minimize the window
	{
        ShowWindow(FALSE);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
#define AMOI_LOGO_WIDTH     275
#define AMOI_LOGO_HEIGHT    38
#define AMOI_LOGO_POS_Y     355
#define AMOI_LOGO_Y_PADDING 0

void CDialupKitDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
        CPaintDC dc(this);

	    CDC maskDC;
	    CDC imageDC;
	    CBitmap bmp;
	    CBitmap mask;
        CRect rect;
        int nAmoiLogoPosX = 0;
        int nAmoiLogoPosY = 0;
        
        GetClientRect(rect);
        nAmoiLogoPosX = (rect.Width() - AMOI_LOGO_WIDTH) / 2;
        nAmoiLogoPosY = rect.bottom - AMOI_LOGO_HEIGHT - AMOI_LOGO_Y_PADDING;

	    imageDC.CreateCompatibleDC(&dc);
	    maskDC.CreateCompatibleDC(&dc);
    
	    bmp.LoadBitmap(IDB_BITMAP_AMOILOGO);

	    imageDC.SelectObject(&bmp);

	    mask.LoadBitmap(IDB_BITMAP_AMOILOGO_MASK);
	    maskDC.SelectObject(&mask);


	    dc.BitBlt(nAmoiLogoPosX, nAmoiLogoPosY,
                  AMOI_LOGO_WIDTH,
                  AMOI_LOGO_HEIGHT,
                  &imageDC,
                  0,0,
                  SRCAND);

		CMyDlg::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDialupKitDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CDialupKitDlg::CreateMenu()
{
	CMenu  *pSubMenu; 
    
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();

	m_hMenu.LoadMenu(IDR_MAINFRAME);
	pSubMenu = m_hMenu.GetSubMenu(0)->GetSubMenu(1);
	for(int i = 2; i < pApp->m_LangArr.GetSize(); i++) // modified by shaozw to delete GPRS Setting menu item
    {
        //添加的菜单如果有问题，应在这边修改
        pSubMenu->AppendMenu(MF_OWNERDRAW/*MF_STRING*/, ID_LANG_START+ i-1, pApp->m_LangArr.GetAt(i)); 
    }
	SetMenu(&m_hMenu); 

    //CDC *pDC = FromHandle(m_hMenu.m_hMenu);//GetDlgItem(IDR_MAINFRAME)->GetDC();
   // pDC->SetBkColor(RGB(0x99, 0xcc, 0xff));
    //ReleaseDC(pDC);
  //  m_hMenu.Detach(); 
    //SubMenu.Detach(); 
}

#if 0
// Override DrawItem() to implement drawing for an owner-draw CMenu 
// object.
// CColorMenu is a CMenu-derived class.
void CColorMenu::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
    CDC* pDC = CDC::FromHandle(lpDIS->hDC);
    COLORREF cr = (COLORREF)lpDIS->itemData; // RGB in item data
    
    if (lpDIS->itemAction & ODA_DRAWENTIRE)
    {
        // Paint the color item in the color requested
        CBrush br(cr);
        pDC->FillRect(&lpDIS->rcItem, &br);
    }
    
    if ((lpDIS->itemState & ODS_SELECTED) &&
        (lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
    {
        // item has been selected - hilite frame
        COLORREF crHilite = RGB(255-GetRValue(cr),
            255-GetGValue(cr), 255-GetBValue(cr));
        CBrush br(crHilite);
        pDC->FrameRect(&lpDIS->rcItem, &br);
    }
    
    if (!(lpDIS->itemState & ODS_SELECTED) &&
        (lpDIS->itemAction & ODA_SELECT))
    {
        // Item has been de-selected -- remove frame
        CBrush br(cr);
        pDC->FrameRect(&lpDIS->rcItem, &br);
    }
}
#endif

void CDialupKitDlg::OnMenuSetup() 
{
	// TODO: Add your command handler code here
	 CController::Instance()->SetGPRS();  
}

void CDialupKitDlg::OnUpdateMenuSetup(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}

void CDialupKitDlg::OnLangStart(UINT nID) 
{
	// TODO: Add your command handler code here
	int nLand = nID - ID_LANG_START;
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	pApp->m_nLandId = nLand;
	CString szTmp;
    m_bLanChange = TRUE;

    //m_szUSBConnect.LoadString(STRUSBCONNECT);
    //m_szUSBDisconnect.LoadString(STRUSBDISCONNECT);
	szTmp.Format(_T("%d"),nLand);
	WriteRegister(AMOI_NET_SETTING,_T("LangSet"), szTmp);
	pApp->TransMenu(&m_hMenu);
	SetMenu(&m_hMenu);
	TransDlg();	
	UpdateData();
}

void CDialupKitDlg::OnUpdateLangStart(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	if(pApp->m_nLandId ==(pCmdUI->m_nID-ID_LANG_START))
	pCmdUI->SetCheck(TRUE);
	else
	pCmdUI->SetCheck(FALSE);
}

void CDialupKitDlg::OnMenuConn() 
{
	// TODO: Add your command handler code here
	CController::Instance()->BeginConnect();
}

void CDialupKitDlg::OnUpdateMenuConn(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
pCmdUI->Enable(!CController::Instance()->IsConnectionActive());	
}

void CDialupKitDlg::OnMenuDiscon() 
{
	// TODO: Add your command handler code here
	 Disconnect();
}
void CDialupKitDlg::SetTrayIcon(BOOL bAdd)
{
	NOTIFYICONDATA nid;
	
	nid.cbSize = (DWORD)sizeof(NOTIFYICONDATA);	
	nid.hWnd = this->m_hWnd;
	nid.uID = IDR_MAINFRAME;
	nid.uFlags = NIF_ICON|NIF_MESSAGE|NIF_TIP ;
	nid.uCallbackMessage = WM_TRAY_NOTIFY;//自定义的消息名称
	
	nid.hIcon = LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
	
	_tcscpy(nid.szTip, _T("IT2500 Mobile Modem"));  //信息提示条为“计划任务提醒”	
	
	Shell_NotifyIcon(bAdd ? NIM_ADD : NIM_DELETE, &nid);//在托盘区添加图标
}


void CDialupKitDlg::OnUpdateMenuDiscon(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(CController::Instance()->IsConnectionActive());	
}
void CDialupKitDlg::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu) 
{
    if (!bSysMenu)
    {
        ASSERT(pPopupMenu != NULL);
        
        // check the enabled state of various menu items
        CCmdUI state;        
        state.m_pMenu = pPopupMenu;
        ASSERT(state.m_pOther == NULL);
        
        state.m_nIndexMax = pPopupMenu->GetMenuItemCount();
        for (state.m_nIndex = 0; state.m_nIndex < state.m_nIndexMax;
		state.m_nIndex++)
        {
            state.m_nID = pPopupMenu->GetMenuItemID(state.m_nIndex);
            if (state.m_nID == 0)
                continue; // menu separator or invalid cmd - ignore it
			
            ASSERT(state.m_pOther == NULL);
            ASSERT(state.m_pMenu != NULL);
            if (state.m_nID == (UINT)-1)
            {
                // possibly a popup menu, route to first item of that popup
                state.m_pSubMenu = pPopupMenu->GetSubMenu(state.m_nIndex);
                if (state.m_pSubMenu == NULL ||
                    (state.m_nID = state.m_pSubMenu->GetMenuItemID(0)) == 0 ||
                    state.m_nID == (UINT)-1)
                {                                 
                    continue; // first item of popup can't be routed to
                }
                state.DoUpdate(this, FALSE);  // popups are never auto disabled
            }
            else
            {
                // normal menu item
                // Auto enable/disable if command is _not_ a system command
                state.m_pSubMenu = NULL;
                state.DoUpdate(this, state.m_nID < 0xF000);
            }
        }
    }
}

void CDialupKitDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(TIMER_DETECT_CONNECTION_STATUS == nIDEvent)
	{
        UpdateConnectionStatus();
    }

	if(TIMER_MODEM_DEVICE_CONNECTED == nIDEvent)
	{
		UpdateDeviceConnectStatus();
	}
    
	CMyDlg::OnTimer(nIDEvent);
}
void CDialupKitDlg::OnAppExit() 
{
	// TODO: Add your command handler code here
	if(!m_bShutDown) // 只关闭一次
	{
		m_bShutDown = TRUE;
		PostMessage(WM_CLOSE, NULL, NULL);
	}
}
CString CDialupKitDlg::FormatNumberPointedOff(int n)
{
    CString str;
  
    str.Format(_T("%d"), n);
    
    int len = str.GetLength();
    int nPointsCnt = (len - 1) / 3;

    for(int i = 1; i <= nPointsCnt; i++)
    {
        str.Insert(len - i * 3, _T(',')); 
    }

    TRACE(_T("FormatNumberPointedOff(%d): %s\n"), n, str);
    return str;
}
LRESULT CDialupKitDlg::OnTrayNotify(WPARAM wParam,LPARAM lParam)//wParam接收的是图标的ID，而lParam接收的是鼠标的行为
{
	if(wParam!=IDR_MAINFRAME)
	{		
		return 0;
	}
	
	switch(lParam)
	{	
	case WM_RBUTTONUP://右键起来时弹出快捷菜单，这里只有一个“关闭”
		{	
			LPPOINT lpoint = new tagPOINT;
			
			::GetCursorPos(lpoint);//得到鼠标位置
			
			CMenu *menu= this->GetMenu();
			CMenu *pSubMenu = menu->GetSubMenu(0);
		/*	menu.CreatePopupMenu();//声明一个弹出式菜单
			
			CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
			for(int i = 1; i <= NUMBER_MENU; i++) // modified by shaozw to delete GPRS Setting menu item
			{
				if(i==2)
					continue;
				menu.AppendMenu(MF_STRING, IDS_MENU_ITEM1 + i, pApp->g_GetText(IDS_MENU_ITEM1 + i,pApp->m_nLandId)); 
			}
		*/	
			
			SetForegroundWindow();
			//确定弹出式菜单的位置
			pSubMenu->TrackPopupMenu(TPM_LEFTALIGN,lpoint->x,lpoint->y,this);
			
			//资源回收
		//	menu->Detach();
		//	menu->DestroyMenu();
			delete lpoint;
		}
		break;
		
	case WM_LBUTTONDBLCLK://双击左键的处理
		
		//		SetActiveWindow();
		ShowWindow(SW_NORMAL);//简单的显示主窗口完事儿
        SetForegroundWindow();
		//		SetTimer(ID_TIMER_ACTIVATEWINDOW, 500, callbackActivateWindow);
		//		SetActiveWindow();
		//		BringWindowToTop();
		//		PostMessage(WM_ACTIVATE, WA_ACTIVE, 0);
		break;
	}	
	return 1;
	
}
BOOL CALLBACK CDialupKitDlg::EnumChildProc(
			  HWND hwnd,      // handle to child window
			  LPARAM lParam   // application-defined value
			  )
{
	CDialupKitDlg *pHandle =(CDialupKitDlg*)lParam;
	UINT uID = GetWindowLong(hwnd, GWL_ID);
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	CString strvalue=pApp->g_GetText(uID,pApp->m_nLandId);
    if(uID == IDC_DEVICE_CONNECT)
    {
        return TRUE;
    }
	//	GetDlgItem(ID_Array[j])->SetWindowText(strvalue);
	pHandle->SetDlgItemText(uID,strvalue);
	return TRUE;
}
void CDialupKitDlg::TransDlg()
{
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	int nStartId = IDC_BTN_CONNECT ;
	int nEndId = IDC_STATIC_RECEIVED;
	
	EnumChildWindows(m_hWnd,EnumChildProc,(LPARAM)this);
/*	for(UINT j=nStartId;j<nEndId;j++)
	{
		
		CString strvalue=pApp->g_GetText(j,pApp->m_nLandId);
		//	GetDlgItem(ID_Array[j])->SetWindowText(strvalue);
		SetDlgItemText(j,strvalue);
	}*/
}

void CDialupKitDlg::UpdateDeviceConnectStatus()
{
	//IDC_DEVICE_CONNECT ID that text that should be changed
	static CStatic* pModemConnectstatus	= (CStatic*)GetDlgItem(IDC_DEVICE_CONNECT);
    //static CStatic* pModemDisconnectStatus = (CStatic*)GetDlgItem(IDC_DISCONNECT);
	//CString strDevice ;
    CString strTemp;
	bool    bUpdate = FALSE;
    BOOL    bTest = FALSE;
    CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();

    bTest = CControlData::Instance()->Is8512ModemConnect();

    bUpdate = (bTest == m_bPreConnectState ? FALSE : TRUE);

	if(bUpdate || m_bFirstTime || m_bLanChange)
	{
		HBITMAP hBitmap;
		WORD 	nBitmapID;
    	CStatic *pStatic = (CStatic *)GetDlgItem(IDC_BITSTATUS);
		
		if(bTest == FALSE)
		{
			nBitmapID = IDB_BITMAP_DISCONNECT;
            //pModemDisconnectStatus->ShowWindow(SW_SHOW);
            //pModemConnectstatus->ShowWindow(SW_HIDE);
            //strTemp.LoadString(IDC_DISCONNECT);
            strTemp = pApp->g_GetText(IDC_DISCONNECT,pApp->m_nLandId);
            TRACE(strTemp);
		}
		else
		{
			nBitmapID = IDB_BITMAP_CONNECT;
            //pModemDisconnectStatus->ShowWindow(SW_HIDE);
            //pModemConnectstatus->ShowWindow(SW_SHOW);
            //strTemp.LoadString(IDC_DEVICE_CONNECT);
            strTemp = pApp->g_GetText(IDC_DEVICE_CONNECT,pApp->m_nLandId);
            TRACE(strTemp);
		}
        
        pModemConnectstatus->SetWindowText(strTemp);
		hBitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), 
                                    MAKEINTRESOURCE(nBitmapID), 
                                    IMAGE_BITMAP, 
                                    0, 
                                    0, 
                                    LR_LOADMAP3DCOLORS);
		pStatic->ModifyStyle(0xF, SS_BITMAP);
    	pStatic->SetBitmap(hBitmap);

        //UpdateData(FALSE);
	}

    m_bLanChange = FALSE;
	m_bPreConnectState = bTest;
    m_bFirstTime = FALSE;

	return;
}

void CDialupKitDlg::UpdateConnectionStatus()
{
    static CStatic* pConnectionStatus   = (CStatic*)GetDlgItem(IDC_CONNECTION_STATUS);
	static CStatic* pConnectionDuration = (CStatic*)GetDlgItem(IDC_CONNECTION_DURATION);
    static CStatic* pConnectionSpeed    = (CStatic*)GetDlgItem(IDC_CONNECTION_SPEED);
    static CStatic* pSentBytes          = (CStatic*)GetDlgItem(IDC_STATIC_SENT_BYTES);
    static CStatic* pReceiveBytes       = (CStatic*)GetDlgItem(IDC_STATIC_RECEIVED_BYTES);
    static CStatic* pCompressionIn      = (CStatic*)GetDlgItem(IDC_STATIC_COMPRESSION_IN);
    static CStatic* pCompressionOut     = (CStatic*)GetDlgItem(IDC_STATIC_COMPRESSION_OUT);
    static CStatic* pErrorsCnt          = (CStatic*)GetDlgItem(IDC_STATIC_ERRORS_CNT);
    
	CString str;

    RAS_STATS statistics;
    memset(&statistics, 0, sizeof(RAS_STATS));
    statistics.dwSize = sizeof(RAS_STATS);
    
    // get connection statistics
    RasGetConnectionStatistics(CController::Instance()->GetRASConnHandle(), &statistics);

    // check whether connected
    if(!CController::Instance()->IsConnectionActive()) // connection is not active(disconnected not via our program)
    {        
        Disconnect();
        TRACE(L"**** Call DisConnecct() in UpdateConnectionStatus()\n");
        return; // need not to compute duration;
    }
    // active
    CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
    // set connection status string
    //str.LoadString(IDS_STATUS_CONNECTED);
    str = pApp->g_GetText(IDC_BTN_CONNECT,pApp->m_nLandId);
    pConnectionStatus->SetWindowText(str);
    
    // set connection speed
    pConnectionSpeed->SetWindowText(FormatSpeed(statistics.dwBps));
    
    // set sent bytes
    pSentBytes->SetWindowText(FormatNumberPointedOff(statistics.dwBytesXmited));
    
    // set received bytes
    pReceiveBytes->SetWindowText(FormatNumberPointedOff(statistics.dwBytesRcved));
    
    // set compression ratio in
    str.Format(L"%d%%", statistics.dwCompressionRatioIn);
    pCompressionIn->SetWindowText(str);
    
    // set compression ration out
    str.Format(L"%d%%", statistics.dwCompressionRatioOut);
    pCompressionOut->SetWindowText(str);
    
    // set errors
    str.Format(L"%d",
        statistics.dwFramingErr
        +statistics.dwHardwareOverrunErr
        +statistics.dwAlignmentErr
        +statistics.dwBufferOverrunErr
        +statistics.dwCrcErr+statistics.dwTimeoutErr);
    
    pErrorsCnt->SetWindowText(str);
    
    // set disconnect button
    //str.LoadString(IDS_BTN_DISCONNECT);
    //GetDlgItem(IDC_BTN_CONNECT)->SetWindowText(str);
    //lishch modify
    str = pApp->g_GetText(T_DISCON,pApp->m_nLandId);
    str.TrimRight(_T("(&D)"));
    //str=pApp->g_GetText(IDC_CONNECTION_STATUS,pApp->m_nLandId);
    SetDlgItemText(IDC_BTN_CONNECT,str);
    
    
    // update connection duration
    CString strDuration;  
    m_tsDuration = CTimeSpan(0, 0, 0, statistics.dwConnectDuration / 1000);
    strDuration = m_tsDuration.Format(IDS_TIMESPAN_FORMAT);
    
    // update duration
    pConnectionDuration->SetWindowText(strDuration);

    
    TRACE(L"Sent: %dbytes, Recieved: %dbytes\n", statistics.dwBytesXmited, statistics.dwBytesRcved);

    TRACE(L"compression in: %d, compression out: %d\n", statistics.dwCompressionRatioIn, statistics.dwCompressionRatioOut);

    TRACE(L"Errors: %d\n", 
          statistics.dwFramingErr
          +statistics.dwHardwareOverrunErr
          +statistics.dwAlignmentErr
          +statistics.dwBufferOverrunErr
          +statistics.dwCrcErr+statistics.dwTimeoutErr);

    TRACE(L"Speed: %d\n", statistics.dwBps);

    UpdateConnectionIcon(statistics.dwBytesXmited, statistics.dwBytesRcved);
    
    // Update Tx and Rx
    m_nTx = statistics.dwBytesXmited;
    m_nRx = statistics.dwBytesRcved;
}



void CDialupKitDlg::UpdateConnectionIcon(int nTx, int nRx)
{   

    //     Disconnect - 0
    //           Sent - 1
    //       Received - 2
    //  Sent&Received - 3
    int nBmpIndex = (nTx != m_nTx) | ((nRx != m_nRx) << 1);

    TRACE(_T("nBmpIndex = %d\n"), nBmpIndex);

    HBITMAP hBitmap;
    CStatic *pStatic = (CStatic *)GetDlgItem(IDC_STATIC_STATUS_ICON);
    hBitmap = (HBITMAP)LoadImage(
        AfxGetInstanceHandle(), 
        MAKEINTRESOURCE(IDB_STATUS1 + nBmpIndex), 
        IMAGE_BITMAP, 
        0, 
        0, 
        LR_LOADMAP3DCOLORS);
    pStatic->ModifyStyle(0xF, SS_BITMAP);
    pStatic->SetBitmap(hBitmap);  
}

CString CDialupKitDlg::FormatSpeed(int nBps)
{
    CString str;

    if(nBps >= ONE_KBYTE)
    {
        if(nBps % ONE_KBYTE)
        {
            str.Format(_T("%.1fKbps"), ((double)nBps) / ONE_KBYTE_F);
        }
        else
        {
            str.Format(_T("%dKbps"), nBps / ONE_KBYTE);
        }
    }
    else
    {
        str.Format(_T("%dbps"), nBps);
    }
   // TRACE(_T("FormatSpeed(%dbps): %s\n"), nBps, str);
    return str;
}


void CDialupKitDlg::Disconnect()
{
    CString str;
	
    // Kill the timer
    KillTimer(TIMER_DETECT_CONNECTION_STATUS);
	
    // stop connect if necessary
    CController::Instance()->StopConnect();
	
    // Reset the connection status icon
    UpdateConnectionIcon(m_nTx, m_nRx);
	
    // Write a history record to the registry
    if(m_nTx != 0 && m_nRx !=0)
    {
        WriteOneHistoryRecord(m_tsDuration, m_nTx, m_nRx);
    }
    
    m_nTx = m_nRx = 0;
    
    // set connect button
    //str.LoadString(IDS_BTN_CONNECT);
    //GetDlgItem(IDC_BTN_CONNECT)->SetWindowText(str);
    //lishch modify
    CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
    str=pApp->g_GetText(IDC_BTN_CONNECT,pApp->m_nLandId);
    SetDlgItemText(IDC_BTN_CONNECT,str);
    
    // display disconnected status;
    //str.LoadString(IDS_STATUS_DISCONNECTED);
    //GetDlgItem(IDC_CONNECTION_STATUS)->SetWindowText(str);
    str=pApp->g_GetText(IDC_CONNECTION_STATUS,pApp->m_nLandId);
    SetDlgItemText(IDC_CONNECTION_STATUS,str);	
}

//message handler
void CDialupKitDlg::OnBtnConnect() 
{
	// TODO: Add your control notification handler code here
	CString str;
    
	// TODO: Add your control notification handler code here
	if(CController::Instance()->IsConnectionActive()) // active, hang up...
	{
        Disconnect();
        TRACE(L"**** Call DisConnecct() in OnBtnConnect()\n");
	}
	else // not active, beging dialing...
	{
		CController::Instance()->BeginConnect();
	}
}

void CDialupKitDlg::OnBtnExit() 
{
	// TODO: Add your control notification handler code here
	m_bShutDown = TRUE;
	PostMessage(WM_CLOSE, 0, 0);
}

void CDialupKitDlg::OnBtnMinimize() 
{
	// TODO: Add your control notification handler code here
	PostMessage(WM_SYSCOMMAND, SC_MINIMIZE, 0);
}
void CDialupKitDlg::OnGetDialStatus(WPARAM wParam, LPARAM lParam)
{
    // TODO: Add with the status during dialing
	
    CController::Instance()->OnGetDialStatus(wParam, lParam);
}

void CDialupKitDlg::OnMenuAbout() 
{
	// TODO: Add your command handler code here
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();
}

void CDialupKitDlg::OnUpdateMenuAbout(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}

void CDialupKitDlg::OnAppAbout() 
{
	// TODO: Add your command handler code here

}

void CDialupKitDlg::OnUpdateAppAbout(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}

void CDialupKitDlg::OnMenuHistory() 
{
	// TODO: Add your command handler code here
	CHistoryDlg dlg;
	dlg.DoModal();
}

void CDialupKitDlg::OnUpdateMenuHistory(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}

void CDialupKitDlg::OnDestroy() 
{
	CMyDlg::OnDestroy();
	
	// TODO: Add your message handler code here
	int ret = 0;
	
	
	if(!m_bShutDown)  // just hide the window
	{
		ShowWindow(SW_HIDE);
		return;
	}
	
	// check whether there is a connection still active
	
	SetTrayIcon(FALSE); // delete the tray icon	
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
	// TODO: Add extra initialization here
	CString strvalue=pApp->g_GetText(ID_MENU_ABOUT,pApp->m_nLandId);
	strvalue =strvalue.Left(strvalue.Find(_T("(")));
    SetWindowText(strvalue);
	strvalue.Empty();
	strvalue=pApp->g_GetText(IDOK,pApp->m_nLandId);
	GetDlgItem(IDOK)->SetWindowText(strvalue);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

#if 0
void CDialupKitDlg::OnBtnDetect() 
{
	// TODO: Add your control notification handler code here
	
}
#endif

void CDialupKitDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
    if(!m_bShutDown)
    {
       ShowWindow(SW_HIDE);    
	   return;
    }
    else
    {
        if(CController::Instance()->IsConnectionActive())
        {
            // ask user whether want to hang up the connection
            //CString str;
            //str.LoadString(IDS_QUIT_VERIFY);
            //lishch modify
            //CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
            //str = pApp->g_GetText(206,pApp->m_nLandId);
            //ret = MessageBox(str, NULL, MB_YESNO | MB_ICONINFORMATION);
            
            //if(IDNO == ret)
            //{
            //    m_bShutDown = FALSE;
            //   return;
            //}
            
            //    CController::Instance()->StopConnect();
            CString str;
            int ret = 0;
            str.LoadString(IDS_QUIT_VERIFY);
            //lishch modify
            //CDialupKitApp *pApp =(CDialupKitApp*)AfxGetApp();
            //str = pApp->g_GetText(206,pApp->m_nLandId);
            ret = MessageBox(str, NULL, MB_YESNO | MB_ICONINFORMATION);
            
            if(IDNO == ret)
            {
                m_bShutDown = FALSE;
                return;
            }
            Disconnect();
            TRACE(_T("**** Call DisConnecct() in OnCancel()\n"));
        }

    }

	CMyDlg::OnClose();
}

void CDialupKitDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your message handler code here and/or call default
    if(!nIDCtl) m_hMenu.DrawItem(lpDrawItemStruct);
	CMyDlg::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CDialupKitDlg::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	// TODO: Add your message handler code here and/or call default
    if(!nIDCtl) m_hMenu.MeasureItem(lpMeasureItemStruct);
	CMyDlg::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}
