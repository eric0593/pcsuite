// ControlData.h: interface for the CControlData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONTROLDATA_H__9660A5A3_1706_4437_9417_6DDC7D0B721F__INCLUDED_)
#define AFX_CONTROLDATA_H__9660A5A3_1706_4437_9417_6DDC7D0B721F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ras.h"
#include "raserror.h"
#include <setupapi.h>
#include "Singleton.h"
#include "DialupKit.h"
#define  GSM		0
#define  CDMA		1



//#define FILENAE_MODULE            "C:\\AMOI-GPRS\\AMOI-GPRS.EXE"

// shortcut icon
#define SEPARATOR                   "\\"
#define SUFFIX                      ".LNK"
#define LINKNAME                    "AMOI-DIAL"
#define FILENAME_MODULE             "AMOI-DIAL.EXE"

// Sys Settings
//#define AMOI_SYS_SETTING            _T("Software\\AMOI AMOI-GPRS\\SysSeting")
#define AMOI_VALUENAME_COM          _T("AMOI-COM")
#define AMOI_VALUENAME_MODEM        _T("AMOI-MODEM")
#define AMOI_VALUENAME_DIAL         _T("AMOI-DIAL")

#define AMOI_DEFAULVALUE_COM         _T("COM")
#define AMOI_DEFAULVALUE_MODEM       _T("Mobile")
#define AMOI_DEFAULVALUE_DIAL        _T("AMOI-DIAL")

// Net Settings





#define AMOI_IP4_USER_INIT          L"UserInit"
#define AMOI_IP4_AT                 L"AT+CGDCONT=1,\"IP\",\"orange.fr\""

#define AMOI_MOBILE_DISCONNECTED    L"Please ensure Amoi A500 is connected."


#define RASDT_LINKNAME_CM               L"INQ1 Mobile Modem"
#define RASDT_LINKNAME_GM               L"INQ1 Mobile Modem"
#define RASDT_COUNTRYID                 33
#define RASDT_COUNTRYCODE               33
#define RASDT_AREACODE                  "06"
#define RASDT_LOCALPHONENUMBER          "*99#"

#define PBKPROFILE_BASEPROTOCOL         "BaseProtocol"
#define PBKPROFILE_EXCLUDEDPROTOCOLS    "ExcludedProtocols"
#define PBKPROFILE_SWCOMPRESSION        "SwCompression"
#define PBKPROFILE_DIALPERCENT          "DialPercent"
#define PBKPROFILE_DIALSECONDS          "DialSeconds"
#define PBKPROFILE_HANGUPPERCENT        "HangUpPercent"
#define PBKPROFILE_HANGUPSECONDS        "HangUpSeconds"
#define PBKPROFILE_SHAREMSFILEPRINT     "ShareMsFilePrint"
#define PBKPROFILE_BINDMSNETCLIENT      "BindMsNetClient"
#define PBKPROFILE_HANGUPSECONDS        "HangUpSeconds"
#define PBKPROFILE_PREVIEWUSERPW        "PreviewUserPw"
#define PBKPROFILE_SHOWDIALINGPROGRESS  "ShowDialingProgress"
#define PBKPROFILE_SHOWICONINTASKBAR    "ShowMonitorIconInTaskBar"
#define PBKPROFILE_MS_MSCLIENT          "ms_msclient"
#define PBKPROFILE_MS_SERVER            "ms_server"
#define PBKPROFILE_HWFLOWCONTROL        "HwFlowControl"
#define PBKPROFILE_PROTOCOL             "Protocol"
#define PBKPROFILE_COMPRESSION          "Compression"
#define PBKPROFILE_SPEAKER              "Speaker"

#define CONTROLLER_STR_OPENPORT                 "open port..."
#define CONTROLLER_STR_PORTOPENED               "port opened."
#define CONTROLLER_STR_CONNECTDEVICE            "connecting device..."
#define CONTROLLER_STR_DEVICECONNECTED          "device connected..."
#define CONTROLLER_STR_VERIFY_USERNAME_PASSWORD "verify user name and password..."
#define CONTROLLER_STR_PASS_VERIFY              "identity pass."
#define CONTROLLER_STR_CONNECTED                "CM connected successfully."
#define CONTROLLER_STR_DISCONNECTED             "Disconnected."
#define CONTROLLER_STR_ERRORCODE                "error code: %d %s"





#define    STR_HANGINGUP           "Hanging up..."

extern CString m_strInitKey;

enum COMTYPE
{
	COM_TYPE_COM,
	COM_TYPE_MODEM	
};

class CStatusDlg;

class CControlData  
{
    DEFINE_SINGLETON(CControlData);
public:
	BOOL m_bInstall;
	BOOL SysSetup();
	CString GetParamByIndex(int nIndex);
	void InitDialParam(DIAL_PRAM* pSrc);
	void CopyParam(DIAL_PRAM* pSrc,DIAL_PRAM* pDst);
    BOOL QueryNetSettings();        // 查询网络设置(APN, UserName, Password, PhoneNumber)
    BOOL WriteNetSettings();        // 写入网络设置(APN, UserName, Password, PhoneNumber)

    void InitSettings();            // 初始化一些默认参数
    void InitVariable();            // 初始化一些其他变量(与上一个函数可能需要合并)
    BOOL InitSystem(HWND hWnd);     // 初始化(如果没有查询到网络设置，需要用默认参数)

    BOOL EsTablishLinkAttribute(CString strDevice);     // 在"网络邻居"中建立连接图标
    BOOL SetPbkProfileParam();                          // 建立好连接图标后，设置Pbk目录下的rasphone.pbk文件参数
    
    BOOL BeginConnect();                                // 发起拨号
    BOOL StopConnect();                                 // 停止拨号
    LRESULT GetDialStatus(WPARAM wParam, LPARAM lParam, CString &strInfo);  // 拨号后产生的各种呼叫状态
    void SetRASStatus(DWORD dwErrorCode, CString strInfo);  // 显示拨号后各种状态信息

    BOOL DetectModemDrive();                            // 检测Modem的驱动是否存在，这个可以不用了
    BOOL DetectModemConnect(CString &strDevice);        // 检测当前PC是否有夏新手机的Modem连接

   	BOOL Is8512ModemConnect();         //detect the checked if curent device is 8512 modem

    BOOL SetGPRS();                 // 设置C网、G网参数，并用WriteNetSettings()写入注册表(当前只完成C网，这个函数最后需要能设置C网、G网参数)
    void SetAPN();                  //给当前的COM设置APN
    void SetAPN8709(CString strIPV4); 
	int GetDataCom();				//获得用来传输RSSI等数据信息的Com口
	int GetValidComPort_V2(COMTYPE comtype,int *pComIdBuf, size_t ComIdBufSize); //用来获取手机中的com口信息


//	void SleepAfterHangUp(HRASCONN hrasConn); // ADD BY SHAOZW to replace Sleep(3000) after call RasHangUp() 
	BOOL IsConnectionActive(HRASCONN* phRasConn = NULL);                 // ADD BY SHAOZW TO CHECK WHETHER THE CONNECTION IS STILL ACTIVE

	BOOL IsPhoonBookEntryExist();             // ADD BY SHAOZW TO CHECK WHETHER THE PHOON-BOOK ENTRY IS EXIST
	inline HRASCONN GetRASConnHandle()
	{
		return m_hRasConn;
	}
	inline void SetRASConnHandle(HRASCONN hRasConn)
	{
        m_hRasConn = hRasConn;
	}
	
	// Operate 
	BOOL SetModemInit(CString strDevice);
protected:
    // protected里的函数还不知道有什么用，是厦门同事写的，有可能G网下要用到，所以暂且我把它们给放着
    int  InstallSuntekModem(LPCTSTR lpszComName);
    BOOL RegisterModem
        (
        IN HDEVINFO hdi, 
        IN PSP_DEVINFO_DATA pdevData, 
        IN LPCTSTR pszPort
        );
    int InstallRootEnumeratedDriver
        (
        IN LPGUID lpGUID, IN LPCTSTR lpszHardwareID ,
        IN LPCTSTR lpszDescription, IN LPCTSTR lpszInfName,
        IN LPCTSTR lpszComName,
        OUT LPTSTR lpszInstanceID, OUT LPBOOL pfNeedReboot
        );

// Attribute
private:
    HWND    m_hWnd;                 // 主对话框的句柄(最后有可能会被去掉，毕竟这个类是主对话框不应该直接发生关系)
    HRASCONN m_hRasConn;            // 拨号的RAS句柄

    CString m_strDialName;          // "网络邻居"中显示的连接名，对应"Amoi-CM"、"Amoi-GM"
                                    // 实际上并没有用到该连接名，均使用字符串常量(RASDT_LINKNAME_CM)[shaozw#]

    BOOL    m_bConnectFlag;         // 当前是否已经拨号了
	CString m_RegDial;
	CString m_RegModem;
	CString m_RegCOM;
    CString	m_strAPN;
    CString	m_strPhoneNumber;
    CString	m_strUserName;
    CString	m_strPassword;
    CString	m_strIPAddress;

	CString m_strDeviceName;        // ADD BY SHAOZW, DEVICE NAME
	

    CStatusDlg *m_pStatusDlg;       // 拨号后显示的状态信息对话框指针

	const CString GetUSBRegistrySubKey( const HKEY hKey, const CString strSection ) const;

	const CString GetModemRegistrySubKey( const HKEY hKey, const CString strSection ) const;
	const CString GetEnumIndex( const int nIndex, const int nID ) const;
	const CString MyQueryKeyValue( const HKEY hKey, LPCTSTR pszKey, LPCTSTR pszValue ) const;
	void PolicySet(BOOL bFlag);
	void m_GetOSVersion();
	BOOL QueryCOM();

	BOOL QueryModem();
	void SetupDial();
	CString m_CurrentDial;
	CString m_CurrentModem;
	CString m_CurrentCOM;
	CString m_TempCOM;
    CString m_LastCom;
};

#endif // !defined(AFX_CONTROLDATA_H__9660A5A3_1706_4437_9417_6DDC7D0B721F__INCLUDED_)
