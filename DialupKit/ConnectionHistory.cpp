// ConnectionHistory.cpp: Connection History implement file
#include "StdAfx.h"
#include "ConnectionHistory.h"
#include "resource.h"
#include "common.h"

#define DIALING_HISTORY        L"Software\\AMOI IM2\\Dialup\\History"
#define DIALING_HISTORY_CNT    L"RecordCnt"
#define DIALING_HISTORY_TOTAL  L"Total"
#define AMOI_DIALUP_KEYNAME    L"Software\\AMOI IM2\\Dialup"
#define AMOI_DIALUP_HISTORY_KEYNAME L"History"

BOOL GetHistoryRecordCnt(int& nCnt)
{
    CString str;
    BOOL ret;
    ret = ReadRegister(DIALING_HISTORY, DIALING_HISTORY_CNT, str);
    if(0 == ret)
    {
        nCnt = 0;
        return FALSE;
    }
    nCnt = _ttoi(str);

    TRACE(L"GetHistoryRecordCnt(): %d\n", nCnt);
    return TRUE;
}

BOOL GetHistoryTotalStatistic(CTimeSpan& timeSpan, int& nTx, int& nRx)
{

    CString strValue;
    CString str;
    int nIndex = -1;
    int nHours = 0; 
    int nMins  = 0; 
    int nSecs  = 0; 
    
    if(!ReadRegister(DIALING_HISTORY, DIALING_HISTORY_TOTAL, strValue))
    {
		timeSpan = CTimeSpan(0, 0, 0, 0);
		nTx = nRx = 0;
		return FALSE;
	}

    if(strValue.IsEmpty())
    {
        timeSpan = CTimeSpan(0, 0, 0, 0);
        nTx = nRx = 0;
        return FALSE;
    }
    // EXTRACT TIMESPAN
    nIndex = strValue.Find(L' ');
    if(-1 == nIndex)
    {
        return FALSE;
    }   
    str = strValue.Left(nIndex);
    strValue = strValue.Right(strValue.GetLength() - nIndex - 1);
    
    // EXTRACT HOUR
    nIndex = str.Find(L':');  
    CString str1;
    str1 = str.Left(nIndex);
    nHours = _ttoi(str1);

    // EXTRACT MINUTE
    str = str.Right(str.GetLength() - nIndex - 1);
    str1 = str.Left(2);
    nMins = _ttoi(str1);

    // EXTRACT SECOND
    str1 = str.Right(2);
    nSecs = _ttoi(str1);

    timeSpan = CTimeSpan(0, nHours, nMins, nSecs);

    // EXTRACT RX
    nIndex = strValue.ReverseFind(' ');
    if(-1 == nIndex)
    {
        return FALSE;
    }
    str = strValue.Right(strValue.GetLength() - nIndex - 1);
    nRx = _ttoi(str);

    // EXTRACT TX
    str = strValue.Left(nIndex);
    nTx = _ttoi(str);
    
    TRACE(L"GetHistoryTotalStatistic(): %d:%d:%d %d %d\n", nHours, nMins, nSecs, nTx, nRx);
    return TRUE;
}



BOOL WriteOneHistoryRecord(CTimeSpan timeSpan, int nTx, int nRx)
{
    
    int nRecordCnt = 0;

    CString str;
    CString strValue;
    CTimeSpan totalTimeSpan;
    int nTotalTx = 0;
    int nTotalRx = 0;

    strValue = FormatHistoryRecordString(timeSpan, nTx, nRx);
    
    GetHistoryRecordCnt(nRecordCnt);

    CString strRecordValueName;
    strRecordValueName.Format(L"%d",nRecordCnt);
    
    if(!WriteRegister(DIALING_HISTORY, strRecordValueName, strValue))
        return FALSE;

    nRecordCnt++;
    str.Format(L"%d", nRecordCnt);
    if(!WriteRegister(DIALING_HISTORY, DIALING_HISTORY_CNT, str))
        return FALSE;

    // UPDATE TOTAL STATISTIC
    if(!GetHistoryTotalStatistic(totalTimeSpan, nTotalTx, nTotalRx)) 
    {
        // total statistic maybe don't exist, create it
        WriteRegister(DIALING_HISTORY, DIALING_HISTORY_TOTAL, strValue.Right(strValue.GetLength() - 10));
    }
    else
    {
        totalTimeSpan += timeSpan;
        nTotalTx      += nTx;
        nTotalRx      += nRx;
        str = FormatHistoryRecordString(totalTimeSpan, nTotalTx, nTotalRx);
        str = str.Right(str.GetLength() - 10);  // delete the date string

        WriteRegister(DIALING_HISTORY, DIALING_HISTORY_TOTAL, str);
    }

    return TRUE;
}


CString FormatHistoryRecordString(CTimeSpan timeSpan, int nTx, int nRx)
{

    CString str;
    CString strValue;
    // GERNERATE DATE STRING
    CTime sysTime = CTime::GetCurrentTime();
    strValue = sysTime.Format(L"%d/%b/%y ");
    strValue.MakeUpper();
    
    // GERNARATE TIME STRING

    if(timeSpan.GetDays() > 0)
    {
        str.Format(L"%d:%.2d:%.2d", 
                   timeSpan.GetDays()*24 + timeSpan.GetHours(), 
                   timeSpan.GetMinutes(), 
                   timeSpan.GetSeconds());
    }
    else
    {
        str = timeSpan.Format(IDS_TIMESPAN_FORMAT);
    }

    strValue += str+" ";

    // GERNARATE TX STRING
    str.Format(L"%d ", nTx);
    strValue += str;

    // GERNARATE RX STRING
    str.Format(L"%d", nRx);
    strValue += str;
    
    TRACE(L"FormatHistoryRecordString(timeSpan %d, %d): %s\n", nTx, nRx, strValue);
    return strValue;
}


BOOL GetOneHistoryRecord(int nNo, CString& strDate, CString& strTimeSpan, int& nTx, int& nRx)
{
    CString str;
    CString strValueName;
    CString strValue;
    int nIndex = -1;
    
    strValueName.Format(L"%d", nNo);

    if(!ReadRegister(DIALING_HISTORY, strValueName, strValue))
    {
        return FALSE;
    }
    
    // EXTRACT DATA STRING
    nIndex = strValue.Find(' ');
    if(-1 == nIndex)
    {
        return FALSE;
    }
    
    strDate = strValue.Left(nIndex);
    strValue = strValue.Right(strValue.GetLength() - nIndex - 1);
    
    // EXTRACT TIMESPAN STRING
    nIndex = strValue.Find(' ');  
    strTimeSpan = strValue.Left(nIndex);
    strValue = strValue.Right(strValue.GetLength() - nIndex - 1);

    // EXTRACT RX
    nIndex = strValue.ReverseFind(' ');
    if(-1 == nIndex)
    {
        return FALSE;
    }
    str = strValue.Right(strValue.GetLength() - nIndex - 1);
    nRx = _ttoi(str);

    // EXTRACT TX
    str = strValue.Left(nIndex);
    nTx = _ttoi(str);
    
    TRACE(L"GetOneHistoryRecord(%d): %s %s %s %s\n", nNo, strDate, strTimeSpan, FormatBytesOutput(nTx), FormatBytesOutput(nRx));
    return TRUE;    
}

CString FormatBytesOutput(int nBytes)
{
    CString str;
    if(nBytes < ONE_KBYTE)
    {
        str.Format(L"%dbytes", nBytes);
    }
    else
    {
        if(nBytes > ONE_MBYTE)
       
		{   if(nBytes % ONE_MBYTE)
            {
                str.Format(_T("%.2fMbytes"), ((double)nBytes) / ONE_MBYTE_F);
            }
            else
            {
                str.Format(L"%dMbytes", nBytes / ONE_MBYTE);
            }

        }
        else
        {
            if(nBytes % ONE_KBYTE)
            {
                str.Format(L"%.2fKbytes", ((double)nBytes) / ONE_KBYTE_F);
            }
            else
            {
                str.Format(L"%dKbytes", nBytes / ONE_KBYTE);
            }
            
        }
    }

    TRACE(L"FormatBytesOutput(%d): %s\n", nBytes, str);
    return str;
}


BOOL ClearAllHistoryRecords()
{
    int nCnt = 0;
    CString strValueName;
    GetHistoryRecordCnt(nCnt);

    WriteRegister(DIALING_HISTORY, DIALING_HISTORY_TOTAL, _T(""));
    WriteRegister(DIALING_HISTORY, DIALING_HISTORY_CNT, _T("0"));

//    for(int i = 0; i < nCnt; i++)
//    {
//        strValueName.Format("%d", i);
//        if(!DeleteRegister(DIALING_HISTORY, strValueName))
//            return FALSE;
//    }

    CRegKey regKey;
    if(ERROR_SUCCESS != regKey.Create(HKEY_LOCAL_MACHINE, AMOI_DIALUP_KEYNAME))
        return FALSE;
    regKey.DeleteSubKey(AMOI_DIALUP_HISTORY_KEYNAME);
    regKey.Close();
    
    return TRUE;
}




