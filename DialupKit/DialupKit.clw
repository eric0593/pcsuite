; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=SelCountry
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "dialupkit.h"
LastPage=0

ClassCount=8
Class1=CDialSetDlg
Class2=CDialupKitApp
Class3=CAboutDlg
Class4=CDialupKitDlg
Class5=CHistoryDlg
Class6=CMyDlg
Class7=SelCountry
Class8=CStatusDlg

ResourceCount=10
Resource1=IDD_DIALOG_HISTORY (English (U.S.))
Resource2=IDD_HANGING_UP (English (U.S.))
Resource3=IDD_DIALOG_LINKSTATUS (English (U.S.))
Resource4=IDD_ABOUTBOX
Resource5=IDD_DIA_INITSET
Resource6=IDD_DIALUPKIT_DIALOG (English (U.S.))
Resource7=IDD_DIALOG_HANGING_UP (English (U.S.))
Resource8=IDD_DLG_NETSET (English (U.S.))
Resource9=IDR_MAINFRAME
Resource10=IDD_DIALOG_Dialup

[CLS:CDialSetDlg]
Type=0
BaseClass=CDialog
HeaderFile=DialSetDlg.h
ImplementationFile=DialSetDlg.cpp
LastObject=2
Filter=D
VirtualFilter=dWC

[CLS:CDialupKitApp]
Type=0
BaseClass=CWinApp
HeaderFile=DialupKit.h
ImplementationFile=DialupKit.cpp
Filter=N
LastObject=ID_MENU_CONN

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=DialupKitDlg.cpp
ImplementationFile=DialupKitDlg.cpp
LastObject=ID_MENU_SETUP
Filter=W

[CLS:CDialupKitDlg]
Type=0
BaseClass=CMyDlg
HeaderFile=DialupKitDlg.h
ImplementationFile=DialupKitDlg.cpp
Filter=D
LastObject=CDialupKitDlg
VirtualFilter=dWC

[CLS:CHistoryDlg]
Type=0
BaseClass=CDialog
HeaderFile=HistoryDlg.h
ImplementationFile=HistoryDlg.cpp
LastObject=CHistoryDlg

[CLS:CMyDlg]
Type=0
BaseClass=CDialog
HeaderFile=MyDlg.h
ImplementationFile=MyDlg.cpp
LastObject=CMyDlg

[CLS:SelCountry]
Type=0
BaseClass=CDialog
HeaderFile=SelCountry.h
ImplementationFile=SelCountry.cpp
Filter=D
LastObject=IDC_COUN_SET
VirtualFilter=dWC

[CLS:CStatusDlg]
Type=0
BaseClass=CDialog
HeaderFile=StatusDlg.h
ImplementationFile=StatusDlg.cpp
LastObject=CStatusDlg

[DLG:IDD_DLG_NETSET]
Type=1
Class=CDialSetDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_DIALUPKIT_DIALOG]
Type=1
Class=CDialupKitDlg

[DLG:IDD_DIALOG_HANGING_UP]
Type=1
Class=CHangingupStatusDlg

[DLG:IDD_DIALOG_HISTORY]
Type=1
Class=CHistoryDlg

[DLG:IDD_DIA_INITSET]
Type=1
Class=SelCountry
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDC_COUN_TITLE,static,1342308352
Control3=IDC_COUN_SET,combobox,1344340227

[DLG:IDD_DIALOG_LINKSTATUS]
Type=1
Class=CStatusDlg

[DLG:IDD_HANGING_UP (English (U.S.))]
Type=1
Class=?
ControlCount=1
Control1=IDC_STATIC_HANGINGUP,static,1342308353

[DLG:IDD_DLG_NETSET (English (U.S.))]
Type=1
Class=?
ControlCount=15
Control1=IDC_EDIT_APN,edit,1350631552
Control2=IDC_EDIT_USER,edit,1350631552
Control3=IDC_EDIT_PASS,edit,1350631584
Control4=IDOK,button,1342242817
Control5=IDC_ACCESSNAME,button,1342177287
Control6=IDC_COUNTRYCOMBO,combobox,1344340227
Control7=IDC_COUNTRY,static,1342308352
Control8=IDC_PBNUMBER,static,1342308352
Control9=IDC_CONNECTNAME,static,1208090624
Control10=IDC_USERNAME,static,1342308352
Control11=IDC_PASSWORD,static,1342308352
Control12=IDC_APNNAME,static,1342308352
Control13=2,button,1342242816
Control14=IDC_DEFAULT,button,1342242816
Control15=IDC_DIALNO,combobox,1344340227

[DLG:IDD_DIALOG_LINKSTATUS (English (U.S.))]
Type=1
Class=?
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDC_STATIC_STATUSTEXT,static,1342308352
Control3=ID_DISCONNECT,button,1342242817

[DLG:IDD_DIALOG_HISTORY (English (U.S.))]
Type=1
Class=?
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDC_LIST_HISTORY,SysListView32,1342242817
Control3=IDC_BTN_RESETALL,button,1342242816

[DLG:IDD_DIALOG_HANGING_UP (English (U.S.))]
Type=1
Class=?
ControlCount=1
Control1=IDC_STATIC_HANGINGUP,static,1342308353

[DLG:IDD_DIALUPKIT_DIALOG (English (U.S.))]
Type=1
Class=?
ControlCount=29
Control1=IDC_LIST_GPRS,SysListView32,1073807364
Control2=IDC_BTN_CONNECT,button,1342242816
Control3=IDC_BTN_EXIT,button,1342242816
Control4=IDC_BTN_MINIMIZE,button,1342242816
Control5=IDC_GRPBOX_CONNECTION,button,1342177287
Control6=IDC_STATIC_STATUS,static,1342308352
Control7=IDC_STATIC_DURATION,static,1342308352
Control8=IDC_CONNECTION_STATUS,static,1342308354
Control9=IDC_CONNECTION_DURATION,static,1342308354
Control10=IDC_STATIC,static,1342177296
Control11=IDC_STATIC_ACTIVITY,button,1342177287
Control12=IDC_STATIC_BYTES,static,1342308352
Control13=IDC_STATIC_COMPRESSION,static,1342308352
Control14=IDC_STATIC_ERRORS,static,1342308352
Control15=IDC_STATIC_SENT,static,1342308353
Control16=IDC_STATIC_RECEIVED,static,1342308352
Control17=IDC_STATIC_SENT_BYTES,static,1342308353
Control18=IDC_STATIC_RECEIVED_BYTES,static,1342308354
Control19=IDC_STATIC_COMPRESSION_IN,static,1342308354
Control20=IDC_STATIC_COMPRESSION_OUT,static,1342308353
Control21=IDC_STATIC_ERRORS_CNT,static,1342308354
Control22=IDC_STATIC_SPEED,static,1342308352
Control23=IDC_CONNECTION_SPEED,static,1342308354
Control24=IDC_STATIC_STATUS_ICON,static,1342177294
Control25=IDC_STATIC,static,1342177287
Control26=IDC_STATIC,static,1342177287
Control27=IDC_STATIC,static,1342177287
Control28=IDC_DEVICE_CONNECT,static,1342308864
Control29=IDC_BITSTATUS,static,1342308352

[MNU:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_MENU_SETUP
Command2=ID_LANG_START
Command3=ID_MENU_CONN
Command4=ID_MENU_DISCON
Command5=ID_MENU_EXIT
Command6=ID_MENU_ABOUT
CommandCount=6

[DLG:IDD_DIALOG_Dialup]
Type=1
ControlCount=15
Control1=IDC_SETTING,button,1342242816
Control2=IDC_HELP,button,1342242816
Control3=IDC_EXIT1,button,1342242944
Control4=IDC_MIN,button,1342259328
Control5=IDC_BUTTON1,button,1342242816
Control6=IDC_BUTTON_CONTROL,button,1342242944
Control7=IDC_STATE,static,1342308352
Control8=IDC_CONNSTATU1,static,1342308352
Control9=IDC_CONNSTATU2,static,1342308352
Control10=IDC_CONNSTATU3,static,1342308352
Control11=IDC_SIGNAL1,static,1342308352
Control12=IDC_IDC_SIGNAL2,static,1342308352
Control13=IDC_SPEED,static,1342308352
Control14=IDC_BITMAP_SIGNAL,static,1342177294
Control15=IDC_STATIC,static,1342177294

