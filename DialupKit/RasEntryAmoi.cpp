// RasEntryAmoi.cpp: implementation of the CRasEntryAmoi class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "RasEntryAmoi.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//#define RASENTRY_AMOI_MOBILE    L"WP-S1 Proprietary USB Modem"
#define RASENTRY_AMOI_MOBILE	L"INQ1 USB Modem"
#define RASPHONE_DIRECTORY      L"Documents and Settings\\All Users\\Application Data\\Microsoft\\Network\\Connections\\Pbk\\rasphone.pbk"
#define STR_SUBKEY_COMNAME_AMOI L"SYSTEM\\CurrentControlSet\\Enum\\USB\\Vid_1614&Pid_0405&Amoi-A500"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRasEntryAmoi::CRasEntryAmoi()
{

}

CRasEntryAmoi::~CRasEntryAmoi()
{

}

// 检测驱动是否安装
BOOL CRasEntryAmoi::ExistModemDriver() 
{
    HKEY hSubKey;
    if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, STR_SUBKEY_COMNAME_AMOI, 0, KEY_READ, \
        &hSubKey)  != ERROR_SUCCESS)  
        return  FALSE; 
    
    return TRUE;
}

// 枚举当前是否有夏新手机Modem连接
BOOL CRasEntryAmoi::EnumModem(TCHAR *szDeviceType, CString &strDevice)
{
    BOOL bRet = FALSE;
    CRasEntry RasEntry;
    CStringArray strArray;
    RasEntry.EnumModem(szDeviceType, strArray);
	if(!strArray.GetSize())
	{
		strDevice = _T("Mobile 115200");
		//return TRUE;
		return FALSE;
	}
    for (int i = 0; i < strArray.GetSize(); i++)
    {
		// MODIFIED BY SHAOZW
        //if (strcmp(strArray[i], RASENTRY_AMOI_MOBILE) >= 0) // this way dosn't work (SHAOZW)
		//TRACE(strArray[i]);
		//TRACE(L"\n");
        
        if(-1 != strArray[i].Find(RASENTRY_AMOI_MOBILE))
        {
            strDevice = strArray[i];
			//TRACE(strDevice);
			//TRACE(L"\n");
            bRet = TRUE;
            break;
        }
    }

    return bRet;
}

// 取的Pbk文件目录
void CRasEntryAmoi::GetPbkDir(CString &str)
{
    TCHAR szFileName[_MAX_PATH] = L"";
    GetSystemDirectory(szFileName, MAX_PATH);
    TCHAR *p = _tcschr(szFileName, L'\\');
    if (p)
        *(p + 1) = 0;
    
    str.Format(_T("%s%s"), szFileName, RASPHONE_DIRECTORY);
}

// 写参数到Pbk文件中
BOOL CRasEntryAmoi::WritePbkProfile(
                                CString strSection, 
                                CString strKeyName, 
                                int nValue
                                )
{
    CString strFileName;
    GetPbkDir(strFileName);
    CString strValue;
    strValue.Format(_T("%d"), nValue);
    if (!WritePrivateProfileString(strSection, strKeyName, strValue, strFileName))
        return FALSE;
    
    return TRUE;
}
